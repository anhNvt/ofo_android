package com.ofo.ofo.service;

import android.content.Intent;

import com.google.firebase.messaging.FirebaseMessagingService;

/**
 * Created by cat on 7/16/16.
 */
public class OfoMsgService extends FirebaseMessagingService {

    @Override
    public void onDestroy() {
        super.onDestroy();

        startService(new Intent(getApplicationContext(), OfoMsgService.class));

    }
}

package com.ofo.ofo.util;

/**
 * Created by cat on 5/2/16.
 */
public class Const {

    public static final String KEY_PREF_FIRST_LAUNCH = "first_launch";

    public static final String KEY_PREF_SIGNED_IN = "log_in";

    public static final String KEY_PREF_CITY_ID = "city_id";

    public static final String KEY_PREF_CITY_NAME = "city_name";

    public static final String KEY_PREF_FCM_REG_ID = "fcm_reg_id";

    //key send data

    public static final String KEY_SEND_ORDER = "key_send_order";
    public static final String KEY_SEND_ADDRESS_MODE = "key_send_order";

}

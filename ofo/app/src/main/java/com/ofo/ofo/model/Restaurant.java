package com.ofo.ofo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ofo.ofo.util.LocaleHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.Optional;

/**
 * Created by cat on 5/7/16.
 */
public class Restaurant implements Serializable {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("name_ar")
    @Expose
    private String nameAr;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("banner_url")
    @Expose
    private String bannerUrl;
    @SerializedName("phone_no")
    @Expose
    private String phoneNo;
    @SerializedName("time_open")
    @Expose
    private String timeOpen;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lng")
    @Expose
    private String lng;
    @SerializedName("place_id")
    @Expose
    private String placeId;
    @SerializedName("promotion")
    @Expose
    private Boolean promotion;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("types")
    @Expose
    private List<Type> types = new ArrayList<>();
    @SerializedName("tags")
    @Expose
    private List<Tag> tags = new ArrayList<>();
    @SerializedName("count_review")
    @Expose
    private Integer countReview;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("order_online")
    @Expose
    private String orderOnline;
    @SerializedName("rating")
    @Expose
    private Double rating;
    @Expose
    @SerializedName("favourite")
    private Integer favor;

    private boolean favorite;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {

        if (LocaleHelper.getLanguage(LocaleHelper.LOCALE_ENG).equals(LocaleHelper.LOCALE_ENG)) {

            return name;

        }

        else return nameAr;

    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The nameAr
     */
    public String getNameAr() {
        return nameAr;
    }

    /**
     *
     * @param nameAr
     * The name_ar
     */
    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    /**
     *
     * @return
     * The address
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     * The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     * The bannerUrl
     */
    public String getBannerUrl() {
        return bannerUrl;
    }

    /**
     *
     * @param bannerUrl
     * The banner_url
     */
    public void setBannerUrl(String bannerUrl) {
        this.bannerUrl = bannerUrl;
    }

    /**
     *
     * @return
     * The phoneNo
     */
    public String getPhoneNo() {
        return phoneNo;
    }

    /**
     *
     * @param phoneNo
     * The phone_no
     */
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    /**
     *
     * @return
     * The timeOpen
     */
    public String getTimeOpen() {
        return timeOpen;
    }

    /**
     *
     * @param timeOpen
     * The time_open
     */
    public void setTimeOpen(String timeOpen) {
        this.timeOpen = timeOpen;
    }

    /**
     *
     * @return
     * The price
     */
    public String getPrice() {
        return price;
    }

    /**
     *
     * @param price
     * The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     *
     * @return
     * The lat
     */
    public String getLat() {
        return lat;
    }

    /**
     *
     * @param lat
     * The lat
     */
    public void setLat(String lat) {
        this.lat = lat;
    }

    /**
     *
     * @return
     * The lng
     */
    public String getLng() {
        return lng;
    }

    /**
     *
     * @param lng
     * The lng
     */
    public void setLng(String lng) {
        this.lng = lng;
    }

    /**
     *
     * @return
     * The placeId
     */
    public String getPlaceId() {
        return placeId;
    }

    /**
     *
     * @param placeId
     * The place_id
     */
    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    /**
     *
     * @return
     * The promotion
     */
    public Boolean getPromotion() {
        return promotion;
    }

    /**
     *
     * @param promotion
     * The promotion
     */
    public void setPromotion(Boolean promotion) {
        this.promotion = promotion;
    }

    /**
     *
     * @return
     * The discount
     */
    public String getDiscount() {
        return discount;
    }

    /**
     *
     * @param discount
     * The discount
     */
    public void setDiscount(String discount) {
        this.discount = discount;
    }

    /**
     *
     * @return
     * The types
     */
    public List<Type> getTypes() {
        return types;
    }

    /**
     *
     * @param types
     * The types
     */
    public void setTypes(List<Type> types) {
        this.types = types;
    }

    /**
     *
     * @return
     * The tags
     */
    public List<Tag> getTags() {
        return tags;
    }

    /**
     *
     * @param tags
     * The tags
     */
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    /**
     *
     * @return
     * The countReview
     */
    public Integer getCountReview() {
        return countReview;
    }

    /**
     *
     * @param countReview
     * The count_review
     */
    public void setCountReview(Integer countReview) {
        this.countReview = countReview;
    }

    /**
     *
     * @return
     * The distance
     */
    public String getDistance() {
        return distance;
    }

    /**
     *
     * @param distance
     * The distance
     */
    public void setDistance(String distance) {
        this.distance = distance;
    }

    /**
     *
     * @return
     * The orderOnline
     */
    public String getOrderOnline() {
        return orderOnline;
    }

    /**
     *
     * @param orderOnline
     * The order_online
     */
    public void setOrderOnline(String orderOnline) {
        this.orderOnline = orderOnline;
    }

    /**
     *
     * @return
     * The rating
     */
    public Double getRating() {
        return rating;
    }

    /**
     *
     * @param rating
     * The rating
     */
    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Integer getFavor() {
        return favor;
    }

    public void setFavor(Integer favor) {
        this.favor = favor;
    }

    public boolean isFavorite() {
        return favor != 0;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }
}

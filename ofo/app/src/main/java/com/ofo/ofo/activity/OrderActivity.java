package com.ofo.ofo.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.ofo.ofo.R;
import com.ofo.ofo.adapter.OrderAdapter;
import com.ofo.ofo.dialog.NoOnlineDeliverDialog;
import com.ofo.ofo.model.DishMenu;
import com.ofo.ofo.model.DishOrderItem;
import com.ofo.ofo.model.MenuOrderItem;
import com.ofo.ofo.model.OrderHeaderItem;
import com.ofo.ofo.model.ResMenu;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.GlobalData;
import com.ofo.ofo.util.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.FloatingActionButton;
import carbon.widget.RelativeLayout;
import eu.davidea.flexibleadapter.common.SmoothScrollLinearLayoutManager;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderActivity extends BaseActivity {

    public static Restaurant res;

    List<AbstractFlexibleItem> data = new ArrayList<>();

    List<ResMenu> calling;

    @BindView(R.id.btnCall)
    View btnCall;

    @BindView(R.id.tvCall)
    TextView tvCall;

    @BindView(R.id.root)
    View root;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edtSearch)
    EditText edtSearch;
    @BindView(R.id.group_search)
    RelativeLayout groupSearch;
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.tvBadge)
    TextView tvBadge;

    OrderAdapter adapter;
    @BindView(R.id.btnAdd)
    FloatingActionButton btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        ButterKnife.bind(this);

        btnCall.setVisibility(res.getOrderOnline().equals("0") ? View.VISIBLE : View.GONE);

        Drawable d = DrawableCompat.wrap(ContextCompat.getDrawable(this, R.drawable.ic_call));

        d.mutate();

        DrawableCompat.setTint(d, ContextCompat.getColor(this, R.color.carbon_white));

        tvCall.setCompoundDrawablesRelativeWithIntrinsicBounds(d, null, null, null);

        btnAdd.setVisibility(res.getOrderOnline().equals("0") ? View.GONE : View.VISIBLE);

        tvBadge.setVisibility(res.getOrderOnline().equals("0") ? View.GONE : View.VISIBLE);

        if (res.getOrderOnline().equals("0")) {

            new NoOnlineDeliverDialog().show(getSupportFragmentManager(), "no_online_deliver_frag");

        }

        GlobalData.res = res;

        data.add(new OrderHeaderItem("HD" + res.getId(), res));

        adapter = new OrderAdapter(data, this);

        adapter.setAnimationOnScrolling(true);

        adapter.setRemoveOrphanHeaders(false);

        list.setLayoutManager(new SmoothScrollLinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        list.setAdapter(adapter);

        list.setHasFixedSize(false);

        list.setItemAnimator(new DefaultItemAnimator() {

            @Override
            public boolean canReuseUpdatedViewHolder(@NonNull RecyclerView.ViewHolder viewHolder) {

                return true;

            }
        });

        adapter.setDisplayHeadersAtStartUp(true);

        adapter.setAutoScrollOnExpand(true);

        adapter.enableStickyHeaders();

        getData();

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (GlobalData.listDishOrder.size() > 0) {

            btnAdd.setEnabled(true);

        } else {

            btnAdd.setEnabled(false);

        }

        tvBadge.setText(GlobalData.listDishOrder.size() + "");

    }

    @Override
    public void onBackPressed() {

        GlobalData.listDishOrder = new ArrayList<>();

        super.onBackPressed();

    }

    @OnClick({R.id.btnBack, R.id.btnReset, R.id.btnClear, R.id.btnAdd, R.id.btnCall})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                GlobalData.listDishOrder = new ArrayList<>();

                finish();

                break;
            case R.id.btnReset:

                GlobalData.listDishOrder = new ArrayList<>();

                recreate();

                break;
            case R.id.btnClear:

                edtSearch.setText("");

                break;
            case R.id.btnAdd:

                ConfirmOrderActivity.res = res;

                startActivity(ConfirmOrderActivity.class);

                break;

            case R.id.btnCall:

                Util.dial(res.getPhoneNo(), root);

                break;

        }
    }

    @Override
    protected void getData() {
        super.getData();

        toastLoading();

        RestClient.get().getListResMenu(res.getId()).enqueue(new Callback<Result<List<ResMenu>>>() {
            @Override
            public void onResponse(Call<Result<List<ResMenu>>> call, Response<Result<List<ResMenu>>> response) {

                if (!ErrorUtil.foundAndHandledResponseError(response, root)) {

                    if (response.body().getData().size() < 1) {

                        toastOk();

                        return;

                    }

                    for (final ResMenu m : response.body().getData()) {

                        final MenuOrderItem menuItem = new MenuOrderItem("M" + m.getId(), m, null);

                        data.add(menuItem);

                        RestClient.get().getListDishMenu(m.getId()).enqueue(new Callback<Result<List<DishMenu>>>() {
                            @Override
                            public void onResponse(Call<Result<List<DishMenu>>> call, Response<Result<List<DishMenu>>> response) {

//                                    calling.remove(m);

                                if (!ErrorUtil.foundAndHandledResponseError(response, root)) {

                                    List<DishOrderItem> dish = new ArrayList<>();

                                    for (DishMenu d : response.body().getData()) {

                                        DishOrderItem dishItem = new DishOrderItem("M" + m.getId() + "D" + d.getId(), d);

                                        dishItem.setHeader(menuItem);

                                        dish.add(dishItem);


                                    }

                                    menuItem.setDish(dish);

                                }

                                if (data.indexOf(menuItem) == data.size() - 1) {

                                    toastOk();

                                    adapter.notifyDataSetChanged();

                                }

                            }

                            @Override
                            public void onFailure(Call<Result<List<DishMenu>>> call, Throwable t) {

//                                    calling.remove(m);

                                ErrorUtil.showCannotConnectError(root);

                            }
                        });

//                            calling.add(m);

                    }

                } else {

                    toastErr();

                }

            }

            @Override
            public void onFailure(Call<Result<List<ResMenu>>> call, Throwable t) {

                toastErr();

                ErrorUtil.showCannotConnectError(root);

            }
        });

    }

    @Override
    protected void displayData() {
        super.displayData();

        adapter.updateDataSet(data);

    }

}

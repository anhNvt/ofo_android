package com.ofo.ofo.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ofo.ofo.R;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.LinearLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostReviewResActivity extends BaseActivity {

    public static final int START_CODE = 7;

    public static Restaurant res;

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.rate_bar)
    RatingBar rateBar;
    @BindView(R.id.edtCmt)
    EditText edtCmt;
    @BindView(R.id.root)
    LinearLayout root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_review_res);
        ButterKnife.bind(this);

        tvTitle.setText(res.getName());

    }

    @OnClick({R.id.btnBack, R.id.btnSearch, R.id.btnPost})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnSearch:

                startActivity(SearchActivity.class);

                break;
            case R.id.btnPost:

                toastLoading();

                RestClient.get().postReviewRes(res.getId(), Data.getAccountInfo(this).getAccountId(), rateBar.getRating(), edtCmt.getText().toString().trim()).enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {

                        if (!ErrorUtil.foundAndHandledResponseError(response, root)) {

                            toastOk();

                            Util.showSnackBarInView(root, getString(R.string.post_res_cmt_success));

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    setResult(RESULT_OK);

                                    finish();

                                }
                            }, 500);

                        } else {

                            toastErr();

                        }

                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {

                        toastErr();

                        ErrorUtil.showCannotConnectError(root);

                    }
                });

                break;
        }
    }
}

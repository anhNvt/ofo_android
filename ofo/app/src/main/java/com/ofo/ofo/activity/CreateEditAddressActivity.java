package com.ofo.ofo.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.ofo.ofo.R;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.model.Address;
import com.ofo.ofo.model.AddressType;
import com.ofo.ofo.model.Area;
import com.ofo.ofo.model.City;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.GlobalData;
import com.ofo.ofo.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.Button;
import carbon.widget.LinearLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateEditAddressActivity extends BaseActivity {

    public static int MODE_ADD = 0;
    public static int MODE_EDIT = 1;


    public static Address address;

    @BindView(R.id.tvTitle)
    android.widget.TextView tvTitle;

    @BindView(R.id.root)
    LinearLayout root;

    @BindView(R.id.edtName)
    EditText edtName;

    @BindView(R.id.spinnerCity)
    Spinner spinnerCity;

    @BindView(R.id.spinnerArea)
    Spinner spinnerArea;

    @BindView(R.id.spinnerAddType)
    Spinner spinnerAddType;

    @BindView(R.id.edtStreet)
    EditText edtStreet;

    @BindView(R.id.edtBuilding)
    EditText edtBuilding;

    @BindView(R.id.edtFloor)
    EditText edtFloor;

    @BindView(R.id.edtPhone)
    EditText edtPhone;

    @BindView(R.id.edtDirection)
    EditText edtDirection;

    List<Area> areaList;
    List<AddressType> addressTypeList;

    List<String> aresas = new ArrayList<>();
    ArrayAdapter<String> adapterArea;

    List<String> cityList = new ArrayList<>();
    ArrayAdapter<String> adapterCity;

    List<String> addressTypes = new ArrayList<>();
    ArrayAdapter<String> adapterAddressType;

    Map<Integer, List<String>> areaCity = new HashMap<>();

    @BindView(R.id.btnSave)
    Button btnSave;

    @BindView(R.id.btnDelete)
    ImageView btnDelete;

    int mode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_edit_address);
        ButterKnife.bind(this);


        adapterCity = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner, R.id.tvSpinner, cityList);
        spinnerCity.setAdapter(adapterCity);

        adapterArea = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner, R.id.tvSpinner, aresas);
        spinnerArea.setAdapter(adapterArea);


        adapterAddressType = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner, R.id.tvSpinner, addressTypes);
        spinnerAddType.setAdapter(adapterAddressType);


        spinnerArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cityList.clear();
                cityList.addAll(getCityString(areaList.get(position).getCities()));
                adapterCity.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        loadCity();
        loadAddressType();

        mode = getIntent().getIntExtra(Const.KEY_SEND_ADDRESS_MODE, 0);
        if (mode == MODE_EDIT) {

            tvTitle.setText(getString(R.string.update_address));
            btnSave.setText(getString(R.string.update));
            btnDelete.setVisibility(View.VISIBLE);

            if (address != null) {


                edtName.setText(address.getAddressName());
                edtStreet.setText(address.getStreetName());
                edtFloor.setText(address.getFloor());
                edtBuilding.setText(address.getBuildingName());
                edtDirection.setText(address.getDirection());
                edtPhone.setText(address.getPhoneNo());


            }

        } else {
            tvTitle.setText(getString(R.string.add_new_address));
            btnSave.setText(getString(R.string.save));
            btnDelete.setVisibility(View.GONE);
        }


    }

    void initAreaCity() {
        if (address != null) {


            int areaid = Integer.parseInt(address.getAreaId());
            int areaIndex = 0;
            for (int i = 0; i < areaList.size(); i++) {
                if (areaList.get(i).getCountryId() == areaid) {
                    areaIndex = i;
                    break;
                }
            }

            spinnerArea.setSelection(areaIndex);

            int cityId = Integer.parseInt(address.getCityId());
            int cityIndex = 0;
            List<City> cities = areaList.get(areaIndex).getCities();
            for (int i = 0; i < cities.size(); i++) {
                if (cities.get(i).getId() == cityId) {
                    cityIndex = i;
                    break;
                }
            }
            spinnerCity.setSelection(cityIndex);

        }
    }

    void initAddressType() {

        if (address != null) {

        }

    }

    @OnClick(R.id.btnDelete)
    public void onDelete() {

//        AlertDialog.Builder dialog = new AlertDialog.Builder(CreateEditAddressActivity.this);
//        dialog.setMessage(getString(R.string.delete_address_confirm));
//        dialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                delete();
//            }
//        });
//
//        dialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//            }
//        });
//
//        dialog.show();

        Util.showBasicDialog(getSupportFragmentManager(), getString(R.string.are_you_sure_you_want_to_delete_this_address), getString(R.string.delete), new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                delete();

            }
        });

    }


    void delete() {
//        showProgressDialog(getString(R.string.deleting));

        toastLoading();

        RestClient.get().deleteAdd(address.getId(), Data.getAccountInfo(getBaseContext()).getAccountId()).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
//                closeProgressDialog();
                if (response.isSuccessful()) {

                    toastOk();

                    Util.showSnackBarInView(root, R.string.deleted_success);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    }, 500);
                } else {

                    toastErr();

                    Util.showSnackBarInView(root, response.message());
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
//                closeProgressDialog();

                toastErr();

                ErrorUtil.showCannotConnectError(root);
            }
        });
    }

    @OnClick(R.id.btnSave)
    public void onSave() {

        String errEmpty = getString(R.string.field_required);

        String name = edtName.getText().toString();
        int areaId = areaList.get(spinnerArea.getSelectedItemPosition()).getCountryId();
        int cityId = areaList.get(spinnerArea.getSelectedItemPosition()).getCities().get(spinnerCity.getSelectedItemPosition()).getId();
        int addTypeId = addressTypeList.get(spinnerAddType.getSelectedItemPosition()).getId();
        String street = edtStreet.getText().toString();
        String building = edtBuilding.getText().toString();
        String floor = edtFloor.getText().toString();
        String mobile = edtPhone.getText().toString();
        String direction = edtDirection.getText().toString();

        edtName.setError(null);
        edtStreet.setError(null);
        edtBuilding.setError(null);

        if (TextUtils.isEmpty(name)) {

            edtName.setError(errEmpty);
            edtName.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(street)) {

            edtStreet.setError(errEmpty);
            edtStreet.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(building)) {

            edtBuilding.setError(errEmpty);
            edtBuilding.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(floor)) {

            edtFloor.setError(errEmpty);
            edtFloor.requestFocus();
            return;
        }


        if (TextUtils.isEmpty(mobile)) {

            edtPhone.setError(errEmpty);
            edtPhone.requestFocus();
            return;
        }


        if (TextUtils.isEmpty(direction)) {

            edtDirection.setError(errEmpty);
            edtDirection.requestFocus();
            return;
        }


//        showProgressDialog(getString(mode == MODE_ADD ? R.string.adding_address : R.string.updating_address));

        toastLoading();

        if (mode == MODE_ADD) {
            RestClient.get().createAdd(Data.getAccountInfo(getBaseContext()).getAccountId(), name, cityId, areaId, addTypeId, street, building, floor, mobile, direction)
                    .enqueue(new Callback<Result>() {
                        @Override
                        public void onResponse(Call<Result> call, Response<Result> response) {
//                            closeProgressDialog();
                            if (response.isSuccessful()) {

                                toastOk();

                                Util.showSnackBarInView(root, R.string.add_success);
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        finish();
                                    }
                                }, 500);
                            } else {

                                toastErr();

                                Util.showSnackBarInView(root, response.message());
                            }
                        }

                        @Override
                        public void onFailure(Call<Result> call, Throwable t) {
//                            closeProgressDialog();

                            toastErr();

                            ErrorUtil.showCannotConnectError(root);
                        }
                    });
        } else {

            RestClient.get().updateAdd(address.getId(), Data.getAccountInfo(getBaseContext()).getAccountId(), name, cityId, areaId, addTypeId, street, building, floor, mobile, direction)
                    .enqueue(new Callback<Result>() {
                        @Override
                        public void onResponse(Call<Result> call, Response<Result> response) {
//                            closeProgressDialog();
                            if (response.isSuccessful()) {

                                toastOk();

                                Util.showSnackBarInView(root, R.string.update_successed);
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        finish();
                                    }
                                }, 500);
                            } else {

                                toastErr();

                                Util.showSnackBarInView(root, response.message());
                            }
                        }

                        @Override
                        public void onFailure(Call<Result> call, Throwable t) {

                            toastErr();

                            ErrorUtil.showCannotConnectError(root);

                        }
                    });

        }


    }

    void loadCity() {
//        showProgressDialog(getString(R.string.loading));

        toastLoading();

        RestClient.get().getListCity().enqueue(new Callback<Result<ArrayList<Area>>>() {
            @Override
            public void onResponse(Call<Result<ArrayList<Area>>> call, Response<Result<ArrayList<Area>>> response) {
//                closeProgressDialog();
                if (response.isSuccessful()) {

                    toastOk();

                    areaList = response.body().getData();
                    aresas.addAll(getAreaString(areaList));
                    adapterArea.notifyDataSetChanged();

                    cityList.addAll(getCityString(areaList.get(0).getCities()));
                    adapterCity.notifyDataSetChanged();

                    if (mode == MODE_EDIT) {
                        initAreaCity();
                    }


                } else {

                    toastErr();

                    Util.showSnackBarInView(root, response.message());
                }

            }

            @Override
            public void onFailure(Call<Result<ArrayList<Area>>> call, Throwable t) {
//                closeProgressDialog();

                toastErr();

                ErrorUtil.showCannotConnectError(root);
            }
        });
    }

    void loadAddressType() {
        RestClient.get().getListAddType(Data.getAccountInfo(getBaseContext()).getAccountId()).enqueue(new Callback<Result<List<AddressType>>>() {
            @Override
            public void onResponse(Call<Result<List<AddressType>>> call, Response<Result<List<AddressType>>> response) {

                if (response.isSuccessful()) {
                    addressTypeList = response.body().getData();
                    addressTypes.addAll(getAddressTypesString(response.body().getData()));
                    adapterAddressType.notifyDataSetChanged();
                    if (mode == MODE_EDIT) {
                        initAddressType();
                    }


                } else {
                    Util.showSnackBarInView(root, response.message());
                }

            }

            @Override
            public void onFailure(Call<Result<List<AddressType>>> call, Throwable t) {
                ErrorUtil.showCannotConnectError(root);
            }
        });
    }

    List<String> getAreaString(List<Area> areas) {
        List<String> areaString = new ArrayList<>();
        for (Area ae : areas) {

            if (Util.isar_Arabic(getApplicationContext())) {
                areaString.add(ae.getCountryNameAr());
            } else {
                areaString.add(ae.getCountryName());
            }


        }

        return areaString;
    }

    List<String> getCityString(List<City> cities) {
        List<String> citiesString = new ArrayList<>();
        for (City city : cities) {
            if (Util.isar_Arabic(getApplicationContext())) {
                citiesString.add(city.getNameAr());
            } else {
                citiesString.add(city.getName());
            }

        }

        return citiesString;
    }

    List<String> getAddressTypesString(List<AddressType> addressTypes) {
        List<String> addTypeString = new ArrayList<>();
        for (AddressType addressType : addressTypes) {
            if (Util.isar_Arabic(getApplicationContext())) {
                addTypeString.add(addressType.getNameAr());
            } else {
                addTypeString.add(addressType.getName());
            }

        }

        return addTypeString;
    }

    @OnClick({R.id.btnBack, R.id.btnExit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnExit:

                finish();

                break;

        }
    }


    @Override
    public void onBackPressed() {

        address = null;

        super.onBackPressed();
    }
}

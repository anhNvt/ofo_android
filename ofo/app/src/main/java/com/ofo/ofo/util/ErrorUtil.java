package com.ofo.ofo.util;

import android.content.Context;
import android.view.View;

import com.ofo.ofo.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Response;

/**
 * Created by cat on 5/8/16.
 */
public class ErrorUtil {

    public static boolean foundAndHandledResponseError(Context ctx, Response response, View root) {

        if (response == null || (response.body() == null && response.errorBody() == null)) {

            Util.showSnackBarInView(root, ctx.getString(R.string.res_err));

            return true;

        }

        if (response.errorBody() != null) {

            try {
                try {
                    JSONObject err = new JSONObject(response.errorBody().string());

                    if (err.optJSONArray("error") != null) {

                        Util.showSnackBarInView(root, err.getJSONArray("error").getString(0));

                    } else {
                        Util.showSnackBarInView(root, err.getString("error"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return true;

        }

        return false;

    }

    public static boolean foundAndHandledResponseError(Response response, View root) {

        Context ctx = root.getContext();

        if (response == null || (response.body() == null && response.errorBody() == null)) {

            Util.showSnackBarInView(root, ctx.getString(R.string.res_err));

            return true;

        }

        if (response.errorBody() != null) {

            try {
                try {
                    JSONObject err = new JSONObject(response.errorBody().string());

                    if (err.optJSONArray("error") != null) {

                        Util.showSnackBarInView(root, err.getJSONArray("error").getString(0));

                    } else {
                        Util.showSnackBarInView(root, err.getString("error"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                    Util.showSnackBarInView(root, ctx.getString(R.string.res_err));

                }
            } catch (IOException e) {
                e.printStackTrace();

                Util.showSnackBarInView(root, ctx.getString(R.string.res_err));

            }

            return true;

        }

        return false;

    }

    public static void showCannotConnectError(Context ctx, View v) {

        Util.showSnackBarInView(v, ctx.getString(R.string.can_not_connect));

    }

    public static void showCannotConnectError(View v) {

        Util.showSnackBarInView(v, R.string.can_not_connect);

    }

}

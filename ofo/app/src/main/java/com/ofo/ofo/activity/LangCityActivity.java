package com.ofo.ofo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.ofo.ofo.R;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.LocaleHelper;
import com.ofo.ofo.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LangCityActivity extends BaseActivity {

    @BindView(R.id.btnBack)
    ImageView btnBack;

    @BindView(R.id.btnExit)
    ImageView btnExit;

    @BindView(R.id.btnChooseCity)
    Button btnCity;

    @BindView(R.id.btnEng)
    Button btnEng;

    @BindView(R.id.btnEngSelected)
    Button btnEngSelected;

    @BindView(R.id.btnArb)
    Button btnArab;

    @BindView(R.id.btnArbSelected)
    Button btnArabSelected;

    @OnClick(R.id.btnChooseCity) void chooseCity() {

        startActivity(new Intent(this, AreaCityActivity.class));

    }

    @OnClick(R.id.btnDone) void done() {

        startActivity(new Intent(this, SignUpActivity.class));

        finish();

    }

    @OnClick({R.id.btnEng, R.id.btnArb}) void changeLanguage(View v) {

        switch (v.getId()) {

            case R.id.btnArb:

                LocaleHelper.setLocale(getBaseContext(), LocaleHelper.LOCALE_ARAB);

                recreate();

                break;
            case R.id.btnEng:

                LocaleHelper.setLocale(getBaseContext(), LocaleHelper.LOCALE_ENG);

                recreate();

                break;

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_lang_city);

        ButterKnife.bind(this);

        btnBack.setVisibility(View.INVISIBLE);

        btnExit.setVisibility(View.INVISIBLE);

        String localeCode = LocaleHelper.getLanguage(this, LocaleHelper.LOCALE_ENG);

        btnEngSelected.setVisibility(localeCode.equals(LocaleHelper.LOCALE_ENG) ? View.VISIBLE : View.GONE);

        btnArabSelected.setVisibility(localeCode.equals(LocaleHelper.LOCALE_ARAB) ? View.VISIBLE : View.GONE);

    }

    @Override
    protected void onResume() {
        super.onResume();

        btnCity.setText(Util.getPrefManager(this).get(Const.KEY_PREF_CITY_NAME).defaultValue("").go());

    }
}

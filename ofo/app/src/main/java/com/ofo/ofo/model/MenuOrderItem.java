package com.ofo.ofo.model;

import android.support.annotation.MenuRes;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ofo.ofo.R;
import com.ofo.ofo.activity.OrderActivity;
import com.ofo.ofo.viewholer.RowMenuOrderHolder;

import java.util.ArrayList;
import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.IExpandable;
import eu.davidea.flexibleadapter.items.IHeader;

/**
 * Created by cat on 5/17/16.
 */
public class MenuOrderItem extends AbstractModelItem<RowMenuOrderHolder> implements IExpandable<RowMenuOrderHolder, DishOrderItem>, IHeader<RowMenuOrderHolder> {

    private boolean isExpanded = false;

    ResMenu menu;

    private List<DishOrderItem> dish = new ArrayList<>();

    public MenuOrderItem(String id, ResMenu data, List<DishOrderItem> subItems) {
        super(id);

        this.menu = data;

        if (subItems != null) {

            this.dish = subItems;

        }

        setHidden(false);

        setExpanded(false);

        setSelectable(false);

    }

    public List<DishOrderItem> getDish() {
        return dish;
    }

    public void setDish(List<DishOrderItem> dish) {
        this.dish = dish;
    }

    @Override
    public boolean isExpanded() {
        return isExpanded;
    }

    @Override
    public void setExpanded(boolean expanded) {

        this.isExpanded = expanded;

    }

    @Override
    public int getExpansionLevel() {
        return 0;
    }

    @Override
    public List<DishOrderItem> getSubItems() {
        return dish;
    }

    @Override
    public int getLayoutRes() {

        return R.layout.row_res_menu;

    }

    @Override
    public RowMenuOrderHolder createViewHolder(FlexibleAdapter adapter, LayoutInflater inflater, ViewGroup parent) {

        return new RowMenuOrderHolder(inflater.inflate(getLayoutRes(), parent, false), adapter);

    }

    @Override
    public void bindViewHolder(FlexibleAdapter adapter, RowMenuOrderHolder holder, int position, List payloads) {

        if (payloads.size() > 0) {

        } else {

            holder.tvName.setText(menu.getName());

        }

        if (isExpanded()) {

            holder.tvName.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.colorPrimary));

            holder.tvName.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.carbon_white));

            if (OrderActivity.res.getOrderOnline().equals("0")) {

                holder.tvName.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.ic_up), null);

            } else {

                holder.tvName.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.ic_order_white_small), null);

            }

        } else {

            holder.tvName.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.colorGrey));

            holder.tvName.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.colorText));

            if (OrderActivity.res.getOrderOnline().equals("0")) {

                holder.tvName.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.ic_down), null);

            } else {

                holder.tvName.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.ic_order), null);

            }

        }

    }

}

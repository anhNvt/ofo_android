package com.ofo.ofo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.ofo.ofo.R;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.model.AccountInfo;
import com.ofo.ofo.model.AccountInfoLogin;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.service.OfoInstantIdService;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.ImageView;
import carbon.widget.TextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hung Pham on 7/5/2016.
 */
public class LoginSecondActivity extends BaseActivity {

    public static final int ADD_RES = 0;

    public static final int MY_PROFILE = 1;

    public static final int RE_ORDER = 2;

    public static final int RATE_MY_ORDER = 3;

    public static final int NOTIFICATION = 4;

    public static final int WISH_LIST = 5;

    public static final int UPLOAD_DISH = 6;

    public static final int ADD_PHOTO = 7;

    public static final int REVIEW = 8;

    @BindView(R.id.login_form_second)
    View loginFormSecond;
    @BindView(R.id.imgViewIcon)
    ImageView imgViewIcon;
    @BindView(R.id.tvPrompt)
    TextView tvPrompt;

    @OnClick(R.id.btnExitSecond)
    void exit() {

        finish();
    }

    @OnClick(R.id.btnLoginSecond)
    void goToLogin() {

        startActivity(LoginActivity.class);

        finish();
    }

    @OnClick(R.id.btnRegisterSecond)
    void goToRegister() {

        startActivity(SignUpActivity.class);

        finish();
    }

    @OnClick(R.id.btnLoginFacebookSecond)
    void goToLoginFacebook() {

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                loginFacebook(loginResult);
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }

    CallbackManager callbackManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_login_second);

        ButterKnife.bind(this);

        int code = 5;

        try {

            code = getIntent().getExtras().getInt("caller");

        } catch (NullPointerException e) {

            e.printStackTrace();

        }

        String text = getString(R.string.please_login_to) + " ";

        int img_res_id = R.drawable.icon_cicler_heart;

        switch (code) {

            case ADD_RES:

                text += getString(R.string.add_restaurant).toLowerCase();

                img_res_id = R.drawable.ic_add_restaurant;

                break;

            case MY_PROFILE:

                text += getString(R.string.view_and_edit_profile).toLowerCase();

                img_res_id = R.drawable.ic_profile;

                break;

            case RE_ORDER:

                text += getString(R.string.view_orders).toLowerCase();

                img_res_id = R.drawable.ic_re_order;

                break;

            case RATE_MY_ORDER:

                text += getString(R.string.rate_your_order).toLowerCase();

                img_res_id = R.drawable.ic_rate_order;

                break;

            case NOTIFICATION:

                text += getString(R.string.view_notification).toLowerCase();

                img_res_id = R.drawable.ic_notification;

                break;

            case WISH_LIST:

                text += getString(R.string.save_and_view_wish_list).toLowerCase();

                break;

            case UPLOAD_DISH:

                text += getString(R.string.upload_dish).toLowerCase();

                img_res_id = R.drawable.ic_dish_login;

                break;

            case ADD_PHOTO:

                text += getString(R.string.add_photo).toLowerCase();

                img_res_id = R.drawable.ic_photo_login;

                break;

            case REVIEW:

                text += getString(R.string.review_res).toLowerCase();

                img_res_id = R.drawable.ic_review_login;

                break;

        }

        tvPrompt.setText(text);

        imgViewIcon.setImageResource(img_res_id);

        callbackManager = CallbackManager.Factory.create();

    }

    void login(String mail, String name, String socialId) {

        toastLoading();

        RestClient.get().loginFacebook(mail, name, socialId).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

                if (response == null || (response.body() == null && response.errorBody() == null)) {

                    toastErr();

                    Util.showSnackBarInView(loginFormSecond, getString(R.string.res_err));

                    return;

                }

                if (response.errorBody() != null) {

                    toastErr();

                    try {
                        try {
                            JSONObject err = new JSONObject(response.errorBody().string());

                            if (err.optJSONArray("error") != null) {

                                Util.showSnackBarInView(loginFormSecond, err.getJSONArray("error").getString(0));

                            } else {
                                Util.showSnackBarInView(loginFormSecond, err.getString("error"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;

                }

                AccountInfoLogin infoLog = response.body().getInfo();

                Data.createOrUpdate(getBaseContext(), infoLog);

                RestClient.get().getAccountInfo(infoLog.getAccountId()).enqueue(new Callback<Result<AccountInfo>>() {
                    @Override
                    public void onResponse(Call<Result<AccountInfo>> call, Response<Result<AccountInfo>> response) {

                        if (!ErrorUtil.foundAndHandledResponseError(response, loginFormSecond)) {

                            AccountInfo info = response.body().getData();

                            Data.createOrUpdate(getBaseContext(), info);

                            if (info.getCityId() == null || info.getCityId().length() < 1) {

                                changeCity();

                            } else {

                                toastOk();

                                Util.getPrefManager(getBaseContext()).set(Const.KEY_PREF_CITY_ID).value(info.getCityId()).go();

                                Util.getPrefManager(getBaseContext()).set(Const.KEY_PREF_CITY_NAME).value(info.getCityName()).go();

                                Util.getPrefManager(getBaseContext()).set(Const.KEY_PREF_SIGNED_IN).value(true).go();

                                OfoInstantIdService.regToken(getBaseContext());

                                setResult(RESULT_OK);
                                finish();

                            }

                        } else {

                            toastErr();

                        }

                    }

                    @Override
                    public void onFailure(Call<Result<AccountInfo>> call, Throwable t) {

                        toastErr();

                        ErrorUtil.showCannotConnectError(loginFormSecond);

                    }
                });


            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

                toastErr();

                Util.showSnackBarInView(loginFormSecond, getString(R.string.can_not_connect));

            }
        });

    }

    void loginFacebook(LoginResult loginResult) {

        GraphRequest request = GraphRequest.newMeRequest
                (loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        // Application code
                        Log.v("LoginActivity", response.toString());
                        //System.out.println("Check: " + response.toString());
                        try {
                            String id = object.getString("id");
                            String name = object.getString("name");
                            String email = object.getString("email");

                            login(email, name, id);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    void changeCity() {
        final int id = Util.getPrefManager(this).get(Const.KEY_PREF_CITY_ID).defaultValue(1).go();

        toastLoading();

        RestClient.get().changeCity(Data.getAccountInfo(getBaseContext()).getAccountId(), id).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

                toastOk();

                AccountInfo info = Data.getAccountInfo(getBaseContext());

                Data.getRealmInstance(getBaseContext()).beginTransaction();

                info.setCityId(id + "");

                info.setCityName(Util.getPrefManager(getBaseContext()).get(Const.KEY_PREF_CITY_NAME).defaultValue("Al-Anbar").go());

                Data.getRealmInstance(getBaseContext()).commitTransaction();

                Util.getPrefManager(getBaseContext()).set(Const.KEY_PREF_SIGNED_IN).value(true).go();

                OfoInstantIdService.regToken(getBaseContext());

                setResult(RESULT_OK);

                finish();

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

                toastErr();

//                ErrorUtil.showCannotConnectError(content);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}

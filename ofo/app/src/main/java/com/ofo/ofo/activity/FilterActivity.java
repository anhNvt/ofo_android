package com.ofo.ofo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;

import com.ofo.ofo.R;
import com.ofo.ofo.model.FilterField;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class FilterActivity extends BaseActivity {

    public static final int START_CODE = 2;

    @BindView(R.id.chkBoxLowToHigh)
    CheckBox chkBoxLowToHigh;
    @BindView(R.id.chkBoxHighToLow)
    CheckBox chkBoxHighToLow;
    @BindView(R.id.chkBoxOffer)
    CheckBox chkBoxOffer;
    @BindView(R.id.spinnerOrderAmount)
    Spinner spinnerOrderAmount;
    @BindView(R.id.spinnerDeliverTime)
    Spinner spinnerDeliverTime;
    @BindView(R.id.chkBoxRate)
    CheckBox chkBoxRate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        ButterKnife.bind(this);

    }

    @OnClick({R.id.btnBack, R.id.btnSearch, R.id.btnApply, R.id.btnReset})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnSearch:

                startActivity(SearchActivity.class);

                break;
            case R.id.btnApply:

                int price = chkBoxLowToHigh.isChecked() ? 1 : 2;

                int offer = chkBoxOffer.isChecked() ? 1 : 2;

                int mini_amount = spinnerOrderAmount.getSelectedItemPosition() + 1;

                int deliver_time = spinnerDeliverTime.getSelectedItemPosition() + 1;

                int rate = chkBoxRate.isChecked() ? 1 : 2;

                FilterField field = new FilterField(price, offer, mini_amount, deliver_time, rate);

                Intent i = new Intent();

                i.putExtra("filter_field", field);

                setResult(RESULT_OK, i);

                finish();

                break;
            case R.id.btnReset:

                reset();

                break;
        }
    }

    @OnCheckedChanged({R.id.chkBoxLowToHigh, R.id.chkBoxHighToLow}) void changeState(CompoundButton v) {

        switch (v.getId()) {

            case R.id.chkBoxLowToHigh:

                chkBoxHighToLow.setChecked(!chkBoxLowToHigh.isChecked());

                break;

            case R.id.chkBoxHighToLow:

                chkBoxLowToHigh.setChecked(!chkBoxHighToLow.isChecked());

                break;

        }

    }

    void reset() {

        chkBoxLowToHigh.setChecked(true);

        chkBoxHighToLow.setChecked(false);

        chkBoxOffer.setChecked(false);

        spinnerOrderAmount.setSelection(0);

        spinnerDeliverTime.setSelection(0);

        chkBoxRate.setChecked(false);

    }

}

package com.ofo.ofo.fragment;

import android.view.View;

import com.bumptech.glide.Glide;
import com.ofo.ofo.R;
import com.ofo.ofo.activity.OrderActivity;
import com.ofo.ofo.activity.ResDetailActivity;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.util.Util;
import com.ofo.ofo.viewholer.RowResHolder;

/**
 * Created by cat on 5/7/16.
 */
public class RestaurantListFragment extends ListFragment<Restaurant, RowResHolder> {

    int expandedPos = 0;

    @Override
    protected int getRowLayoutResId() {

        return R.layout.row_res;

    }

    @Override
    protected RowResHolder inflateViewHolder(View view) {

        return new RowResHolder(view);

    }

    @Override
    protected void bindDataToHolder(RowResHolder holder, final Restaurant data, final int position) {

        super.bindDataToHolder(holder, data, position);

        Glide.with(this).load(data.getBannerUrl()).into(holder.imgViewOverlayCover);

        Glide.with(this).load(data.getBannerUrl()).into(holder.imgViewCover);

        holder.tvOverlayName.setText(data.getName());

        holder.tvOverlayAddress.setText(data.getAddress());

        holder.tvOverlayPromo.setText(data.getDiscount());

        holder.tvName.setText(data.getName());

        holder.tvAddress.setText(data.getAddress());

        holder.tvPromo.setText(data.getDiscount());

        holder.groupRate.setVisibility(data.getRating() > 0 ? View.VISIBLE : View.GONE);

        holder.tvRating.setText(Util.roundToString(data.getRating(), "#.#"));

        holder.progressBarRating.setProgress((float) (data.getRating()/5));

        holder.btnMenu.setVisibility(data.getOrderOnline().equals("0") ? View.VISIBLE : View.GONE);

        holder.btnOrder.setVisibility(data.getOrderOnline().equals("1") ? View.VISIBLE : View.GONE);

        holder.groupOverlayPromo.setVisibility(data.getPromotion() ? View.VISIBLE : View.GONE);

        holder.groupPromo.setVisibility(data.getPromotion() ? View.VISIBLE : View.GONE);

        holder.overlayRow.setVisibility(position == expandedPos ? View.GONE : View.VISIBLE);

        holder.row.setVisibility(position == expandedPos ? View.VISIBLE : View.GONE);

        holder.overlayRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandedPos = position;

                list.getAdapter().notifyDataSetChanged();

            }
        });

        holder.row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ResDetailActivity.res = data;

                startActivity(ResDetailActivity.class);

            }
        });

        holder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ResDetailActivity.res = data;

                startActivity(ResDetailActivity.class);

            }
        });

        holder.btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Util.dial(data.getPhoneNo(), getView());

            }
        });

        holder.btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                OrderActivity.res = data;

                startActivity(OrderActivity.class);

            }
        });

        holder.btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                OrderActivity.res = data;

                startActivity(OrderActivity.class);

            }
        });

    }
}

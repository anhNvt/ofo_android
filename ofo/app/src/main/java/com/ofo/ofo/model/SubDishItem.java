package com.ofo.ofo.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ofo.ofo.R;
import com.ofo.ofo.viewholer.RowSubDishHolder;

import java.util.ArrayList;
import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.IExpandable;
import eu.davidea.flexibleadapter.items.IHeader;

/**
 * Created by cat on 7/21/16.
 */
public class SubDishItem extends AbstractModelItem<RowSubDishHolder> implements IExpandable<RowSubDishHolder, SubDishOptionItem>, IHeader<RowSubDishHolder> {

    boolean isExpanded = false;

    SubDish data;

    List<SubDishOptionItem> options = new ArrayList<>();

    public SubDishItem(String id, SubDish data, List<SubDishOptionItem> options) {

        super(id);

        this.data = data;

        if (options != null) {

            this.options = options;

        }

        setHidden(false);

        setExpanded(false);

        setSelectable(false);

    }

    public List<SubDishOptionItem> getOptions() {
        return options;
    }

    public void setOptions(List<SubDishOptionItem> options) {
        this.options = options;
    }

    @Override
    public boolean isExpanded() {
        return isExpanded;
    }

    @Override
    public void setExpanded(boolean expanded) {

        this.isExpanded = expanded;

    }

    @Override
    public int getExpansionLevel() {
        return 0;
    }

    @Override
    public List<SubDishOptionItem> getSubItems() {

        return options;

    }

    @Override
    public int getLayoutRes() {

        return R.layout.row_sub_dish;

    }

    @Override
    public RowSubDishHolder createViewHolder(FlexibleAdapter adapter, LayoutInflater inflater, ViewGroup parent) {

        return new RowSubDishHolder(inflater.inflate(getLayoutRes(), parent, false), adapter);

    }

    @Override
    public void bindViewHolder(FlexibleAdapter adapter, RowSubDishHolder holder, int position, List payloads) {

        Context ctx = holder.itemView.getContext();

        holder.tvName.setText(data.getName());

        holder.tvSelectLimit.setText(options.size() < 1 ? "" : "(" + ctx.getString(R.string.select) + " " + ctx.getString(R.string.min_cap) + " 0, " + ctx.getString(R.string.max_cap) + " " + options.size() + ")");

        holder.btnArr.setImageResource(isExpanded() ? R.drawable.ic_arr_down : R.drawable.ic_down);

    }
}
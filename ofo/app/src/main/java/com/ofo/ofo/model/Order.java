package com.ofo.ofo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by eo_cuong on 5/19/16.
 */
public class Order {

    @SerializedName("order_id")
    @Expose
    private int orderId;
    @SerializedName("account_id")
    @Expose
    private String accountId;
    @SerializedName("restaurant_id")
    @Expose
    private String restaurantId;
    @SerializedName("restaurant_name")
    @Expose
    private String restaurantName;
    @SerializedName("restaurant_name_ar")
    @Expose
    private String restaurantNameAr;


    @SerializedName("restaurant_promotion")
    @Expose
    private String restaurantPromotion;
    @SerializedName("restaurant_discount")
    @Expose
    private String restaurantDiscount;
    @SerializedName("restaurant_banner_url")
    @Expose
    private String restaurantBannerUrl;



    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("phone_no")
    @Expose
    private String phoneNo;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("street_name")
    @Expose
    private String streetName;
    @SerializedName("building_name")
    @Expose
    private String buildingName;
    @SerializedName("total_amount")
    @Expose
    private String totalAmount;
    @SerializedName("order_time")
    @Expose
    private String orderTime;

    @SerializedName("items")
    @Expose
    private List<OrderItem> orderItems;


    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    /**
     * @return The orderId
     */
    public int getOrderId() {
        return orderId;
    }

    /**
     * @param orderId The order_id
     */
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    /**
     * @return The accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * @param accountId The account_id
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * @return The restaurantId
     */
    public String getRestaurantId() {
        return restaurantId;
    }

    /**
     * @param restaurantId The restaurant_id
     */
    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    /**
     * @return The restaurantName
     */
    public String getRestaurantName() {
        return restaurantName;
    }

    /**
     * @param restaurantName The restaurant_name
     */
    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    /**
     * @return The restaurantNameAr
     */
    public String getRestaurantNameAr() {
        return restaurantNameAr;
    }

    /**
     * @param restaurantNameAr The restaurant_name_ar
     */
    public void setRestaurantNameAr(String restaurantNameAr) {
        this.restaurantNameAr = restaurantNameAr;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The phoneNo
     */
    public String getPhoneNo() {
        return phoneNo;
    }

    /**
     * @param phoneNo The phone_no
     */
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    /**
     * @return The city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return The streetName
     */
    public String getStreetName() {
        return streetName;
    }

    /**
     * @param streetName The street_name
     */
    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    /**
     * @return The buildingName
     */
    public String getBuildingName() {
        return buildingName;
    }

    /**
     * @param buildingName The building_name
     */
    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    /**
     * @return The totalAmount
     */
    public String getTotalAmount() {
        return totalAmount;
    }

    /**
     * @param totalAmount The total_amount
     */
    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * @return The orderTime
     */
    public String getOrderTime() {
        return orderTime;
    }

    /**
     * @param orderTime The order_time
     */
    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    /**
     *
     * @return
     * The restaurantPromotion
     */
    public String getRestaurantPromotion() {
        return restaurantPromotion;
    }

    /**
     *
     * @param restaurantPromotion
     * The restaurant_promotion
     */
    public void setRestaurantPromotion(String restaurantPromotion) {
        this.restaurantPromotion = restaurantPromotion;
    }

    /**
     *
     * @return
     * The restaurantDiscount
     */
    public String getRestaurantDiscount() {
        return restaurantDiscount;
    }

    /**
     *
     * @param restaurantDiscount
     * The restaurant_discount
     */
    public void setRestaurantDiscount(String restaurantDiscount) {
        this.restaurantDiscount = restaurantDiscount;
    }

    /**
     *
     * @return
     * The restaurantBannerUrl
     */
    public String getRestaurantBannerUrl() {
        return restaurantBannerUrl;
    }

    /**
     *
     * @param restaurantBannerUrl
     * The restaurant_banner_url
     */
    public void setRestaurantBannerUrl(String restaurantBannerUrl) {
        this.restaurantBannerUrl = restaurantBannerUrl;
    }

}

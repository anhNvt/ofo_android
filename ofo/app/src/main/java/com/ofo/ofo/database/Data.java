package com.ofo.ofo.database;

import android.content.Context;

import com.ofo.ofo.model.AccountInfo;
import com.ofo.ofo.model.AccountInfoLogin;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by cat on 5/2/16.
 */
public class Data {

    private static Realm realm;

    public static Realm getRealmInstance(Context ctx) {

        if (realm == null) {

            RealmConfiguration config = new RealmConfiguration.Builder(ctx)
                    .schemaVersion(0)
                    .migration(new Migration())
                    .build();

            realm = Realm.getInstance(config);

        }

        return realm;

    }

    public static RealmObject findFirst(Context ctx, Class c) {

        return getRealmInstance(ctx).where(c).findFirst();

    }

    public static RealmResults findAll(Context ctx, Class c) {

        return getRealmInstance(ctx).where(c).findAll();

    }

    public static void createOrUpdate(Context ctx, RealmObject item) {

        getRealmInstance(ctx).beginTransaction();

        getRealmInstance(ctx).copyToRealmOrUpdate(item);

        getRealmInstance(ctx).commitTransaction();

    }

    public static void createOrUpdate(Context ctx, List items) {

        getRealmInstance(ctx).beginTransaction();

        getRealmInstance(ctx).copyToRealmOrUpdate(items);

        getRealmInstance(ctx).commitTransaction();

    }

    public static void deleteAll(Context ctx) {

        getRealmInstance(ctx).beginTransaction();

        getRealmInstance(ctx).deleteAll();

        getRealmInstance(ctx).commitTransaction();

    }

    public static AccountInfo getAccountInfo(Context ctx) {

        return (AccountInfo) findFirst(ctx, AccountInfo.class);

    }

    public static AccountInfoLogin getAccountInfoLogin(Context ctx) {

        return (AccountInfoLogin) findFirst(ctx, AccountInfoLogin.class);

    }

}

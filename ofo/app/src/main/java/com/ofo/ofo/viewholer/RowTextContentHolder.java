package com.ofo.ofo.viewholer;

import android.view.View;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.ofo.ofo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import carbon.widget.TextView;

/**
 * Created by cat on 7/11/16.
 */
public class RowTextContentHolder extends UltimateRecyclerviewViewHolder {

    @BindView(R.id.tvContent)
    public TextView tvContent;

    public RowTextContentHolder(View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);

    }
}

package com.ofo.ofo.activity;

import android.media.Rating;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ofo.ofo.R;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.model.Order;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.GsonUtils;
import com.ofo.ofo.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.LinearLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eo_cuong on 5/19/16.
 */
public class RatingOrderActivity extends BaseActivity {

    @BindView(R.id.tvTitle)
    android.widget.TextView tvTitle;

    @BindView(R.id.imvRestaurant)
    ImageView imvRestaurant;


    @BindView(R.id.tvRestarantName)
    TextView tvRestarantName;

    @BindView(R.id.progressRating)
    ProgressBar progressRating;

    @BindView(R.id.rateQuantity)
    RatingBar rateQuantity;

    @BindView(R.id.rateDelivery)
    RatingBar rateDelivery;

    @BindView(R.id.rateRestaurant)
    RatingBar rateRestaurant;

    @BindView(R.id.edtCmt)
    EditText edtCmt;

    @BindView(R.id.btnSubmitRating)
    TextView btnSubmitRating;

    @BindView(R.id.root)
    LinearLayout root;


    Order order;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_order);
        ButterKnife.bind(this);


        tvTitle.setText(getString(R.string.rate_my_order));

        String data = getIntent().getStringExtra(Const.KEY_SEND_ORDER);
        if (!TextUtils.isEmpty(data)) {
            order = GsonUtils.String2Object(data, Order.class);
        }

        Glide.with(getApplicationContext()).load(order.getRestaurantBannerUrl()).into(imvRestaurant);
        tvRestarantName.setText(Util.isar_Arabic(getApplicationContext()) ? order.getRestaurantNameAr() : order.getRestaurantName());


        progressRating.setMax(15);

        rateQuantity.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                calculateChange();

            }
        });

        rateDelivery.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                calculateChange();
            }
        });

        rateRestaurant.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                calculateChange();
            }
        });

    }

    void calculateChange() {
        int process = (int) (rateQuantity.getRating() + rateRestaurant.getRating() + rateDelivery.getRating());
        progressRating.setProgress(process);
    }

    @OnClick(R.id.btnSubmitRating)
    public void submitClick(View v) {
        rating();
    }

    void rating() {

//        showProgressDialog(getString(R.string.rating));

        toastLoading();

        RestClient.get().ratingOrder(order.getOrderId(), Data.getAccountInfo(getApplicationContext()).getAccountId(), (int) rateQuantity.getRating(), (int) rateDelivery.getRating(), (int) rateRestaurant.getRating(), edtCmt.getText().toString()).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

//                closeProgressDialog();
                if (response.isSuccessful()) {

                    toastOk();

                    Util.showSnackBarInView(root, getString(R.string.rate_succes));

                } else {

                    toastErr();

                    Util.showSnackBarInView(root, response.message());
                }

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
//                closeProgressDialog();

                toastErr();

                ErrorUtil.showCannotConnectError(root);

            }
        });
    }

    @OnClick({R.id.btnBack, R.id.btnExit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnExit:

                finish();

                break;

        }
    }
}

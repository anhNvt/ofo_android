package com.ofo.ofo.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.bumptech.glide.Glide;
import com.ofo.ofo.R;
import com.ofo.ofo.activity.ViewImageActivity;
import com.ofo.ofo.model.ResPhoto;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.viewholer.RowResPhotoHolder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by cat on 5/14/16.
 */
public class ResPhotosFragment extends ListFragment<List<ResPhoto>, RowResPhotoHolder> {

    public static Restaurant res;

    List<ResPhoto> allImages = new ArrayList<>();

    @Override
    protected int getRowLayoutResId() {

        return R.layout.row_res_photo;

    }

    @Override
    protected RowResPhotoHolder inflateViewHolder(View view) {
        return new RowResPhotoHolder(view);
    }

    @Override
    protected void bindDataToHolder(RowResPhotoHolder holder, final List<ResPhoto> data, final int position) {

        try {

            Glide.with(this).load(data.get(0).getImageUrl()).into(holder.imgView1);

            holder.imgView1.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    viewImages(4*position);

                }

            });

            Glide.with(this).load(data.get(1).getImageUrl()).into(holder.imgView2);

            holder.imgView2.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    viewImages(4*position + 1);

                }

            });

            Glide.with(this).load(data.get(2).getImageUrl()).into(holder.imgView3);

            holder.imgView3.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    viewImages(4*position + 2);

                }

            });

            Glide.with(this).load(data.get(3).getImageUrl()).into(holder.imgView4);

            holder.imgView4.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    viewImages(4*position + 3);

                }

            });

        } catch (IndexOutOfBoundsException e) {



        }

    }

    public void getListPhoto(Call<Result<List<ResPhoto>>> call) {

      call.enqueue(new Callback<Result<List<ResPhoto>>>() {
            @Override
            public void onResponse(Call<Result<List<ResPhoto>>> call, Response<Result<List<ResPhoto>>> response) {

                if (!ErrorUtil.foundAndHandledResponseError(getContext(), response, getView())) {

                    allImages = response.body().getData();

                    setListData(makeGroupPhoto(response.body().getData()));

                }

            }

            @Override
            public void onFailure(Call<Result<List<ResPhoto>>> call, Throwable t) {

                ErrorUtil.showCannotConnectError(getView());

            }
        });

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (listData == null || willReloadData()) {

            getListPhoto(RestClient.get().getListResPhoto(res.getId()));

            setReloadData(false);

        } else {

            setAdapter();

        }

    }

    private List<List<ResPhoto>> makeGroupPhoto(List<ResPhoto> data) {

        List<List<ResPhoto>> photos = new ArrayList<>();

        List<ResPhoto> tmp = new ArrayList<>();

        tmp.addAll(data);

        int i = 0;

        while (i + 4 <= data.size()) {

            photos.add(data.subList(i, i+4));

            tmp.subList(0, 4).clear();

            i = i + 4;

           }

        if (tmp.size() > 0) {

            photos.add(tmp);

        }

        return photos;

    }

    void viewImages(int pos) {

        ViewImageActivity.images = allImages;

        ViewImageActivity.currentPos = pos;

        startActivity(ViewImageActivity.class);

    }

}

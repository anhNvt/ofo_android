package com.ofo.ofo.activity;

import android.os.Bundle;
import android.view.View;

import com.ofo.ofo.R;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.fragment.WishListResFragment;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WishListActivity extends BaseActivity {

    List data = new ArrayList();

    @BindView(R.id.btnBack)
    ImageView btnBack;
    @BindView(R.id.btnSearch)
    ImageView btnSearch;
    @BindView(R.id.root)
    LinearLayout root;

    WishListResFragment frag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);
        ButterKnife.bind(this);

        frag = (WishListResFragment) getSupportFragmentManager().findFragmentById(R.id.frag);

        getData();

    }

    @OnClick({R.id.btnBack, R.id.btnSearch})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnSearch:

                startActivity(SearchActivity.class);

                break;
        }
    }

    @Override
    protected void getData() {
        super.getData();

        toastLoading();

        RestClient.get().getWishListRes(Data.getAccountInfoLogin(getBaseContext()).getAccountId()).enqueue(new Callback<Result<List<Restaurant>>>() {
            @Override
            public void onResponse(Call<Result<List<Restaurant>>> call, Response<Result<List<Restaurant>>> response) {

                if (!ErrorUtil.foundAndHandledResponseError(getBaseContext(), response, root)) {

                    toastOk();

                    data = response.body().getData();

                    displayData();

                } else {

                    toastErr();

                }

            }

            @Override
            public void onFailure(Call<Result<List<Restaurant>>> call, Throwable t) {

                toastErr();

                Util.showSnackBarInView(root, getString(R.string.can_not_connect));

            }
        });

    }

    @Override
    protected void displayData() {
        super.displayData();

        frag.setListData(data);

    }

}

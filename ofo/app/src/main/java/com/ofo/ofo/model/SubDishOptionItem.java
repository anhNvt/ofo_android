package com.ofo.ofo.model;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ofo.ofo.R;
import com.ofo.ofo.viewholer.RowSubDishOptionHolder;

import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.IFilterable;
import eu.davidea.flexibleadapter.items.IHeader;
import eu.davidea.flexibleadapter.items.ISectionable;

/**
 * Created by cat on 7/21/16.
 */
public class SubDishOptionItem extends AbstractModelItem<RowSubDishOptionHolder> implements ISectionable<RowSubDishOptionHolder, IHeader>{

    IHeader header;

    SubDishOption data;

    public SubDishOptionItem(String id, SubDishOption data) {
        super(id);

        this.data = data;

        setSelectable(true);

    }

    @Override
    public IHeader getHeader() {
        return header;
    }

    @Override
    public void setHeader(IHeader header) {

        this.header = header;

    }

    @Override
    public int getLayoutRes() {

        return R.layout.row_sub_dish_option;

    }

    @Override
    public RowSubDishOptionHolder createViewHolder(FlexibleAdapter adapter, LayoutInflater inflater, ViewGroup parent) {

        return new RowSubDishOptionHolder(inflater.inflate(getLayoutRes(), parent, false), adapter);

    }

    @Override
    public void bindViewHolder(FlexibleAdapter adapter, RowSubDishOptionHolder holder, int position, List payloads) {

        holder.btnOption.setText(data.getName());

    }
}

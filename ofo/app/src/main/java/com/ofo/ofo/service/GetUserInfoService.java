package com.ofo.ofo.service;

import android.app.IntentService;
import android.content.Intent;

import com.ofo.ofo.database.Data;
import com.ofo.ofo.listener.OnUserInfoDownloadCompleted;
import com.ofo.ofo.model.AccountInfo;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.GlobalData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eo_cuong on 5/19/16.
 */
public class GetUserInfoService extends IntentService {


    boolean isRunning = false;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public GetUserInfoService() {
        super("GetUserInfoService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (!isRunning) {
            Integer id = GlobalData.CURRENT_USER.getAccountId();
            execute(id);
        }


    }

    void execute(Integer id) {
        isRunning = true;

    }
}

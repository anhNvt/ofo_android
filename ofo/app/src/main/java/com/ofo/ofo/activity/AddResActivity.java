package com.ofo.ofo.activity;

import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ofo.ofo.R;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.model.Cuisine;
import com.ofo.ofo.model.Location;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.Util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddResActivity extends BaseActivity {

    List<Location> listLoc = new ArrayList<>();

    List<Cuisine> listCuisine = new ArrayList<>();

    File img;

    @BindView(R.id.edtName)
    EditText edtName;
    @BindView(R.id.spinnerLoc)
    Spinner spinnerLoc;
    @BindView(R.id.spinnerCuisine)
    Spinner spinnerCuisine;
    @BindView(R.id.root)
    LinearLayout root;
    @BindView(R.id.imgViewPhoto)
    ImageView imgViewPhoto;
    @BindView(R.id.edtPhone)
    EditText edtPhone;
    @BindView(R.id.edtEmail)
    EditText edtEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_res);
        ButterKnife.bind(this);

        RestClient.get().getListLocation().enqueue(new Callback<Result<List<Location>>>() {
            @Override
            public void onResponse(Call<Result<List<Location>>> call, Response<Result<List<Location>>> response) {

                if (!ErrorUtil.foundAndHandledResponseError(response, root)) {

                    listLoc = response.body().getData();

                    initLocSpinner();

                }

            }

            @Override
            public void onFailure(Call<Result<List<Location>>> call, Throwable t) {

                ErrorUtil.showCannotConnectError(root);

            }
        });

        RestClient.get().getListCuisine().enqueue(new Callback<Result<List<Cuisine>>>() {
            @Override
            public void onResponse(Call<Result<List<Cuisine>>> call, Response<Result<List<Cuisine>>> response) {

                if (!ErrorUtil.foundAndHandledResponseError(getBaseContext(), response, root)) {

                    listCuisine = response.body().getData();

                    initCuiSpinner();

                }

            }

            @Override
            public void onFailure(Call<Result<List<Cuisine>>> call, Throwable t) {

                Util.showSnackBarInView(root, getString(R.string.can_not_connect));

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ChooseTakePicTypeActivity.START_CODE) {

            if (resultCode == RESULT_OK) {

                img = new File(data.getStringExtra("photo_path"));

                Glide.with(this).load(img).into(imgViewPhoto);

            }

        }

    }

    void initLocSpinner() {

        spinnerLoc.setAdapter(new SpinnerAdapter() {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {

                TextView view;

                if (convertView != null) {

                    view = (TextView) convertView;

                } else {

                    view = (TextView) getLayoutInflater().inflate(R.layout.row_drop_down, parent, false);

                }

                view.setText(listLoc.get(position).getName());

                return view;

            }

            @Override
            public void registerDataSetObserver(DataSetObserver observer) {

            }

            @Override
            public void unregisterDataSetObserver(DataSetObserver observer) {

            }

            @Override
            public int getCount() {
                return listLoc.size();
            }

            @Override
            public Object getItem(int position) {
                return null;
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public boolean hasStableIds() {
                return false;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                TextView view;

                if (convertView != null) {

                    view = (TextView) convertView;

                } else {

                    view = (TextView) getLayoutInflater().inflate(R.layout.row_drop_down, parent, false);

                    view.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.colorPrimary));

                }

                view.setText(listLoc.get(position).getName());

                return view;

            }

            @Override
            public int getItemViewType(int position) {
                return 0;
            }

            @Override
            public int getViewTypeCount() {
                return 1;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }
        });

    }

    void initCuiSpinner() {

        spinnerCuisine.setAdapter(new SpinnerAdapter() {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {

                TextView view;

                if (convertView != null) {

                    view = (TextView) convertView;

                } else {

                    view = (TextView) getLayoutInflater().inflate(R.layout.row_drop_down, parent, false);

                }

                view.setText(listCuisine.get(position).getName());

                return view;

            }

            @Override
            public void registerDataSetObserver(DataSetObserver observer) {

            }

            @Override
            public void unregisterDataSetObserver(DataSetObserver observer) {

            }

            @Override
            public int getCount() {
                return listCuisine.size();
            }

            @Override
            public Object getItem(int position) {
                return null;
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public boolean hasStableIds() {
                return false;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                TextView view;

                if (convertView != null) {

                    view = (TextView) convertView;

                } else {

                    view = (TextView) getLayoutInflater().inflate(R.layout.row_drop_down, parent, false);

                    view.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.colorPrimary));

                }

                view.setText(listCuisine.get(position).getName());

                return view;

            }

            @Override
            public int getItemViewType(int position) {
                return 0;
            }

            @Override
            public int getViewTypeCount() {
                return 1;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }
        });

    }

    @OnClick({R.id.btnBack, R.id.btnSearch, R.id.btnAdd, R.id.btnAddPhoto, R.id.imgViewPhoto})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnSearch:

                startActivity(SearchActivity.class);

                break;
            case R.id.btnAdd:

                toastLoading();

                RequestBody photo = null;

                if (img != null) {

                    photo = RequestBody.create(MediaType.parse("image/*"), img);

                }

                RestClient.get().addRes(listLoc.get(spinnerLoc.getSelectedItemPosition()).getId(), listCuisine.get(spinnerCuisine.getSelectedItemPosition()).getId(), edtName.getText().toString(), edtPhone.getText().toString(), edtEmail.getText().toString(), Data.getAccountInfo(getApplicationContext()).getAccountId(), photo).enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {

                        if (!ErrorUtil.foundAndHandledResponseError(getBaseContext(), response, root)) {

                            toastOk();

                            Util.showSnackBarInView(root, getString(R.string.add_ress_success));

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {


                                    finish();

                                }
                            }, 500);

                        } else {

                            toastErr();

                        }

                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {

                        toastErr();

                        Util.showSnackBarInView(root, getString(R.string.res_ifo_missing));

                    }
                });

                break;

            case R.id.btnAddPhoto :

            case R.id.imgViewPhoto :

                startActivityForResult(ChooseTakePicTypeActivity.class, ChooseTakePicTypeActivity.START_CODE);

                break;

        }
    }

}

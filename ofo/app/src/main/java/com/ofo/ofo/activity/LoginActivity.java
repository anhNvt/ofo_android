package com.ofo.ofo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.ofo.ofo.R;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.model.AccountInfo;
import com.ofo.ofo.model.AccountInfoLogin;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.service.OfoInstantIdService;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.login_form)
    View loginForm;

    @BindView(R.id.edtEmail)
    EditText edtMail;

    @BindView(R.id.edtPass)
    EditText edtPass;

//    @BindView(R.id.btnLoginFacebook)
    LoginButton btnLoginFacebook;

    @OnClick(R.id.btnLogin)
    void validate() {

        String mail = edtMail.getText().toString().trim();

        String pass = edtPass.getText().toString().trim();

        if (mail.length() < 1 || pass.length() < 1) {

            Util.showSnackBarInView(loginForm, getString(R.string.res_ifo_missing));

            return;

        }

        login(mail, pass);

    }

    @OnClick(R.id.btnExit)
    void exit() {

        finish();

    }

    @OnClick(R.id.btnGoSignUp)
    void goSignUp() {

        if (SignUpActivity.getInstance() == null) {
            startActivity(SignUpActivity.class);
            finish();
        } else {
            finish();
        }

    }

    @OnClick(R.id.btnGoForgotPass)
    void resetPass() {

        startActivity(ResetPassActivity.class);

    }
    @OnClick(R.id.btnLoginWithFacebook)
    void loginFB(){

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                loginFacebook(loginResult);
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

    }
    @OnClick(R.id.btnSkipLogin)
    void skipLogin(){

        startActivity(HomeActivity.class);

        finish();
    }


    CallbackManager callbackManager;
    LoginManager loginManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        callbackManager = CallbackManager.Factory.create();
//
        try {

            String mail = getIntent().getStringExtra("mail");

            String pass = getIntent().getStringExtra("pass");

            if (mail.length() > 0 && pass.length() > 0) {

                login(mail, pass);

            }

        } catch (NullPointerException e) {
        }

    }

    void login(String mail, String pass) {

        toastLoading();

        RestClient.get().login(mail, pass).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

                if (response == null || (response.body() == null && response.errorBody() == null)) {

                    toastErr();

                    Util.showSnackBarInView(loginForm, getString(R.string.res_err));

                    return;

                }

                if (response.errorBody() != null) {

                    toastErr();

                    try {
                        try {
                            JSONObject err = new JSONObject(response.errorBody().string());

                            if (err.optJSONArray("error") != null) {

                                Util.showSnackBarInView(loginForm, err.getJSONArray("error").getString(0));

                            } else {
                                Util.showSnackBarInView(loginForm, err.getString("error"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;

                }

                AccountInfoLogin infoLog = response.body().getInfo();

                Data.createOrUpdate(getBaseContext(), infoLog);

                RestClient.get().getAccountInfo(infoLog.getAccountId()).enqueue(new Callback<Result<AccountInfo>>() {
                    @Override
                    public void onResponse(Call<Result<AccountInfo>> call, Response<Result<AccountInfo>> response) {

                        if (!ErrorUtil.foundAndHandledResponseError(response, loginForm)) {

                            toastOk();

                            AccountInfo info = response.body().getData();

                            Data.createOrUpdate(getBaseContext(), info);

                            if (info.getCityId()==null || info.getCityId().length() < 1){

                                changeCity();

                            }else {

                                toastOk();

                                Util.getPrefManager(getBaseContext()).set(Const.KEY_PREF_CITY_ID).value(info.getCityId()).go();

                                Util.getPrefManager(getBaseContext()).set(Const.KEY_PREF_CITY_NAME).value(info.getCityName()).go();

                                Util.getPrefManager(getBaseContext()).set(Const.KEY_PREF_SIGNED_IN).value(true).go();

                                OfoInstantIdService.regToken(getBaseContext());

                                    setResult(RESULT_OK);

                                    finish();

                            }

                        } else {

                            toastErr();

                        }

                    }
                    @Override
                    public void onFailure(Call<Result<AccountInfo>> call, Throwable t) {

                        toastErr();

                        ErrorUtil.showCannotConnectError(loginForm);

                    }
                });


            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

                toastErr();

                Util.showSnackBarInView(loginForm, getString(R.string.can_not_connect));

            }
        });

    }

    void login(String mail, String name, String socialId) {

        toastLoading();

        RestClient.get().loginFacebook(mail, name, socialId).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

                if (response == null || (response.body() == null && response.errorBody() == null)) {

                    toastErr();

                    Util.showSnackBarInView(loginForm, getString(R.string.res_err));

                    return;

                }

                if (response.errorBody() != null) {

                    toastErr();

                    try {
                        try {
                            JSONObject err = new JSONObject(response.errorBody().string());

                            if (err.optJSONArray("error") != null) {

                                Util.showSnackBarInView(loginForm, err.getJSONArray("error").getString(0));

                            } else {
                                Util.showSnackBarInView(loginForm, err.getString("error"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return;

                }

                AccountInfoLogin infoLog = response.body().getInfo();

                Data.createOrUpdate(getBaseContext(), infoLog);

                RestClient.get().getAccountInfo(infoLog.getAccountId()).enqueue(new Callback<Result<AccountInfo>>() {
                    @Override
                    public void onResponse(Call<Result<AccountInfo>> call, Response<Result<AccountInfo>> response) {

                        if (!ErrorUtil.foundAndHandledResponseError(response, loginForm)) {

                            AccountInfo info = response.body().getData();

                            Data.createOrUpdate(getBaseContext(), info);

                            if (info.getCityId()==null || info.getCityId().length() < 1){

                                changeCity();

                            }else {

                                toastOk();

                                Util.getPrefManager(getBaseContext()).set(Const.KEY_PREF_CITY_ID).value(info.getCityId()).go();

                                Util.getPrefManager(getBaseContext()).set(Const.KEY_PREF_CITY_NAME).value(info.getCityName()).go();

                                Util.getPrefManager(getBaseContext()).set(Const.KEY_PREF_SIGNED_IN).value(true).go();

                                OfoInstantIdService.regToken(getBaseContext());

                                setResult(RESULT_OK);

                                finish();

                            }

                        } else {

                            toastErr();

                        }

                    }
                    @Override
                    public void onFailure(Call<Result<AccountInfo>> call, Throwable t) {

                        toastErr();

                        ErrorUtil.showCannotConnectError(loginForm);

                    }
                });


            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

                toastErr();

                Util.showSnackBarInView(loginForm, getString(R.string.can_not_connect));

            }
        });

    }

    void loginFacebook(LoginResult loginResult){

        GraphRequest request = GraphRequest.newMeRequest
                (loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback()
                {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response)
                    {
                        // Application code
                        Log.v("LoginActivity", response.toString());
                        //System.out.println("Check: " + response.toString());
                        try
                        {
                            String id = object.getString("id");
                            String name = object.getString("name");
                            String email = object.getString("email");

                            login(email, name, id);

                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    void changeCity(){
        final int id = Util.getPrefManager(this).get(Const.KEY_PREF_CITY_ID).defaultValue(1).go();

        toastLoading();

        RestClient.get().changeCity(Data.getAccountInfo(getBaseContext()).getAccountId(), id).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

                toastOk();

                AccountInfo info = Data.getAccountInfo(getBaseContext());

                Data.getRealmInstance(getBaseContext()).beginTransaction();

                info.setCityId(id + "");

                info.setCityName(Util.getPrefManager(getBaseContext()).get(Const.KEY_PREF_CITY_NAME).defaultValue("Al-Anbar").go());

                Data.getRealmInstance(getBaseContext()).commitTransaction();

                Util.getPrefManager(getBaseContext()).set(Const.KEY_PREF_SIGNED_IN).value(true).go();

                OfoInstantIdService.regToken(getBaseContext());

                    setResult(RESULT_OK);

                    finish();

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

                toastErr();

//                ErrorUtil.showCannotConnectError(content);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}

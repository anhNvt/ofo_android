package com.ofo.ofo.listener;

import android.view.View;

/**
 * Created by cat on 5/12/16.
 */
public interface OnItemClickListener<T> {

    void onItemClick(View v, int pos, T item);

}

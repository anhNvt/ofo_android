package com.ofo.ofo.activity;

import android.content.Intent;
import android.os.Bundle;

import com.ofo.ofo.R;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.Util;

public class WayPointActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Util.getPrefManager(this).get(Const.KEY_PREF_FIRST_LAUNCH).defaultValue(true).go()) {

            startActivity(new Intent(this, LangCityActivity.class));

            Util.getPrefManager(this).set(Const.KEY_PREF_FIRST_LAUNCH).value(false).go();

        } else {

            startActivity(new Intent(this, HomeActivity.class));

        }

        finish();

    }
}

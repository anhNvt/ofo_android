package com.ofo.ofo.viewholer;

import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.ofo.ofo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import carbon.widget.TextView;

/**
 * Created by cat on 5/14/16.
 */
public class RowResCmtHolder extends UltimateRecyclerviewViewHolder {

    @BindView(R.id.imgViewAvatar)
    public ImageView imgViewAvatar;
    @BindView(R.id.tvName)
    public TextView tvName;
    @BindView(R.id.rate_bar)
    public RatingBar rateBar;
    @BindView(R.id.tvCmt)
    public TextView tvCmt;
    @BindView(R.id.tvDate)
    public TextView tvDate;

    public RowResCmtHolder(View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);

    }
}

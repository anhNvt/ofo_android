package com.ofo.ofo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ofo.ofo.util.LocaleHelper;

/**
 * Created by cat on 5/4/16.
 */
public class City {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("name_ar")
    @Expose
    private String nameAr;
    @SerializedName("order_number")
    @Expose
    private String orderNumber;
    @SerializedName("active")
    @Expose
    private String active;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The countryId
     */
    public String getCountryId() {
        return countryId;
    }

    /**
     *
     * @param countryId
     * The country_id
     */
    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {

        if (LocaleHelper.getLanguage(LocaleHelper.LOCALE_ENG).equals(LocaleHelper.LOCALE_ENG)) {

            return name;

        }

        else return nameAr;

    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The nameAr
     */
    public String getNameAr() {
        return nameAr;
    }

    /**
     *
     * @param nameAr
     * The name_ar
     */
    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    /**
     *
     * @return
     * The orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     *
     * @param orderNumber
     * The order_number
     */
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     *
     * @return
     * The active
     */
    public String getActive() {
        return active;
    }

    /**
     *
     * @param active
     * The active
     */
    public void setActive(String active) {
        this.active = active;
    }

}

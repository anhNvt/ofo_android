package com.ofo.ofo.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.ofo.ofo.R;
import com.ofo.ofo.dialog.BasicDialog;
import com.prashantsolanki.secureprefmanager.SecurePrefManager;
import com.prashantsolanki.secureprefmanager.SecurePrefManagerInit;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by cat on 5/2/16.
 */
public class Util {

    public static void showBasicDialog(FragmentManager fragManager, String msg, String okText, View.OnClickListener okClickListener) {

        BasicDialog dialog = new BasicDialog();

        dialog.setMsg(msg);

        dialog.setOkBtnText(okText);

        dialog.setOkClickListener(okClickListener);

        dialog.setHasCancelBtn(true);

        dialog.show(fragManager, "info_dialog");

    }

    public static void showInfoDialog(FragmentManager fragManager, String msg, View.OnClickListener okClickListener) {

        BasicDialog dialog = new BasicDialog();

        dialog.setMsg(msg);

        dialog.setOkClickListener(okClickListener);

        dialog.show(fragManager, "info_dialog");

    }

    public static void showInfoDialog(FragmentManager fragManager, String msg) {

        BasicDialog dialog = new BasicDialog();

        dialog.setMsg(msg);

        dialog.show(fragManager, "info_dialog");

    }

    public static void showBasicDialog(FragmentManager fragManager, String title, String msg, String okText, View.OnClickListener okClickListener) {

        BasicDialog dialog = new BasicDialog();

        dialog.setTitle(title);

        dialog.setMsg(msg);

        dialog.setOkBtnText(okText);

        dialog.setOkClickListener(okClickListener);

        dialog.setHasCancelBtn(true);

        dialog.show(fragManager, "info_dialog");

    }


    public static boolean isar_Arabic(Context context) {

        String localeCode = LocaleHelper.getLanguage(context, LocaleHelper.LOCALE_ENG);


        return localeCode.equals(LocaleHelper.LOCALE_ARAB);
    }

    public static boolean stringNotNullOrEmpty(String s) {

        return s != null && s.trim().length() > 0;

    }

    public static void showSoftKeyboard(Context ctx, View view) {
        if (view.requestFocus()) {
            InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public static void hideSoftKeyboard(Context ctx, View view) {
        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static SecurePrefManager getPrefManager(Context ctx) {

        if (!SecurePrefManagerInit.isInit()) {

            new SecurePrefManagerInit.Initializer(ctx).useEncryption(true).initialize();

        }

        return SecurePrefManager.with(ctx);

    }

    public static void showSnackBarInView(View v, String msg) {

        if (v == null) {

            return;

        }

        Snackbar.make(v, msg, Snackbar.LENGTH_SHORT).show();

    }

    public static void showSnackBarInView(View v, int msgResId) {

        if (v == null) {

            return;

        }

        Snackbar.make(v, v.getContext().getString(msgResId), Snackbar.LENGTH_SHORT).show();

    }

    public static String roundToString(double value, String format) {

        DecimalFormat df = new DecimalFormat(format);

        df.setRoundingMode(RoundingMode.CEILING);

        return df.format(value);

    }

    public static void dial(String number, View v) {

        try {

            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + number));
            v.getContext().startActivity(callIntent);

        } catch (Exception e) {

            Util.showSnackBarInView(v, v.getContext().getString(R.string.can_not_call));

        }

    }

    public static void shareTextUrl(Context ctx, String title, String link, String shareMenuTitle) {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, title);
        share.putExtra(Intent.EXTRA_TEXT, link);

        ctx.startActivity(Intent.createChooser(share, shareMenuTitle));
    }

    public static void shareTextToWhasapp(String text, View v) {

        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, text);
        try {
            v.getContext().startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {

            showSnackBarInView(v, v.getContext().getString(R.string.whatsapp_not_installed));

        }

    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static void shareTextToMessenger(String text, View v) {

        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.facebook.orca");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, text);
        try {
            v.getContext().startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {

            showSnackBarInView(v, v.getContext().getString(R.string.messenger_not_installed));

        }
    }

    public static void startViewIntent(Context ctx, String url) {

        ctx.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));

    }

    public static String convertToLocalTimeZone(String date) {

        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {

            Date parsed = sourceFormat.parse(date);

            TimeZone tz = Calendar.getInstance().getTimeZone();

            SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

            destFormat.setTimeZone(tz);

            return destFormat.format(parsed);

        } catch (ParseException e) {

            e.printStackTrace();

            return "";

        }

    }

}

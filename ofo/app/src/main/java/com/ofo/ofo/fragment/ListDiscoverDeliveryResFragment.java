package com.ofo.ofo.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ofo.ofo.R;
import com.ofo.ofo.activity.FilterActivity;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.model.FilterField;
import com.ofo.ofo.network.RestClient;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by cat on 5/12/16.
 */
public class ListDiscoverDeliveryResFragment extends RestaurantListFragment {

    int type = DiscoverDeliveryFragment.TYPE_DISCOVER;

    int loc_id = -1;

    int cui_id = -1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_res_discover, container, false);

        ButterKnife.bind(this, root);

        return root;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (listData !=null) {

            setAdapter();

        } else {

            Integer acc_id = Data.getAccountInfo(getContext()) != null ? Data.getAccountInfo(getContext()).getAccountId() : null;

            switch (type) {

                case DiscoverDeliveryFragment.TYPE_DISCOVER:

                    getListData(RestClient.get().getListResDiscover(loc_id == -1 ? null : loc_id, cui_id == -1 ? null : cui_id, acc_id));

                    break;
                case DiscoverDeliveryFragment.TYPE_DELIVERY:

                    getListData(RestClient.get().getListResDelivery(loc_id == -1 ? null : loc_id, acc_id));

                    break;

            }

        }

    }

    @OnClick(R.id.btnFilter)
    public void onClick() {

        startActivityForResult(FilterActivity.class, FilterActivity.START_CODE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FilterActivity.START_CODE) {

            if (resultCode == AppCompatActivity.RESULT_OK) {

                FilterField field = (FilterField) data.getExtras().get("filter_field");

                filter(field);

            }

        }

    }

    void setInitParam(int type, int loc_id, int cui_id) {

        this.type = type;

        this.loc_id = loc_id;

        this.cui_id = cui_id;

    }

    void filter(FilterField field) {

        getListData(RestClient.get().filterRes(null, loc_id == -1 ? null : loc_id, cui_id == -1 ? null : cui_id, field.getOrder_amount(), field.getDelivery_time(), field.getRating(), Data.getAccountInfo(getContext()) != null ? Data.getAccountInfo(getContext()).getAccountId() : null));

    }

}

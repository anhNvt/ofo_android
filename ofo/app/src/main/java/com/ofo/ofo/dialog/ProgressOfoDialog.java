package com.ofo.ofo.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.ofo.ofo.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by cat on 7/5/16.
 */
public class ProgressOfoDialog extends BaseDialog {

    @BindView(R.id.imgViewStroke)
    ImageView imgViewStroke;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_progress, container, false);
        ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.clockwise);

        anim.setRepeatMode(Animation.INFINITE);

        anim.setRepeatCount(Animation.INFINITE);

        imgViewStroke.startAnimation(anim);

    }
}

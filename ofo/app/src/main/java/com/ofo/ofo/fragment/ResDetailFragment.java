package com.ofo.ofo.fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.ofo.ofo.R;
import com.ofo.ofo.activity.LoginSecondActivity;
import com.ofo.ofo.activity.MapActivity;
import com.ofo.ofo.activity.OrderActivity;
import com.ofo.ofo.activity.ResDetailActivity;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.model.Tag;
import com.ofo.ofo.model.Type;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.Util;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.LinearLayout;
import carbon.widget.ProgressBar;
import carbon.widget.TextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by cat on 5/13/16.
 */
public class ResDetailFragment extends BaseFragment {

    public static Restaurant res = new Restaurant();


    @BindView(R.id.btnFavor)
    TextView btnFavor;

    @BindView(R.id.imgViewCover)
    ImageView imgViewCover;
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.btnDeliverMenu)
    RelativeLayout btnDeliverMenu;
    @BindView(R.id.tvDistant)
    TextView tvDistant;
    @BindView(R.id.tvPrice)
    TextView tvPrice;
    @BindView(R.id.tvCuisine)
    TextView tvCuisine;
    @BindView(R.id.tvTimeOpen)
    TextView tvTimeOpen;
    @BindView(R.id.tvDeliverType)
    TextView tvDeliverType;
    @BindView(R.id.progress_bar_rating)
    ProgressBar progressBarRating;
    @BindView(R.id.tvNumReview)
    TextView tvNumReview;
    @BindView(R.id.tvLocation)
    TextView tvLocation;
    @BindView(R.id.tvDistantMap)
    TextView tvDistantMap;
    @BindView(R.id.root)
    LinearLayout root;
    @BindView(R.id.btnMenu)
    RelativeLayout btnMenu;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_res_detail, container, false);
        ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Glide.with(this).load(res.getBannerUrl()).into(imgViewCover);

        tvName.setText(res.getName());

        tvAddress.setText(res.getAddress());

        btnMenu.setVisibility(!res.getOrderOnline().equals("0") ? View.GONE : View.VISIBLE);

        btnDeliverMenu.setVisibility(!res.getOrderOnline().equals("0") ? View.VISIBLE : View.GONE);

        if (res.getDistance() != null) {

            tvDistant.setText(Util.roundToString(Double.parseDouble(res.getDistance()), "#.##") + " km");

            tvDistantMap.setText(Util.roundToString(Double.parseDouble(res.getDistance()), "#.##") + " km");

        } else {

            tvDistant.setVisibility(View.GONE);

            tvDistantMap.setVisibility(View.GONE);

        }

        tvPrice.setText("ADE " + res.getPrice());

        if (res.getTypes().size() > 0) {

            String cuisine = "";

            for (Type t :
                    res.getTypes()) {

                cuisine += t.getName();

                if (res.getTypes().indexOf(t) < res.getTypes().size() - 1) {

                    cuisine += ", ";

                }

            }

            tvCuisine.setText(cuisine);

            tvCuisine.setVisibility(View.VISIBLE);
            
        } else {

            tvCuisine.setVisibility(View.GONE);

        }

        tvTimeOpen.setText(res.getTimeOpen());

        if (res.getTags().size() > 0) {

            String deliverType = "";

            for (Tag t : res.getTags()) {

                deliverType += t.getName();

                if (res.getTags().indexOf(t) < res.getTags().size() - 1) {

                    deliverType += ", ";

                }

            }

            tvDeliverType.setText(deliverType);

        } else {

            tvDeliverType.setVisibility(View.GONE);

        }

        progressBarRating.setProgress((float) (res.getRating() / 5));

        tvNumReview.setText(res.getCountReview() + "");

        tvLocation.setText(res.getAddress());

        SupportMapFragment mapFragment = new SupportMapFragment();

        getChildFragmentManager().beginTransaction().replace(R.id.frag_map, mapFragment).commit();

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                IconGenerator factory = new IconGenerator(getContext());

                factory.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ic_marker));

                Bitmap icon = factory.makeIcon();

                MarkerOptions marker = new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(icon)).position(new LatLng(Double.parseDouble(res.getLat()), Double.parseDouble(res.getLng()))).title(res.getName());

                googleMap.addMarker(marker);

                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(res.getLat()), Double.parseDouble(res.getLng())), 13f));

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        if (Data.getAccountInfo(getContext()) != null) {

            RestClient.get().getWishListRes(Data.getAccountInfo(getContext()).getAccountId()).enqueue(new Callback<Result<List<Restaurant>>>() {
                @Override
                public void onResponse(Call<Result<List<Restaurant>>> call, Response<Result<List<Restaurant>>> response) {

                    if (!ErrorUtil.foundAndHandledResponseError(getContext(), response, getView())) {

                        for (Restaurant r : response.body().getData()) {

                            if (res.getId().intValue() == r.getId().intValue()) {

                                res.setFavor(1);

                                btnFavor.setCompoundDrawablesWithIntrinsicBounds(null, res.isFavorite() ? ContextCompat.getDrawable(getContext(), R.drawable.ic_unfavor_res_detail) : ContextCompat.getDrawable(getContext(), R.drawable.ic_fav_res_detail), null, null);

                                break;

                            }

                        }

                    }

                }

                @Override
                public void onFailure(Call<Result<List<Restaurant>>> call, Throwable t) {

                }
            });

        } else {

            btnFavor.setCompoundDrawablesWithIntrinsicBounds(null, res.isFavorite() ? ContextCompat.getDrawable(getContext(), R.drawable.ic_unfavor_res_detail) : ContextCompat.getDrawable(getContext(), R.drawable.ic_fav_res_detail), null, null);

        }

    }

    @OnClick({R.id.btnDeliverMenu, R.id.btnCall, R.id.btnFavor, R.id.btnReview, R.id.btnPhoto, R.id.btnTriedIt, R.id.btnWhatsApp, R.id.btnMessenger, R.id.btnMenu, R.id.btnMap})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnDeliverMenu:

                OrderActivity.res = res;

                startActivity(OrderActivity.class);

                break;
            case R.id.btnCall:

                Util.dial(res.getPhoneNo(), getView());

                break;
            case R.id.btnFavor:

                if (!Util.getPrefManager(getContext()).get(Const.KEY_PREF_SIGNED_IN).defaultValue(false).go()) {

                    startActivity(LoginSecondActivity.class);

                    return;

                }

                int acc_id = Data.getAccountInfo(getContext()).getAccountId();

                if (res.isFavorite()) {

                    RestClient.get().unfavoriteRes(acc_id, res.getId()).enqueue(new Callback<Result>() {
                        @Override
                        public void onResponse(Call<Result> call, Response<Result> response) {

                            if (!ErrorUtil.foundAndHandledResponseError(response, getView())) {

                                res.setFavor(0);

                                btnFavor.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(getContext(), R.drawable.ic_fav_res_detail), null, null);

                            }

                        }

                        @Override
                        public void onFailure(Call<Result> call, Throwable t) {

                            ErrorUtil.showCannotConnectError(getView());

                        }
                    });

                } else {

                    RestClient.get().favoriteRes(acc_id, res.getId()).enqueue(new Callback<Result>() {
                        @Override
                        public void onResponse(Call<Result> call, Response<Result> response) {

                            if (!ErrorUtil.foundAndHandledResponseError(response, getView())) {

                                res.setFavor(1);

                                btnFavor.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(getContext(), R.drawable.ic_unfavor_res_detail), null, null);

                            }

                        }

                        @Override
                        public void onFailure(Call<Result> call, Throwable t) {

                            ErrorUtil.showCannotConnectError(getView());

                        }
                    });

                }

                break;
            case R.id.btnReview:

                ((ResDetailActivity)getActivity()).goReview();

                break;
            case R.id.btnPhoto:

                ((ResDetailActivity)getActivity()).goTakePhoto();

                break;
            case R.id.btnTriedIt:

                Util.shareTextUrl(getContext(), res.getName(), "", getString(R.string.res_share_menu_title));

                break;
            case R.id.btnWhatsApp:

                Util.shareTextToWhasapp(res.getName(), getView());

                break;
            case R.id.btnMessenger:

                Util.shareTextToMessenger(res.getName(), getView());

                break;
            case R.id.btnMenu:

                OrderActivity.res = res;

                startActivity(OrderActivity.class);

                break;

            case R.id.btnMap:

                MapActivity.res = res;

                startActivity(MapActivity.class);

                break;

        }
    }

}

package com.ofo.ofo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ofo.ofo.R;
import com.ofo.ofo.model.DishMenu;
import com.ofo.ofo.model.OrderItem;
import com.ofo.ofo.util.GlobalData;
import com.ofo.ofo.viewholer.RowOrderedDishHolder;

import java.util.List;

/**
 * Created by eo_cuong on 5/19/16.
 */
public class OrderItemAdapter extends RecyclerView.Adapter<RowOrderedDishHolder> {

    List<OrderItem> orderItems;

    Context context;

    public OrderItemAdapter(List<OrderItem> orderItems, Context context) {
        this.orderItems = orderItems;
        this.context = context;
    }

    @Override
    public RowOrderedDishHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RowOrderedDishHolder(LayoutInflater.from(context).inflate(R.layout.row_dish_ordered, null, false));
    }

    @Override
    public void onBindViewHolder(RowOrderedDishHolder holder, int position) {

        OrderItem orderItem = orderItems.get(position);


        holder.tvNo.setText((position + 1) + "");


        holder.tvName.setText(orderItem.getName());

        holder.tvCost.setText(Double.parseDouble(orderItem.getPrice()) * Integer.parseInt(orderItem.getQuantity()) + "");


    }

    @Override
    public int getItemCount() {
        return orderItems.size();
    }
}

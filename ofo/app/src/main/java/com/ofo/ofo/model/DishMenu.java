package com.ofo.ofo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ofo.ofo.util.LocaleHelper;

import java.util.List;

/**
 * Created by cat on 5/15/16.
 */
public class DishMenu extends Item {

    @SerializedName("menu_id")
    @Expose
    private String menuId;

    @SerializedName("menu_name")
    @Expose
    private String menuName;

    @SerializedName("menu_name_ar")
    @Expose
    private String menuNameAr;

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("dish_side")
    @Expose
    private List<SubDish> subDishes;

    private Integer quantity = 1;

    private String specialRequest = "";

    /**
     *
     * @return
     * The menuId
     */
    public String getMenuId() {
        return menuId;
    }

    /**
     *
     * @param menuId
     * The menu_id
     */
    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    /**
     *
     * @return
     * The menuName
     */
    public String getMenuName() {

        if (LocaleHelper.getLanguage(LocaleHelper.LOCALE_ENG).equals(LocaleHelper.LOCALE_ENG)) {

            return menuName;

        }

        else return menuNameAr;

    }

    /**
     *
     * @param menuName
     * The menu_name
     */
    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    /**
     *
     * @return
     * The menuNameAr
     */
    public String getMenuNameAr() {
        return menuNameAr;
    }

    /**
     *
     * @param menuNameAr
     * The menu_name_ar
     */
    public void setMenuNameAr(String menuNameAr) {
        this.menuNameAr = menuNameAr;
    }

    /**
     *
     * @return
     * The price
     */
    public String getPrice() {
        return price;
    }

    /**
     *
     * @param price
     * The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity > 0 ? quantity : 1;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getSpecialRequest() {
        return specialRequest;
    }

    public void setSpecialRequest(String specialRequest) {
        this.specialRequest = specialRequest;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<SubDish> getSubDishes() {
        return subDishes;
    }

    public void setSubDishes(List<SubDish> subDishes) {
        this.subDishes = subDishes;
    }
}

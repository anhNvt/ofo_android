package com.ofo.ofo.viewholer;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.ofo.ofo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import carbon.widget.ProgressBar;

/**
 * Created by cat on 5/7/16.
 */
public class RowResHolder extends UltimateRecyclerviewViewHolder {

    @BindView(R.id.group_rate)
    public View groupRate;

    @BindView(R.id.imgViewOverlayCover)
    public ImageView imgViewOverlayCover;
    @BindView(R.id.tvOverlayDistant)
    public TextView tvOverlayDistant;
    @BindView(R.id.tvOverlayName)
    public TextView tvOverlayName;
    @BindView(R.id.tvOverlayAddress)
    public TextView tvOverlayAddress;
    @BindView(R.id.spanOverlayDecs)
    public LinearLayout spanOverlayDecs;
    @BindView(R.id.overlayRow)
    public RelativeLayout overlayRow;
    @BindView(R.id.imgViewCover)
    public ImageView imgViewCover;
    @BindView(R.id.tvDistant)
    public TextView tvDistant;
    @BindView(R.id.tvName)
    public TextView tvName;
    @BindView(R.id.tvAddress)
    public TextView tvAddress;
    @BindView(R.id.tvRating)
    public TextView tvRating;
    @BindView(R.id.spanDecs)
    public LinearLayout spanDecs;
    @BindView(R.id.header)
    public RelativeLayout header;
    @BindView(R.id.btnCall)
    public RelativeLayout btnCall;
    @BindView(R.id.btnMenu)
    public View btnMenu;
    @BindView(R.id.btnOrder)
    public View btnOrder;
    @BindView(R.id.btnDetail)
    public ImageView btnDetail;
    @BindView(R.id.row)
    public ExpandableRelativeLayout row;

    @BindView(R.id.tvOverlayPromo)
    public TextView tvOverlayPromo;

    @BindView(R.id.group_overlay_promo)
    public View groupOverlayPromo;

    @BindView(R.id.tvPromo)
    public TextView tvPromo;

    @BindView(R.id.group_promo)
    public View groupPromo;

    @BindView(R.id.btnUnfavorite)
    public ImageView btnUfavorite;

    @BindView(R.id.progress_bar_rating)
    public ProgressBar progressBarRating;

    public RowResHolder(View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);

    }
}

package com.ofo.ofo.viewholer;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.ofo.ofo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import carbon.widget.ImageView;
import carbon.widget.TextView;

/**
 * Created by cat on 5/18/16.
 */
public class RowOrderedDishHolder extends RecyclerView.ViewHolder {


    @BindView(R.id.tvNo)
    public TextView tvNo;
    @BindView(R.id.tvName)
    public TextView tvName;
    @BindView(R.id.tvCost)
    public TextView tvCost;

    @BindView(R.id.btnEdit)
    public ImageView btnEdit;

    public RowOrderedDishHolder(View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);

    }
}

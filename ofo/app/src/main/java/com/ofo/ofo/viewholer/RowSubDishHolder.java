package com.ofo.ofo.viewholer;

import android.view.View;

import com.ofo.ofo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import carbon.widget.ImageView;
import carbon.widget.TextView;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.viewholders.ExpandableViewHolder;

/**
 * Created by cat on 7/21/16.
 */
public class RowSubDishHolder extends ExpandableViewHolder {

    @BindView(R.id.tvName)
    public TextView tvName;
    @BindView(R.id.tvSelectLimit)
    public TextView tvSelectLimit;
    @BindView(R.id.btnArr)
    public ImageView btnArr;

    public RowSubDishHolder(View view, FlexibleAdapter adapter) {
        super(view, adapter);

        ButterKnife.bind(this, view);

    }

    public RowSubDishHolder(View view, FlexibleAdapter adapter, boolean stickyHeader) {
        super(view, adapter, stickyHeader);

        ButterKnife.bind(this, view);

    }

    @Override
    protected void expandView(int position) {
        super.expandView(position);

        if (mAdapter.isExpanded(position)) {

            mAdapter.notifyItemChanged(position, false);

        }

    }

    @Override
    protected void collapseView(int position) {
        super.collapseView(position);

        if (!mAdapter.isExpanded(position)) {

            mAdapter.notifyItemChanged(position, false);

        }

    }

}

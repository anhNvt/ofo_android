package com.ofo.ofo.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.bumptech.glide.Glide;
import com.ofo.ofo.R;
import com.ofo.ofo.activity.LoginSecondActivity;
import com.ofo.ofo.activity.PostReviewResActivity;
import com.ofo.ofo.model.ResReview;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.Util;
import com.ofo.ofo.viewholer.RowResCmtHolder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.FloatingActionButton;
import carbon.widget.TextView;

/**
 * Created by cat on 5/14/16.
 */
public class ResReviewFragment extends ListFragment<ResReview, RowResCmtHolder> {

    public static Restaurant res;

    @BindView(R.id.imgViewCover)
    ImageView imgViewCover;
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.rate_bar)
    RatingBar rateBar;
    @BindView(R.id.tvCmtCount)
    TextView tvCmtCount;

    @BindView(R.id.btnReview)
    FloatingActionButton btnReview;

    @OnClick(R.id.btnReview)public void goRev() {

        goReview();

    }

    @Override
    protected int getRowLayoutResId() {

        return R.layout.row_review_res;

    }

    @Override
    protected RowResCmtHolder inflateViewHolder(View view) {

        return new RowResCmtHolder(view);

    }

    @Override
    protected void bindDataToHolder(RowResCmtHolder holder, ResReview data, int position) {
        super.bindDataToHolder(holder, data, position);

        Glide.with(this).load(data.getAvatar()).into(holder.imgViewAvatar);

        holder.tvName.setText(data.getAccountName());

        holder.tvCmt.setText(data.getComment());

        holder.tvDate.setText(Util.convertToLocalTimeZone(data.getCreatedAt()));

        holder.rateBar.setRating(Float.parseFloat(data.getRating()));

    }

    @Override
    public void onResume() {
        super.onResume();

        btnReview.setVisibility(View.VISIBLE);

    }

    @Override
    public void onPause() {
        super.onPause();

        btnReview.setVisibility(View.GONE);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_res_review, container, false);
        ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Glide.with(this).load(res.getBannerUrl()).into(imgViewCover);

        tvName.setText(res.getName());

        tvAddress.setText(res.getAddress());

        tvCmtCount.setText(res.getCountReview() + "");

        rateBar.setRating(res.getRating().floatValue());

        if (listData == null || willReloadData()) {

            getListData(RestClient.get().getListResReview(res.getId()));

            setReloadData(false);

        } else {

            setAdapter();

        }

    }

    public void goReview() {

        PostReviewResActivity.res = res;

        startActivityForResultWithCheckLogin(PostReviewResActivity.class, PostReviewResActivity.START_CODE, LoginSecondActivity.REVIEW);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PostReviewResActivity.START_CODE) {

            if (resultCode == AppCompatActivity.RESULT_OK) {

                getListData(RestClient.get().getListResReview(res.getId()));

            }

        }


    }
}

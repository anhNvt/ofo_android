package com.ofo.ofo.listener;

import com.ofo.ofo.model.AccountInfo;

/**
 * Created by eo_cuong on 5/19/16.
 */
public class OnUserInfoDownloadCompleted {

    public AccountInfo accountInfo;

    public OnUserInfoDownloadCompleted(AccountInfo accountInfo) {
        this.accountInfo = accountInfo;
    }
}

package com.ofo.ofo.model;

import android.support.v7.widget.RecyclerView;

import java.io.Serializable;

import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;

/**
 * This class will benefit of the already implemented methods (getter and setters) in
 * {@link eu.davidea.flexibleadapter.items.AbstractFlexibleItem}.
 *
 * It is used as Base item for all example models.
 */
public abstract class AbstractModelItem<VH extends RecyclerView.ViewHolder>
		extends AbstractFlexibleItem<VH> implements Serializable {

	private String id;

	public AbstractModelItem(String id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object inObject) {
		if (inObject instanceof AbstractModelItem) {
			AbstractModelItem inItem = (AbstractModelItem) inObject;
			return this.id.equals(inItem.id);
		}
		return false;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "id=" + id;
	}

}
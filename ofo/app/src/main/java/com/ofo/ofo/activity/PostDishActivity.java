package com.ofo.ofo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ofo.ofo.R;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.Util;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.LinearLayout;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostDishActivity extends BaseActivity {

    public static final int START_CODE = 5;

    public static String photoPath;

    File imgFile;

    Restaurant res;

    @BindView(R.id.btnSelectRes)
    TextView btnSelectRes;

    @BindView(R.id.btnExit)
    ImageView btnExit;
    @BindView(R.id.imgViewPic)
    ImageView imgViewPic;
    @BindView(R.id.edtCmt)
    EditText edtCmt;
    @BindView(R.id.root)
    LinearLayout root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_dish);
        ButterKnife.bind(this);

        btnExit.setVisibility(View.GONE);

        imgFile = new File(photoPath);

        Glide.with(this).load(imgFile).into(imgViewPic);

    }

    @OnClick({R.id.btnBack, R.id.btnEditPic, R.id.btnSelectRes, R.id.btnPost})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnEditPic:

                startActivityForResult(ChooseTakePicTypeActivity.class, ChooseTakePicTypeActivity.START_CODE);

                break;
            case R.id.btnSelectRes:

                startActivityForResult(SearchResNameOnlyActivity.class, SearchResNameOnlyActivity.START_CODE);

                break;
            case R.id.btnPost:

                RequestBody img = RequestBody.create(MediaType.parse("image/*"), imgFile);

                toastLoading();

                RestClient.get().postDish(res != null ? res.getId() : null, Data.getAccountInfo(getBaseContext()).getAccountId(), edtCmt.getText().toString().trim(), img).enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {

                        if (!ErrorUtil.foundAndHandledResponseError(response, root)) {

                            toastOk();

                            Util.showSnackBarInView(root, getString(R.string.post_dish_success));

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    finish();

                                }
                            }, 500);

                        } else {

                            toastErr();

                        }

                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {

                        toastErr();

                        Util.showSnackBarInView(root, getString(R.string.can_not_connect));

                    }
                });

                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ChooseTakePicTypeActivity.START_CODE) {

            if (resultCode == RESULT_OK) {

                photoPath = data.getStringExtra("photo_path");

                imgFile = new File(photoPath);

                Glide.with(this).load(imgFile).into(imgViewPic);

            }

        }

        if (requestCode == SearchResNameOnlyActivity.START_CODE) {

            if (resultCode == RESULT_OK) {

                this.res = (Restaurant) data.getExtras().get("res");

                btnSelectRes.setText(res.getName());

            }

        }

    }
}
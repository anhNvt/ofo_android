package com.ofo.ofo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Pair;
import android.view.View;

import com.ofo.ofo.R;
import com.ofo.ofo.customview.SizeAdjustingTextView;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.model.Address;
import com.ofo.ofo.model.DishMenu;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.GlobalData;
import com.ofo.ofo.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.TextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeliverAddressDetailActivity extends BaseActivity {

    public static Address address;

    @BindView(R.id.root)
    View root;

    @BindView(R.id.tvTitle)
    SizeAdjustingTextView tvTitle;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvCity)
    android.widget.TextView tvCity;
    @BindView(R.id.tvArea)
    android.widget.TextView tvArea;
    @BindView(R.id.tvType)
    android.widget.TextView tvType;
    @BindView(R.id.tvPhone)
    android.widget.TextView tvPhone;
    @BindView(R.id.tvStreetName)
    android.widget.TextView tvStreetName;
    @BindView(R.id.tvFloor)
    android.widget.TextView tvFloor;
    @BindView(R.id.tvBuilding)
    android.widget.TextView tvBuilding;
    @BindView(R.id.tvDirection)
    android.widget.TextView tvDirection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deliver_address_detail);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.deliver_add));

        tvCity.setText(address.getCityName());

        tvArea.setText(address.getAddressName());

        tvType.setText(address.getAddressTypeName());

        tvPhone.setText(address.getPhoneNo());

        tvStreetName.setText(address.getStreetName());

        tvFloor.setText(address.getFloor());

        tvBuilding.setText(address.getBuildingName());

        tvDirection.setText(address.getDirection());

    }

    @OnClick({R.id.btnBack, R.id.btnExit, R.id.btnEdit, R.id.btnOrderNow})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnExit:

                finish();

                break;
            case R.id.btnEdit:

                CreateEditAddressActivity.address = address;

                Intent i = new Intent(getBaseContext(), CreateEditAddressActivity.class);

                i.putExtra(Const.KEY_SEND_ADDRESS_MODE, CreateEditAddressActivity.MODE_EDIT);

                startActivity(i);

                break;
            case R.id.btnOrderNow:

                double subTotal = 0;

                JSONArray items = new JSONArray();

                for (DishMenu m : GlobalData.listDishOrder) {

                    subTotal += Double.parseDouble(m.getPrice()) * m.getQuantity();

                    JSONObject item = new JSONObject();

                    try {

                        item.put("dish_id", m.getId());

                        item.put("quantity", m.getQuantity());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    items.put(item);

                }

                double discount = 0;

                discount = subTotal * Double.parseDouble(GlobalData.res.getDiscount()) / 100.0;

                double total = subTotal - discount;

                toastLoading();

                RestClient.get().order(Data.getAccountInfo(getBaseContext()).getAccountId(), address.getId(), total, GlobalData.res.getId(), items.toString()).enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {

                        if (!ErrorUtil.foundAndHandledResponseError(response, root)) {

                            toastOk();

                            Util.showInfoDialog(getSupportFragmentManager(), getString(R.string.order_success), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    finish();

                                }
                            });

                        } else {

                            toastErr();

                        }

                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {

                        toastErr();

                        ErrorUtil.showCannotConnectError(root);

                    }
                });

                break;
        }
    }
}

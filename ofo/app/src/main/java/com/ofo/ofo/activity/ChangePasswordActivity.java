package com.ofo.ofo.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.ofo.ofo.R;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.LinearLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends BaseActivity {

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.edtOldPass)
    EditText edtOldPass;
    @BindView(R.id.edtNewPass)
    EditText edtNewPass;
    @BindView(R.id.edtReNewPass)
    EditText edtReNewPass;
    @BindView(R.id.root)
    LinearLayout root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);

        tvTitle.setText(R.string.my_profile);

    }

    @OnClick({R.id.btnBack, R.id.btnExit, R.id.btnOk})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnExit:

                finish();

                break;
            case R.id.btnOk:

                Util.hideSoftKeyboard(getBaseContext(), root);

                if (!(edtNewPass.getText().toString().length() > 0)) {

                    Util.showSnackBarInView(root, R.string.pass_missing);

                    return;

                }

                if (!edtNewPass.getText().toString().equals(edtReNewPass.getText().toString())) {

                Util.showSnackBarInView(root, R.string.pass_not_match);

                return;

            }

                toastLoading();

            RestClient.get().changePass(Data.getAccountInfo(getBaseContext()).getAccountId(), edtOldPass.getText().toString(), edtNewPass.getText().toString()).enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {

                    if (!ErrorUtil.foundAndHandledResponseError(response, root)) {

                        toastOk();

                        Util.showSnackBarInView(root, R.string.change_pass_ok);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                finish();

                            }
                        }, 700);

                    } else {

                        toastErr();

                    }

                }

                @Override
                public void onFailure(Call<Result> call, Throwable t) {

                    toastErr();

                    ErrorUtil.showCannotConnectError(root);

                }
            });

                break;
        }
    }
}

package com.ofo.ofo.activity;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;

import com.ofo.ofo.R;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.Util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (Util.getPrefManager(this).get(Const.KEY_PREF_FIRST_LAUNCH).defaultValue(true).go()) {

            Util.getPrefManager(this).set(Const.KEY_PREF_CITY_ID).value(1).go();

            Util.getPrefManager(this).set(Const.KEY_PREF_CITY_NAME).value("Al-Anbar").go();

        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(WayPointActivity.class);

                finish();

            }
        }, 500);

    }
}

package com.ofo.ofo.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.ofo.ofo.R;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.fragment.NearMeFragment;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.FloatingActionButton;
import carbon.widget.ImageView;
import carbon.widget.RelativeLayout;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesWithFallbackProvider;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NearMeActivity extends BaseActivity {

    private static int FINE_LOCATION_PER_ID = 0;

    @BindView(R.id.btnBack)
    ImageView btnBack;
    @BindView(R.id.btnSearch)
    ImageView btnSearch;
    @BindView(R.id.btnLocate)
    FloatingActionButton btnLocate;
    @BindView(R.id.list)
    RelativeLayout list;
    @BindView(R.id.btnViewAsList)
    FloatingActionButton btnViewAsList;
    @BindView(R.id.map)
    RelativeLayout map;

    @BindView(R.id.root)
    View root;

    NearMeFragment frag;

    boolean located = false;

    private GoogleMap googleMap;

    ArrayList<Marker> markers = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_near_me);
        ButterKnife.bind(this);

        frag = (NearMeFragment) getSupportFragmentManager().findFragmentById(R.id.frag_list);

        toastLoading();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, FINE_LOCATION_PER_ID);

        } else {

            getLocation();

        }

    }

    @Override
    protected void onPause() {

        SmartLocation.with(getBaseContext()).location().stop();

        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();

        SmartLocation.with(getBaseContext()).location().stop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        SmartLocation.with(getBaseContext()).location().stop();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == FINE_LOCATION_PER_ID && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            getLocation();

        } else {

            finish();

        }
    }

    void getLocation() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (!located) {

                    toastErr();

                }

            }

        }, 15000);

        SmartLocation.with(this).location(new LocationGooglePlayServicesWithFallbackProvider(this)).oneFix().start(new OnLocationUpdatedListener() {
            @Override
            public void onLocationUpdated(Location location) {

                located = true;

                getListResNearby(new Float(location.getLatitude()), new Float(location.getLongitude()));

//                getListResNearby(new Float(25.239126), new Float(55.312965));

            }

        });

    }

    void getListResNearby(Float lat, Float lon) {
        super.getData();

        RestClient.get().getListResNearMe(lat, lon, Data.getAccountInfo(getBaseContext()) != null ? Data.getAccountInfo(getBaseContext()).getAccountId() : null).enqueue(new Callback<Result<List<Restaurant>>>() {
            @Override
            public void onResponse(Call<Result<List<Restaurant>>> call, Response<Result<List<Restaurant>>> response) {

                if (!ErrorUtil.foundAndHandledResponseError(getBaseContext(), response, root)) {

                    toastOk();

                    frag.setListData(response.body().getData());

                    if (response.body().getData().size() > 0) {

                        initMap(response.body().getData());

                    }

                } else {

                    toastErr();

                }

            }

            @Override
            public void onFailure(Call<Result<List<Restaurant>>> call, Throwable t) {

                toastErr();

                Util.showSnackBarInView(root, getString(R.string.can_not_connect));

            }
        });

    }

    void initMap(final List data) {

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.frag_map);

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap googleMap) {

                IconGenerator factory = new IconGenerator(getBaseContext());

                factory.setBackground(ContextCompat.getDrawable(getBaseContext(), R.drawable.ic_marker));

                final LatLngBounds.Builder builder = new LatLngBounds.Builder();

                for (int i = 0; i < data.size(); i++) {

                    Restaurant res = (Restaurant) data.get(i);

                    Bitmap icon = factory.makeIcon(String.valueOf(i + 1));

                    MarkerOptions marker = new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(icon)).position(new LatLng(Double.parseDouble(res.getLat()), Double.parseDouble(res.getLng()))).title(res.getName());

                    markers.add(googleMap.addMarker(marker));

                    builder.include(marker.getPosition());

                }

                googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {

                        CameraUpdate update = CameraUpdateFactory.newLatLngBounds(builder.build(), 25);

                        googleMap.animateCamera(update);

                    }
                });

                googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {

                        ResDetailActivity.res = (Restaurant) data.get(markers.indexOf(marker));

                        startActivity(ResDetailActivity.class);

                    }
                });

            }
        });

    }

    @OnClick({R.id.btnBack, R.id.btnSearch, R.id.btnLocate, R.id.btnViewAsList})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnSearch:

                startActivity(SearchActivity.class);

                break;
            case R.id.btnLocate:

                list.setVisibility(View.GONE);

                map.setVisibility(View.VISIBLE);

                break;
            case R.id.btnViewAsList:

                map.setVisibility(View.GONE);

                list.setVisibility(View.VISIBLE);

                break;
        }
    }
}

package com.ofo.ofo.fragment;

import android.view.View;

import com.ofo.ofo.R;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.viewholer.RowLocCuiHolder;

/**
 * Created by cat on 5/13/16.
 */
public class ListResNameOnlyFragment extends ListFragment<Restaurant, RowLocCuiHolder> {

    @Override
    protected int getRowLayoutResId() {

        return R.layout.row_res_name_only;

    }

    @Override
    protected RowLocCuiHolder inflateViewHolder(View view) {

        return new RowLocCuiHolder(view);

    }

    @Override
    protected void bindDataToHolder(RowLocCuiHolder holder, Restaurant data, int position) {
        super.bindDataToHolder(holder, data, position);

        holder.tvName.setText(data.getName());

    }
}

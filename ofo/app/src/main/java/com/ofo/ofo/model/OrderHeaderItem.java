package com.ofo.ofo.model;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.ofo.ofo.R;
import com.ofo.ofo.viewholer.OrderHeaderHolder;

import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;

/**
 * Created by cat on 5/17/16.
 */
public class OrderHeaderItem extends AbstractModelItem<OrderHeaderHolder> {

    Restaurant res;

    public OrderHeaderItem(String id, Restaurant res) {
        super(id);

        this.res = res;

    }

    @Override
    public boolean isSelectable() {

        return false;

    }

    @Override
    public int getLayoutRes() {

        return R.layout.order_header;

    }

    @Override
    public OrderHeaderHolder createViewHolder(FlexibleAdapter adapter, LayoutInflater inflater, ViewGroup parent) {

        return new OrderHeaderHolder(inflater.inflate(getLayoutRes(), parent, false), adapter);

    }

    @Override
    public void bindViewHolder(FlexibleAdapter adapter, OrderHeaderHolder holder, int position, List payloads) {

        Glide.with(holder.itemView.getContext()).load(res.getBannerUrl()).into(holder.imgViewCover);

        holder.tvName.setText(res.getName());

        holder.tvAddress.setText(res.getAddress());

        holder.tvAvrTimeDeliver.setText("30 ");

        holder.tvPrice.setText(res.getPrice());

    }
}

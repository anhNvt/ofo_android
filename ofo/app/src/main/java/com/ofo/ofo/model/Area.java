package com.ofo.ofo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ofo.ofo.util.LocaleHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cat on 5/4/16.
 */
public class Area {

    @SerializedName("country_id")
    @Expose
    private Integer countryId;

    @SerializedName("country_name")
    @Expose
    private String countryName;
    @SerializedName("country_name_ar")
    @Expose
    private String countryNameAr;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("cities")
    @Expose
    private List<City> cities = new ArrayList<City>();

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {

        if (LocaleHelper.getLanguage(LocaleHelper.LOCALE_ENG).equals(LocaleHelper.LOCALE_ENG)) {

            return countryName;

        }

        else return countryNameAr;

    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryNameAr() {
        return countryNameAr;
    }

    public void setCountryNameAr(String countryNameAr) {
        this.countryNameAr = countryNameAr;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

}

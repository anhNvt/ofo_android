package com.ofo.ofo.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;

import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ChosenImages;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.kbeanie.imagechooser.api.utils.ImageChooserBuilder;
import com.kbeanie.imagechooser.exceptions.ChooserException;
import com.ofo.ofo.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChooseTakePicTypeActivity extends BaseActivity {

    public static final int START_CODE = 4;

    private static final int STORAGE_PER_ID = 0;

    ImageChooserManager imageChooserManager;

    int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_take_pic_type);
        ButterKnife.bind(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ((requestCode == ChooserType.REQUEST_CAPTURE_PICTURE || requestCode == ChooserType.REQUEST_PICK_PICTURE) && resultCode == RESULT_OK) {

            imageChooserManager.submit(requestCode, data);

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == STORAGE_PER_ID && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            getImage();

        }

    }

    @OnClick({R.id.btnBack, R.id.btnSearch, R.id.btnTakePhoto, R.id.btnSelectFromGallery})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnSearch:

                startActivity(SearchActivity.class);

                break;
            case R.id.btnTakePhoto:

                type = ChooserType.REQUEST_CAPTURE_PICTURE;

                getImage();

                break;

            case R.id.btnSelectFromGallery:

                type = ChooserType.REQUEST_PICK_PICTURE;

                getImage();

                break;

        }
    }

    void getImage() {

        if (ActivityCompat.checkSelfPermission(ChooseTakePicTypeActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(ChooseTakePicTypeActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PER_ID);

            return;

        }

        imageChooserManager = new ImageChooserManager(this, type, false);
        imageChooserManager.setImageChooserListener(new ImageChooserListener() {
            @Override
            public void onImageChosen(final ChosenImage chosenImage) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Intent i = new Intent();

                        i.putExtra("photo_path", chosenImage.getFilePathOriginal());

                        setResult(RESULT_OK, i);

                        finish();

                    }
                });

            }

            @Override
            public void onError(String s) {}

            @Override
            public void onImagesChosen(ChosenImages chosenImages) {}
        });

        try {
            imageChooserManager.choose();
        } catch (ChooserException e) {
            e.printStackTrace();
        }

    }

}

package com.ofo.ofo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.ofo.ofo.dialog.ProgressOfoDialog;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.Util;


/**
 * Created by cat on 4/11/16.
 */
public class BaseActivity extends AppCompatActivity {

    ProgressDialog progressDialog;

    ProgressOfoDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        progressDialog = new ProgressDialog(BaseActivity.this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);

    }

    void showProgressDialog(CharSequence st) {
        progressDialog.setMessage(st);
        progressDialog.show();
    }

    void closeProgressDialog() {
        progressDialog.dismiss();
    }

    protected void getData() {
    }

    protected void displayData() {
    }


    protected void startActivity(Class c) {

        startActivity(new Intent(getBaseContext(), c));

    }

    protected void startActivityForResult(Class c, int code) {

        startActivityForResult(new Intent(getBaseContext(), c), code);
    }

    protected void toastLoading() {

        dialog = new ProgressOfoDialog();

        dialog.show(getSupportFragmentManager(), "progress_dialog");

    }

    protected void toastOk() {

        dialog.dismiss();

    }

    protected void toastErr() {

        dialog.dismiss();

    }

    protected void startActivityWithCheck(Class c, int code){

        if (Util.getPrefManager(this).get(Const.KEY_PREF_SIGNED_IN).defaultValue(false).go()){

            startActivity(c);

        }else {

            startActivityForResult(LoginSecondActivity.class, code);
        }

    }

    protected void startActivityWithCheck(Class c, int request_code, int caller_code){

        if (Util.getPrefManager(this).get(Const.KEY_PREF_SIGNED_IN).defaultValue(false).go()){

            startActivity(c);

        } else {

            Intent i = new Intent(getBaseContext(), LoginSecondActivity.class);

            i.putExtra("caller", caller_code);

            startActivityForResult(i, request_code);

        }

    }

    protected void startActivityForResultWithCheckLogin(Class c, int code) {

        if (Util.getPrefManager(this).get(Const.KEY_PREF_SIGNED_IN).defaultValue(false).go()) {

            startActivityForResult(new Intent(getBaseContext(), c), code);

        } else {

            startActivity(LoginSecondActivity.class);

        }

    }

    protected void startActivityForResultWithCheckLogin(Class c, int request_code, int caller_code) {

        if (Util.getPrefManager(this).get(Const.KEY_PREF_SIGNED_IN).defaultValue(false).go()) {

            startActivityForResult(new Intent(getBaseContext(), c), request_code);

        } else {

            Intent i = new Intent(getBaseContext(), LoginSecondActivity.class);

            i.putExtra("caller", caller_code);

            startActivity(i);

        }

    }

}

package com.ofo.ofo.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.ofo.ofo.R;
import com.ofo.ofo.model.Location;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.viewholer.RowLocCuiHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by cat on 5/12/16.
 */
public class LocFragment extends ListFragment<Location, RowLocCuiHolder> {

    ArrayList dataRoot;

    @BindView(R.id.edtSearch)
    EditText edtSearch;

    @Override
    protected int getRowLayoutResId() {

        return R.layout.row_loc_cuisine;

    }

    @Override
    protected RowLocCuiHolder inflateViewHolder(View view) {

        return new RowLocCuiHolder(view);

    }

    @Override
    protected void bindDataToHolder(RowLocCuiHolder holder, final Location data, final int position) {

        super.bindDataToHolder(holder, data, position);

        holder.tvName.setText(data.getName());

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_list_loc_cui, container, false);

        ButterKnife.bind(this, root);

        return root;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                search(edtSearch.getText().toString());

            }
        });

        if (listData == null) {

            getListData(RestClient.get().getListLocation());

        } else {

            setAdapter();

        }

    }

    @OnClick(R.id.btnClear) void clearText() {

        edtSearch.setText("");

    }

    @Override
    public void setListData(List data) {

        Location l = new Location();

        l.setId(-1);

        l.setName(getString(R.string.all_location));

        l.setNameAr(getString(R.string.all_location));

        data.add(0, l);

        this.listData = data;

        setAdapter();

    }

    void search(String searchText) {

        if (listData == null) {

            return;

        }

        if (dataRoot == null) {

            dataRoot = new ArrayList();

            dataRoot.addAll(listData);

            dataRoot.remove(0);

        }

            listData.clear();

            listData.addAll(dataRoot);

        for (Object loc : dataRoot) {

            if (!((Location)loc).getName().toLowerCase().contains(searchText.toLowerCase())) {

                listData.remove(loc);

            }

        }

        setListData(listData);

    }

}

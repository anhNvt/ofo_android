package com.ofo.ofo.activity;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.ofo.ofo.R;
import com.ofo.ofo.customview.FlowRadioGroup;
import com.ofo.ofo.model.DishMenu;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.util.GlobalData;
import com.ofo.ofo.util.Util;
import com.ofo.ofo.viewholer.RowOrderedDishHolder;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.LinearLayout;

public class ConfirmOrderActivity extends BaseActivity {

    public static Restaurant res;

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.listItem)
    RecyclerView listItem;
    @BindView(R.id.tvTotalCost)
    carbon.widget.TextView tvTotalCost;
    @BindView(R.id.tvDiscountPercent)
    carbon.widget.TextView tvDiscountPercent;
    @BindView(R.id.tvDiscount)
    carbon.widget.TextView tvDiscount;
    @BindView(R.id.tvDeliverCharge)
    carbon.widget.TextView tvDeliverCharge;
    @BindView(R.id.tvGrandTotalCost)
    carbon.widget.TextView tvGrandTotalCost;
    @BindView(R.id.tvEstDeliverTime)
    carbon.widget.TextView tvEstDeliverTime;
    @BindView(R.id.btnOrderNow)
    carbon.widget.TextView btnOrderNow;
    @BindView(R.id.radioGroupDeliverOption)
    FlowRadioGroup radioGroupDeliverOption;
    @BindView(R.id.spanInputTableNo)
    LinearLayout spanInputTableNo;
    @BindView(R.id.edtTableNo)
    EditText edtTableNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_order);
        ButterKnife.bind(this);

        tvEstDeliverTime.setText(30 + "");

        tvTitle.setText(res.getName());

        tvDiscountPercent.setText(getString(R.string.discount) + " (" + res.getDiscount() + getString(R.string.percent) + ")");

        radioGroupDeliverOption.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (radioGroupDeliverOption.getCheckedRadioButtonIndex() == 2) {

                    spanInputTableNo.setVisibility(View.VISIBLE);

                } else {

                    spanInputTableNo.setVisibility(View.GONE);

                }

            }
        });

        radioGroupDeliverOption.check(radioGroupDeliverOption.getChildAt(0).getId());

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (GlobalData.listDishOrder.size() < 1) {

            btnOrderNow.setEnabled(false);

            btnOrderNow.setBackgroundColor(ContextCompat.getColor(this, R.color.com_facebook_button_background_color_disabled));

        } else {

            btnOrderNow.setEnabled(true);

            btnOrderNow.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));

        }

        double subTotal = 0;

        for (DishMenu m : GlobalData.listDishOrder) {

            subTotal += Double.parseDouble(m.getPrice()) * m.getQuantity();

        }

        double discount = 0;

        discount = subTotal * Double.parseDouble(res.getDiscount()) / 100.0;

        double total = subTotal - discount;

        tvTotalCost.setText(getString(R.string.aed) + " " + subTotal + "");

        tvDiscount.setText("-" + discount + "");

        tvGrandTotalCost.setText(getString(R.string.aed) + " " + total + "");

        listItem.setHasFixedSize(true);

        listItem.setLayoutManager(new LinearLayoutManager(this));

        listItem.setAdapter(new RecyclerView.Adapter<RowOrderedDishHolder>() {
            @Override
            public RowOrderedDishHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                return new RowOrderedDishHolder(getLayoutInflater().inflate(R.layout.row_dish_ordered, parent, false));
            }

            @Override
            public void onBindViewHolder(RowOrderedDishHolder holder, final int position) {

                holder.tvNo.setText((position + 1) + "");

                DishMenu d = GlobalData.listDishOrder.get(position);

                holder.tvName.setText(d.getName());

                holder.tvCost.setText(Double.parseDouble(d.getPrice()) * d.getQuantity() + "");

                holder.btnEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        DishOrderDetailActivity.res = res;

                        DishOrderDetailActivity.dish = GlobalData.listDishOrder.get(position);

                        startActivity(DishOrderDetailActivity.class);

                    }
                });

            }

            @Override
            public int getItemCount() {
                return GlobalData.listDishOrder.size();
            }
        });

    }

    @OnClick({R.id.btnBack, R.id.btnExit, R.id.btnAdd, R.id.btnOrderNow})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnExit:

                Util.showBasicDialog(getSupportFragmentManager(), getString(R.string.cancel_order), getString(R.string.cancel_order_msg), getString(R.string.yes), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        GlobalData.listDishOrder = new ArrayList<>();

                        finish();

                    }
                });

                break;
            case R.id.btnAdd:

                finish();

                break;
            case R.id.btnOrderNow:

                startActivity(ChooseDeliverAddressActivity.class);

                break;
        }
    }
}

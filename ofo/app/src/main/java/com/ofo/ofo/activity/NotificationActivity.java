package com.ofo.ofo.activity;

import android.os.Bundle;
import android.view.View;

import com.ofo.ofo.R;
import com.ofo.ofo.customview.SizeAdjustingTextView;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.fragment.NotificationListFragment;
import com.ofo.ofo.model.Notification;
import com.ofo.ofo.model.PagedMetadata;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.FloatingActionButton;
import carbon.widget.RelativeLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends BaseActivity {

    @BindView(R.id.tvTitle)
    SizeAdjustingTextView tvTitle;
    @BindView(R.id.root)
    RelativeLayout root;
    @BindView(R.id.btnClear)
    FloatingActionButton btnClear;

    NotificationListFragment frag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifi);
        ButterKnife.bind(this);

        tvTitle.setText(getText(R.string.notifications));

        frag = (NotificationListFragment) getSupportFragmentManager().findFragmentById(R.id.frag);

    }

    @Override
    protected void onResume() {
        super.onResume();

        getData();

    }

    @Override
    protected void getData() {
        super.getData();

        toastLoading();

        RestClient.get().getListNotification(Util.getPrefManager(getBaseContext()).get(Const.KEY_PREF_FCM_REG_ID).defaultValue("0").go(), Data.getAccountInfo(getBaseContext()) != null ? Data.getAccountInfo(getBaseContext()).getAccountId() : null).enqueue(new Callback<Result<PagedMetadata<List<Notification>>>>() {
            @Override
            public void onResponse(Call<Result<PagedMetadata<List<Notification>>>> call, Response<Result<PagedMetadata<List<Notification>>>> response) {

                toastOk();

                if (!ErrorUtil.foundAndHandledResponseError(response, root)) {

                    btnClear.setVisibility(response.body().getData().getData().size() > 0 ? View.VISIBLE : View.GONE);

                    frag.setListData(response.body().getData().getData());
                }

            }

            @Override
            public void onFailure(Call<Result<PagedMetadata<List<Notification>>>> call, Throwable t) {

                toastOk();

                ErrorUtil.showCannotConnectError(root);

            }

        });

    }

    @OnClick({R.id.btnBack, R.id.btnExit, R.id.btnClear})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnExit:

                finish();

                break;
            case R.id.btnClear:

                if (!Util.getPrefManager(getBaseContext()).get(Const.KEY_PREF_SIGNED_IN).defaultValue(false).go()) {

                    frag.setListData(new ArrayList());

                } else {

                    toastLoading();

                    RestClient.get().clearAllNotification(Data.getAccountInfo(getBaseContext()).getAccountId()).enqueue(new Callback<Result>() {
                        @Override
                        public void onResponse(Call<Result> call, Response<Result> response) {

                            toastOk();

                            if (!ErrorUtil.foundAndHandledResponseError(response, root)) {

                                getData();

                            }

                        }

                        @Override
                        public void onFailure(Call<Result> call, Throwable t) {

                            toastOk();

                            ErrorUtil.showCannotConnectError(root);

                        }
                    });

                }

                break;
        }
    }
}

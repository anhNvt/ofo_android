package com.ofo.ofo.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import carbon.widget.ViewPager;

/**
 * Created by cat on 7/18/16.
 */
public class HackyViewPager extends ViewPager {

    public HackyViewPager(Context context) {
        super(context);
    }

    public HackyViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HackyViewPager(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        try {

            return super.onInterceptTouchEvent(ev);

        } catch (IllegalArgumentException e) {

            return false;

        }

    }
}

package com.ofo.ofo.util;

import com.ofo.ofo.model.AccountInfo;
import com.ofo.ofo.model.Dish;
import com.ofo.ofo.model.DishMenu;
import com.ofo.ofo.model.Restaurant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cat on 5/17/16.
 */
public class GlobalData {

    public static List<DishMenu> listDishOrder = new ArrayList<>();

    public static Restaurant res;


    public static AccountInfo CURRENT_USER = null;

}

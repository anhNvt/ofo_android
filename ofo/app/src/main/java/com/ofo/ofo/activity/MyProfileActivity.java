package com.ofo.ofo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ofo.ofo.R;
import com.ofo.ofo.customview.SizeAdjustingTextView;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.model.AccountInfo;
import com.ofo.ofo.model.Address;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.viewholer.RowLocCuiHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyProfileActivity extends BaseActivity {

    List<Address> data;

    @BindView(R.id.tvTitle)
    SizeAdjustingTextView tvTitle;
    @BindView(R.id.imgViewAvatar)
    ImageView imgViewAvatar;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvLocation)
    carbon.widget.TextView tvLocation;
    @BindView(R.id.root)
    LinearLayout root;

    @BindView(R.id.listAddress)
    RecyclerView listAddress;

    AccountInfo info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.my_profile));

        listAddress.setHasFixedSize(true);

        listAddress.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    protected void onResume() {
        super.onResume();

        info = Data.getAccountInfo(this);

        Glide.with(this).load(info.getAvatar()).into(imgViewAvatar);

        tvName.setText(info.getName());

        tvLocation.setText(info.getCityName());

        listAddress.removeAllViews();

        RestClient.get().getListAddress(Data.getAccountInfo(this).getAccountId()).enqueue(new Callback<Result<List<Address>>>() {
            @Override
            public void onResponse(Call<Result<List<Address>>> call, Response<Result<List<Address>>> response) {

                if (!ErrorUtil.foundAndHandledResponseError(response, root)) {

                    data = response.body().getData();

                    displayData();

                }
            }

            @Override
            public void onFailure(Call<Result<List<Address>>> call, Throwable t) {

                ErrorUtil.showCannotConnectError(root);

            }
        });

    }

    @OnClick({R.id.btnBack, R.id.btnExit, R.id.imgViewAvatar, R.id.spanPerDetail, R.id.spanChangePass, R.id.spanCreateAdd})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnExit:

                finish();

                break;
            case R.id.imgViewAvatar:
                break;
            case R.id.spanPerDetail:


                startActivity(PersonalDetailActivity.class);

                break;
            case R.id.spanChangePass:

                startActivity(ChangePasswordActivity.class);

                break;
            case R.id.spanCreateAdd:

                Intent intent = new Intent(MyProfileActivity.this, CreateEditAddressActivity.class);
                intent.putExtra(Const.KEY_SEND_ADDRESS_MODE, CreateEditAddressActivity.MODE_ADD);
                startActivity(intent);

                break;
        }
    }

    @Override
    protected void displayData() {
        super.displayData();

        listAddress.setAdapter(new RecyclerView.Adapter<RowLocCuiHolder>() {
            @Override
            public RowLocCuiHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                return new RowLocCuiHolder(getLayoutInflater().inflate(R.layout.row_addres_profile, parent, false));
            }

            @Override
            public void onBindViewHolder(final RowLocCuiHolder holder, int position) {

                holder.tvName.setText(data.get(position).getAddressName());

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        CreateEditAddressActivity.address = data.get(holder.getAdapterPosition());
                        Intent intent = new Intent(MyProfileActivity.this, CreateEditAddressActivity.class);
                        intent.putExtra(Const.KEY_SEND_ADDRESS_MODE, CreateEditAddressActivity.MODE_EDIT);
                        startActivity(intent);

                    }
                });

            }

            @Override
            public int getItemCount() {
                return data.size();
            }
        });

    }
}

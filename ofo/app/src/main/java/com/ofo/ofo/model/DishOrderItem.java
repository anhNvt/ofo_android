package com.ofo.ofo.model;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.ofo.ofo.R;
import com.ofo.ofo.activity.DishOrderDetailActivity;
import com.ofo.ofo.activity.OrderActivity;
import com.ofo.ofo.util.GlobalData;
import com.ofo.ofo.viewholer.RowDishOrderHolder;

import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.IFilterable;
import eu.davidea.flexibleadapter.items.IHeader;
import eu.davidea.flexibleadapter.items.ISectionable;

/**
 * Created by cat on 5/17/16.
 */
public class DishOrderItem extends AbstractModelItem<RowDishOrderHolder> implements ISectionable<RowDishOrderHolder, IHeader>, IFilterable {

    DishMenu data;

    IHeader header;

    public DishOrderItem(String id, DishMenu data) {
        super(id);

        this.data = data;

        setSelectable(true);

    }

    @Override
    public IHeader getHeader() {
        return header;
    }

    @Override
    public void setHeader(IHeader header) {

        this.header = header;

    }

    @Override
    public int getLayoutRes() {

        return R.layout.row_res_dish;

    }

    @Override
    public RowDishOrderHolder createViewHolder(FlexibleAdapter adapter, LayoutInflater inflater, ViewGroup parent) {

        return new RowDishOrderHolder(inflater.inflate(getLayoutRes(), parent, false), adapter);

    }

    @Override
    public void bindViewHolder(FlexibleAdapter adapter, RowDishOrderHolder holder, int position, List payloads) {

        holder.tvName.setText(data.getName());

        holder.tvPrice.setText(data.getPrice() + " " + holder.itemView.getContext().getString(R.string.aed));

        holder.holder.setVisibility(OrderActivity.res.getOrderOnline().equals("0") ? View.VISIBLE : View.GONE);

        holder.imgViewIcon.setVisibility(OrderActivity.res.getOrderOnline().equals("0") ? View.GONE : View.VISIBLE);

        holder.imgViewCheck.setVisibility(OrderActivity.res.getOrderOnline().equals("0") ? View.GONE : View.VISIBLE);

        Glide.with(holder.itemView.getContext()).load(data.getImage()).placeholder(R.drawable.ic_dish).centerCrop().into(holder.imgViewIcon);

        if (GlobalData.listDishOrder.contains(data)) {

            holder.imgViewCheck.setImageResource(R.drawable.ic_order);

        } else {

            holder.imgViewCheck.setImageResource(R.drawable.ic_order_gray);

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (OrderActivity.res.getOrderOnline().equals("0")) {

                    return;

                }

                DishOrderDetailActivity.res = OrderActivity.res;

                DishOrderDetailActivity.dish = data;

                v.getContext().startActivity(new Intent(v.getContext(), DishOrderDetailActivity.class));

            }
        });

    }

    @Override
    public boolean filter(String constraint) {
        return data.getName().toLowerCase().trim().contains(constraint);
    }
}

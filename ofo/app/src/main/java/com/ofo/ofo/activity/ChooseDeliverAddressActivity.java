package com.ofo.ofo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.ofo.ofo.R;
import com.ofo.ofo.customview.SizeAdjustingTextView;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.model.Address;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.viewholer.RowLocCuiHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChooseDeliverAddressActivity extends BaseActivity {

    List<Address> data = new ArrayList<>();

    public static Restaurant res;

    @BindView(R.id.root)
    View root;

    @BindView(R.id.tvTitle)
    SizeAdjustingTextView tvTitle;
    @BindView(R.id.list)
    RecyclerView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_deliver_address);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.choose_address));

        list.setHasFixedSize(true);

        list.setLayoutManager(new LinearLayoutManager(this));

        toastLoading();

        RestClient.get().getListAddress(Data.getAccountInfo(this).getAccountId()).enqueue(new Callback<Result<List<Address>>>() {
            @Override
            public void onResponse(Call<Result<List<Address>>> call, Response<Result<List<Address>>> response) {

                if (!ErrorUtil.foundAndHandledResponseError(response, root)) {

                    toastOk();

                    data = response.body().getData();

                    displayData();

                } else {

                    toastErr();

                }

            }

            @Override
            public void onFailure(Call<Result<List<Address>>> call, Throwable t) {

                toastErr();

                ErrorUtil.showCannotConnectError(root);

            }
        });

    }

    @OnClick({R.id.btnBack, R.id.btnExit, R.id.btnCreateAddress})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnExit:

                finish();

                break;
            case R.id.btnCreateAddress:

                Intent intent = new Intent(ChooseDeliverAddressActivity.this, CreateEditAddressActivity.class);
                intent.putExtra(Const.KEY_SEND_ADDRESS_MODE, CreateEditAddressActivity.MODE_ADD);
                startActivity(intent);

                break;
        }
    }

    @Override
    protected void displayData() {
        super.displayData();

        list.setAdapter(new RecyclerView.Adapter<RowLocCuiHolder>() {
            @Override
            public RowLocCuiHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                return new RowLocCuiHolder(getLayoutInflater().inflate(R.layout.row_address, parent, false));
            }

            @Override
            public void onBindViewHolder(RowLocCuiHolder holder, final int position) {

                holder.tvName.setText(data.get(position).getAddressName());

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        DeliverAddressDetailActivity.address = data.get(position);

                        startActivity(DeliverAddressDetailActivity.class);

                    }
                });

            }

            @Override
            public int getItemCount() {
                return data.size();
            }
        });

    }
}

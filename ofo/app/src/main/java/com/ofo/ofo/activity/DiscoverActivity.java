package com.ofo.ofo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ofo.ofo.R;
import com.ofo.ofo.fragment.DiscoverDeliveryFragment;
import com.ofo.ofo.fragment.FeaturedListResFragment;
import com.ofo.ofo.fragment.ListDishFragment;
import com.ofo.ofo.fragment.RestaurantListFragment;
import com.ofo.ofo.fragment.TrendingListResFragment;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.Util;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.FloatingActionButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DiscoverActivity extends BaseActivity {

    @BindView(R.id.btnCapture)
    FloatingActionButton btnCapture;
    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discover);
        ButterKnife.bind(this);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                refreshFloatActionBtn();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        refreshFloatActionBtn();

        getData();

    }

    @OnClick({R.id.btnBack, R.id.btnSearch, R.id.btnCapture})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnSearch:

                startActivity(SearchActivity.class);

                break;
            case R.id.btnCapture:

                startActivityForResultWithCheckLogin(ChooseTakePicTypeActivity.class, ChooseTakePicTypeActivity.START_CODE, LoginSecondActivity.UPLOAD_DISH);

                break;
        }
    }

    void refreshFloatActionBtn() {

        btnCapture.setVisibility(mViewPager.getCurrentItem() == 1 ? View.VISIBLE : View.GONE);

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            switch (position) {

                case 0:

                    return new DiscoverDeliveryFragment();

                case 1:

                    return new ListDishFragment();

                case 2:

                    return new TrendingListResFragment();

                case 3:

                    return new FeaturedListResFragment();

                default:

                    return null;

            }

        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.res);
                case 1:
                    return getString(R.string.dishes);
                case 2:

                    return getString(R.string.feed_me);

                case 3:
                    return getString(R.string.featured);
            }
            return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ChooseTakePicTypeActivity.START_CODE) {

            if (resultCode == RESULT_OK) {

                PostDishActivity.photoPath = data.getStringExtra("photo_path");

                startActivityForResult(PostDishActivity.class, PostDishActivity.START_CODE);

            }

        }

        if (requestCode == PostDishActivity.START_CODE) {

            if (resultCode == RESULT_OK) {

                ListDishFragment frag = (ListDishFragment) mSectionsPagerAdapter.getItem(1);

                if (frag.getView() != null) {

                    frag.getListData(RestClient.get().getListDish());

                } else {

                    frag.setReloadData(true);

                }
            }

        }

    }

}

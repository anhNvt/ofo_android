package com.ofo.ofo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.ofo.ofo.R;
import com.ofo.ofo.fragment.DiscoverDeliveryFragment;
import com.ofo.ofo.fragment.FeaturedListResFragment;
import com.ofo.ofo.fragment.ListDishFragment;
import com.ofo.ofo.fragment.ResDetailFragment;
import com.ofo.ofo.fragment.ResPhotosFragment;
import com.ofo.ofo.fragment.ResReviewFragment;
import com.ofo.ofo.fragment.TrendingListResFragment;
import com.ofo.ofo.model.ResReview;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.network.RestClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.Button;
import carbon.widget.FloatingActionButton;

/**
 * Created by cat on 5/14/16.
 */
public class ResDetailActivity extends BaseActivity {

    @BindView(R.id.btnCapture)
    FloatingActionButton btnCapture;

    public static Restaurant res;

    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    public SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_res_detail);
        ButterKnife.bind(this);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                refreshFloatActionBtn();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        refreshFloatActionBtn();

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            switch (position) {

                case 0:

                    ResDetailFragment.res = res;

                    return new ResDetailFragment();

                case 1:

                    ResPhotosFragment.res = res;

                    return new ResPhotosFragment();

                case 2:

                    ResReviewFragment.res = res;

                    return new ResReviewFragment();

                default:

                    return null;

            }

        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.details);
                case 1:
                    return getString(R.string.photos);
                case 2:
                    return getString(R.string.reviews);
            }
            return null;
        }
    }

    @OnClick({R.id.btnBack, R.id.btnSearch, R.id.btnCapture})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnSearch:

                startActivity(SearchActivity.class);

                break;

            case R.id.btnCapture:

                goTakePhoto();

                break;

        }
    }

    public void goTakePhoto() {

        startActivityForResultWithCheckLogin(ChooseTakePicTypeActivity.class, ChooseTakePicTypeActivity.START_CODE, LoginSecondActivity.ADD_PHOTO);

    }

    public void goReview() {

        PostReviewResActivity.res = res;

        startActivityForResultWithCheckLogin(PostReviewResActivity.class, PostReviewResActivity.START_CODE, LoginSecondActivity.REVIEW);

    }

    void refreshFloatActionBtn() {

        btnCapture.setVisibility(mViewPager.getCurrentItem() == 1 ? View.VISIBLE : View.GONE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ChooseTakePicTypeActivity.START_CODE) {

            if (resultCode == RESULT_OK) {

                UploadResPhotoActivity.photoPath = data.getStringExtra("photo_path");

                UploadResPhotoActivity.res = res;

                startActivityForResult(UploadResPhotoActivity.class, UploadResPhotoActivity.START_CODE);

            }

        }

        if (requestCode == UploadResPhotoActivity.START_CODE) {

            if (resultCode == RESULT_OK) {

                ResPhotosFragment frag = (ResPhotosFragment) mSectionsPagerAdapter.getItem(1);

                if (frag.getView() != null) {

                    frag.getListPhoto(RestClient.get().getListResPhoto(res.getId()));

                } else {

                    frag.setReloadData(true);

                }

            }

        }

        if (requestCode == PostReviewResActivity.START_CODE) {}

    }

}

package com.ofo.ofo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ofo.ofo.util.LocaleHelper;

/**
 * Created by cat on 5/18/16.
 */
public class Address {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("account_id")
    @Expose
    private String accountId;
    @SerializedName("address_name")
    @Expose
    private String addressName;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("city_name_ar")
    @Expose
    private String cityNameAr;
    @SerializedName("area_id")
    @Expose
    private String areaId;
    @SerializedName("area_name")
    @Expose
    private String areaName;
    @SerializedName("area_name_ar")
    @Expose
    private String areaNameAr;
    @SerializedName("address_type_id")
    @Expose
    private String addressTypeId;
    @SerializedName("address_type_name")
    @Expose
    private String addressTypeName;
    @SerializedName("address_type_name_ar")
    @Expose
    private String addressTypeNameAr;
    @SerializedName("street_name")
    @Expose
    private String streetName;
    @SerializedName("building_name")
    @Expose
    private String buildingName;
    @SerializedName("floor")
    @Expose
    private String floor;
    @SerializedName("phone_no")
    @Expose
    private String phoneNo;
    @SerializedName("direction")
    @Expose
    private String direction;
    @SerializedName("status")
    @Expose
    private String status;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     *
     * @param accountId
     * The account_id
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     *
     * @return
     * The addressName
     */
    public String getAddressName() {
        return addressName;
    }

    /**
     *
     * @param addressName
     * The address_name
     */
    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    /**
     *
     * @return
     * The cityId
     */
    public String getCityId() {
        return cityId;
    }

    /**
     *
     * @param cityId
     * The city_id
     */
    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    /**
     *
     * @return
     * The cityName
     */
    public String getCityName() {

        if (LocaleHelper.getLanguage(LocaleHelper.LOCALE_ENG).equals(LocaleHelper.LOCALE_ENG)) {

            return cityName;

        }

        else return cityNameAr;

    }

    /**
     *
     * @param cityName
     * The city_name
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**
     *
     * @return
     * The cityNameAr
     */
    public String getCityNameAr() {
        return cityNameAr;
    }

    /**
     *
     * @param cityNameAr
     * The city_name_ar
     */
    public void setCityNameAr(String cityNameAr) {
        this.cityNameAr = cityNameAr;
    }

    /**
     *
     * @return
     * The areaId
     */
    public String getAreaId() {
        return areaId;
    }

    /**
     *
     * @param areaId
     * The area_id
     */
    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    /**
     *
     * @return
     * The areaName
     */
    public String getAreaName() {

        if (LocaleHelper.getLanguage(LocaleHelper.LOCALE_ENG).equals(LocaleHelper.LOCALE_ENG)) {

            return areaName;

        }

        else return areaNameAr;

    }

    /**
     *
     * @param areaName
     * The area_name
     */
    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    /**
     *
     * @return
     * The areaNameAr
     */
    public String getAreaNameAr() {
        return areaNameAr;
    }

    /**
     *
     * @param areaNameAr
     * The area_name_ar
     */
    public void setAreaNameAr(String areaNameAr) {
        this.areaNameAr = areaNameAr;
    }

    /**
     *
     * @return
     * The addressTypeId
     */
    public String getAddressTypeId() {
        return addressTypeId;
    }

    /**
     *
     * @param addressTypeId
     * The address_type_id
     */
    public void setAddressTypeId(String addressTypeId) {
        this.addressTypeId = addressTypeId;
    }

    /**
     *
     * @return
     * The addressTypeName
     */
    public String getAddressTypeName() {

        if (LocaleHelper.getLanguage(LocaleHelper.LOCALE_ENG).equals(LocaleHelper.LOCALE_ENG)) {

            return addressTypeName;

        }

        else return addressTypeNameAr;

    }

    /**
     *
     * @param addressTypeName
     * The address_type_name
     */
    public void setAddressTypeName(String addressTypeName) {
        this.addressTypeName = addressTypeName;
    }

    /**
     *
     * @return
     * The addressTypeNameAr
     */
    public String getAddressTypeNameAr() {
        return addressTypeNameAr;
    }

    /**
     *
     * @param addressTypeNameAr
     * The address_type_name_ar
     */
    public void setAddressTypeNameAr(String addressTypeNameAr) {
        this.addressTypeNameAr = addressTypeNameAr;
    }

    /**
     *
     * @return
     * The streetName
     */
    public String getStreetName() {
        return streetName;
    }

    /**
     *
     * @param streetName
     * The street_name
     */
    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    /**
     *
     * @return
     * The buildingName
     */
    public String getBuildingName() {
        return buildingName;
    }

    /**
     *
     * @param buildingName
     * The building_name
     */
    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    /**
     *
     * @return
     * The floor
     */
    public String getFloor() {
        return floor;
    }

    /**
     *
     * @param floor
     * The floor
     */
    public void setFloor(String floor) {
        this.floor = floor;
    }

    /**
     *
     * @return
     * The phoneNo
     */
    public String getPhoneNo() {
        return phoneNo;
    }

    /**
     *
     * @param phoneNo
     * The phone_no
     */
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    /**
     *
     * @return
     * The direction
     */
    public String getDirection() {
        return direction;
    }

    /**
     *
     * @param direction
     * The direction
     */
    public void setDirection(String direction) {
        this.direction = direction;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

}

package com.ofo.ofo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.ofo.ofo.R;
import com.ofo.ofo.customview.SizeAdjustingTextView;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.listener.OnUserInfoDownloadCompleted;
import com.ofo.ofo.model.AccountInfo;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.service.GetUserInfoService;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.GlobalData;
import com.ofo.ofo.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.LinearLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PersonalDetailActivity extends BaseActivity {

    AccountInfo info;

    @BindView(R.id.tvTitle)
    SizeAdjustingTextView tvTitle;
    @BindView(R.id.edtName)
    EditText edtName;
    @BindView(R.id.edtEmail)
    EditText edtEmail;
    @BindView(R.id.edtPhone)
    EditText edtPhone;
    @BindView(R.id.edtBuilding)
    EditText edtBuilding;
    @BindView(R.id.edtStreet)
    EditText edtStreet;
    @BindView(R.id.edtFlatVila)
    EditText edtFlatVila;
    @BindView(R.id.edtDirection)
    EditText edtDirection;
    @BindView(R.id.root)
    LinearLayout root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_detail);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.my_profile));

        info = GlobalData.CURRENT_USER;

        edtName.setText(info.getName());

        edtEmail.setText(info.getEmail());

        edtPhone.setText(info.getPhoneNo());

        edtBuilding.setText(info.getBuilding());

        edtStreet.setText(info.getStreet());

        edtFlatVila.setText(info.getFlatVillaNo());

        edtDirection.setText(info.getDirections());


        edtName.setHint(info.getName());

        edtEmail.setHint(info.getEmail());

        edtPhone.setHint(Util.stringNotNullOrEmpty(info.getPhoneNo()) ? info.getPhoneNo() : getString(R.string.mobile));

        edtBuilding.setHint(Util.stringNotNullOrEmpty(info.getBuilding()) ? info.getBuilding() : getString(R.string.building_name));

        edtStreet.setHint(Util.stringNotNullOrEmpty(info.getStreet()) ? info.getStreet() : getString(R.string.street_name));

        edtFlatVila.setHint(Util.stringNotNullOrEmpty(info.getFlatVillaNo()) ? info.getFlatVillaNo() : getString(R.string.flat_villa_unit_no));

        edtDirection.setHint(Util.stringNotNullOrEmpty(info.getDirections()) ? info.getDirections() : getString(R.string.direction_hint));

    }

    void update() {

        String errEmpty = getString(R.string.field_required);
        String errNotEmail = getString(R.string.field_not_email);

        edtName.setError(null);
        edtEmail.setError(null);
        edtPhone.setError(null);
        edtBuilding.setError(null);
        edtStreet.setError(null);
        edtFlatVila.setError(null);
        edtDirection.setError(null);

        String name = edtName.getText().toString();
        String emai = edtEmail.getText().toString();
        String phone = edtPhone.getText().toString();
        String building = edtBuilding.getText().toString();
        String street = edtStreet.getText().toString();
        String flatVila = edtFlatVila.getText().toString();
        String direction = edtDirection.getText().toString();

        if (TextUtils.isEmpty(name)) {
            edtName.setError(errEmpty);
            edtName.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(emai)) {
            edtEmail.setError(errEmpty);
            edtEmail.requestFocus();
            return;
        } else {
            if (!Util.isValidEmail(emai)) {
                edtEmail.setError(errNotEmail);
                edtEmail.requestFocus();
                return;
            }
        }

        if (TextUtils.isEmpty(phone)) {
            edtPhone.setError(errEmpty);
            edtPhone.requestFocus();
            return;
        }


//        showProgressDialog(getString(R.string.updating));

        toastLoading();

        RestClient.get().updateAccountInfo(info.getAccountId(), name, emai, phone, building, street, flatVila, direction).enqueue(new Callback<Result<AccountInfo>>() {
            @Override
            public void onResponse(Call<Result<AccountInfo>> call, Response<Result<AccountInfo>> response) {

                if (response.isSuccessful()) {

                    loadUserInfor();

                } else {

                    toastErr();

                    Util.showSnackBarInView(root, response.message());
                }

            }

            @Override
            public void onFailure(Call<Result<AccountInfo>> call, Throwable t) {
//                closeProgressDialog();

                toastErr();

                ErrorUtil.showCannotConnectError(root);

            }
        });

    }

    void loadUserInfor() {
        RestClient.get().getAccountInfo(info.getAccountId()).enqueue(new Callback<Result<AccountInfo>>() {
            @Override
            public void onResponse(Call<Result<AccountInfo>> call, Response<Result<AccountInfo>> response) {
//                closeProgressDialog();
                if (response.isSuccessful()) {

                    toastOk();

                    Util.showSnackBarInView(root, R.string.update_success);

                    GlobalData.CURRENT_USER = response.body().getData();
                    Data.createOrUpdate(getApplicationContext(),response.body().getData());

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    }, 500);

                } else {

                    toastErr();

                }

            }

            @Override
            public void onFailure(Call<Result<AccountInfo>> call, Throwable t) {
//                closeProgressDialog();

                toastErr();

                ErrorUtil.showCannotConnectError(root);
            }
        });
    }



    @OnClick({R.id.btnBack, R.id.btnExit, R.id.btnUpdate})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnExit:

                finish();

                break;
            case R.id.btnUpdate:
                update();
//                String name =
//
//                RestClient.get().updateAccountInfo(info.getAccountId(), )

                break;
        }
    }
}

package com.ofo.ofo.viewholer;

import android.view.View;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.ofo.ofo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import carbon.widget.ImageView;
import carbon.widget.TextView;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.viewholders.FlexibleViewHolder;

/**
 * Created by cat on 5/17/16.
 */
public class RowDishOrderHolder extends FlexibleViewHolder {

    @BindView(R.id.holder)
    public View holder;

    @BindView(R.id.imgViewIcon)
    public ImageView imgViewIcon;

    @BindView(R.id.tvName)
    public TextView tvName;
    @BindView(R.id.tvPrice)
    public android.widget.TextView tvPrice;
    @BindView(R.id.imgViewCheck)
    public ImageView imgViewCheck;

    /**
     * Default constructor.
     *
     * @param view    The {@link View} being hosted in this ViewHolder
     * @param adapter Adapter instance of type {@link FlexibleAdapter}
     */
    public RowDishOrderHolder(View view, FlexibleAdapter adapter) {
        super(view, adapter);

        ButterKnife.bind(this, view);

    }
}

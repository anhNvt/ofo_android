package com.ofo.ofo.fragment;

import android.view.View;

import com.ofo.ofo.R;
import com.ofo.ofo.model.Notification;
import com.ofo.ofo.viewholer.RowTextContentHolder;

/**
 * Created by cat on 7/11/16.
 */
public class NotificationListFragment extends ListFragment<Notification, RowTextContentHolder> {

    @Override
    protected int getRowLayoutResId() {

        return R.layout.row_notifi;

    }

    @Override
    protected RowTextContentHolder inflateViewHolder(View view) {

        return new RowTextContentHolder(view);

    }

    @Override
    protected void bindDataToHolder(RowTextContentHolder holder, Notification data, int position) {
        super.bindDataToHolder(holder, data, position);

        holder.tvContent.setText(data.getContent());

    }
}


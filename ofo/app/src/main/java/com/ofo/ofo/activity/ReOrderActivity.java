package com.ofo.ofo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.ofo.ofo.R;
import com.ofo.ofo.adapter.ReorderAdapter;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.model.Order;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.GsonUtils;
import com.ofo.ofo.util.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.LinearLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eo_cuong on 5/19/16.
 */
public class ReOrderActivity extends BaseActivity {
    @BindView(R.id.root)
    LinearLayout root;

    @BindView(R.id.tvTitle)
    android.widget.TextView tvTitle;

    @BindView(R.id.rcvOrder)
    RecyclerView rcvOrder;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    ReorderAdapter adapter;

    List<Order> orders = new ArrayList<>();

    int currentPage = 1;


    boolean isloadingMore = false;


    //scroll
    private int previousTotal = 0; // The total number of items in the dataset after the last load
    private boolean loading = true; // True if we are still waiting for the last set of data to load.
    private int visibleThreshold = 3; // The minimum amount of items to have below your current scroll position before loading more.
    int firstVisibleItem, visibleItemCount, totalItemCount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_my_order);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.re_order));

        rcvOrder.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(ReOrderActivity.this, LinearLayoutManager.VERTICAL, false);
        rcvOrder.setLayoutManager(layoutManager);
        adapter = new ReorderAdapter(orders, getApplicationContext());
        rcvOrder.setAdapter(adapter);
        rcvOrder.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItem = layoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {

                    //loadmore
                    if (!isloadingMore) {
                        isloadingMore = true;
                        loadData(false);

                    }

                    loading = true;
                }

            }


        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                resetScroll();
                loadData(true);
            }
        });


        adapter.setOnMyOrderAdapterClick(new ReorderAdapter.OnMyOrderAdapterClick() {
            @Override
            public void onDetailClick(int position, Order order) {

                Intent intent = new Intent(ReOrderActivity.this, DetailOrderActivity.class);
                intent.putExtra(Const.KEY_SEND_ORDER, GsonUtils.Object2String(order));
                startActivity(intent);


            }

            @Override
            public void onReorderClick(int position, Order order) {

                reOrder(order.getOrderId());

            }


            @Override
            public void onItemClick(int position, Order order) {

            }
        });

        loadData(false);


    }


    void resetScroll() {
        previousTotal = 0; // The total number of items in the dataset after the last load
        loading = true; // True if we are still waiting for the last set of data to load.
        visibleThreshold = 3; // The minimum amount of items to have below your current scroll position before loading more.
        firstVisibleItem = visibleItemCount = totalItemCount = 0;
    }

    void reOrder(int orderid) {
//        showProgressDialog(getString(R.string.reordering));

        toastLoading();

        RestClient.get().reorder(orderid).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

//                closeProgressDialog();

                if (response.isSuccessful()) {

                    toastOk();

                    Util.showSnackBarInView(root, R.string.order_success);
                } else {

                    toastErr();

                    Util.showSnackBarInView(root, response.message());
                }

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
//                closeProgressDialog();

                toastErr();

                ErrorUtil.showCannotConnectError(root);
            }
        });
    }


    void loadData(final boolean loadmore) {

//        showProgressDialog(getString(R.string.loading));

        toastLoading();

        if (loadmore) {
            currentPage = 1;
        }

        RestClient.get().getlistOrder(Data.getAccountInfo(getApplication()).getAccountId(), currentPage).enqueue(new Callback<Result<List<Order>>>() {
            @Override
            public void onResponse(Call<Result<List<Order>>> call, Response<Result<List<Order>>> response) {

//                closeProgressDialog();
                swipeRefreshLayout.setRefreshing(false);
                isloadingMore = false;

                if (response.isSuccessful()) {

                    toastOk();

                    if (loadmore) {
                        orders.clear();
                    }

                    orders.addAll(response.body().getData());
                    adapter.notifyDataSetChanged();

                    currentPage++;

                } else {

                    toastErr();

                    Util.showSnackBarInView(root, response.message());

                }


            }

            @Override
            public void onFailure(Call<Result<List<Order>>> call, Throwable t) {
//                closeProgressDialog();

                toastErr();

                swipeRefreshLayout.setRefreshing(false);
                isloadingMore = false;
                ErrorUtil.showCannotConnectError(root);

            }
        });

    }

    @OnClick({R.id.btnBack, R.id.btnExit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnExit:

                finish();

                break;

        }
    }
}

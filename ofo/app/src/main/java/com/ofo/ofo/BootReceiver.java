package com.ofo.ofo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ofo.ofo.service.OfoMsgService;

public class BootReceiver extends BroadcastReceiver {
    public BootReceiver() {}

    @Override
    public void onReceive(Context context, Intent intent) {

            context.startService(new Intent(context, OfoMsgService.class));

    }
}

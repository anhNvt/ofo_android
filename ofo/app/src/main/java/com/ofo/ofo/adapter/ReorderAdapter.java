package com.ofo.ofo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ofo.ofo.R;
import com.ofo.ofo.model.Order;
import com.ofo.ofo.util.Util;

import java.util.List;

/**
 * Created by eo_cuong on 5/19/16.
 */
public class ReorderAdapter extends RecyclerView.Adapter<ReorderAdapter.MyOrderViewHolder> {
    List<Order> orderList;
    Context context;

    public ReorderAdapter(List<Order> orderList, Context context) {
        this.orderList = orderList;
        this.context = context;
    }

    OnMyOrderAdapterClick onMyOrderAdapterClick = new OnMyOrderAdapterClick() {
        @Override
        public void onDetailClick(int position, Order order) {

        }

        @Override
        public void onReorderClick(int position, Order order) {

        }

        @Override
        public void onItemClick(int position, Order order) {

        }
    };

    public OnMyOrderAdapterClick getOnMyOrderAdapterClick() {
        return onMyOrderAdapterClick;
    }

    public void setOnMyOrderAdapterClick(OnMyOrderAdapterClick onMyOrderAdapterClick) {
        this.onMyOrderAdapterClick = onMyOrderAdapterClick;
    }

    @Override
    public MyOrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyOrderViewHolder(LayoutInflater.from(context).inflate(R.layout.item_order, null, false));
    }

    @Override
    public void onBindViewHolder(MyOrderViewHolder holder, int position) {

        Order order = orderList.get(position);

        holder.tvOrderName.setText(Util.isar_Arabic(context) ? order.getRestaurantNameAr() : order.getRestaurantName());
        holder.tvOrderId.setText(order.getOrderId() + "");
        holder.tvOrderDate.setText(order.getOrderTime() + "");
        holder.tvOrderTotal.setText("AED: " + order.getTotalAmount() + "");
        Glide.with(holder.itemView.getContext()).load(order.getRestaurantBannerUrl()).into(holder.imvOrder);


    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    class MyOrderViewHolder extends RecyclerView.ViewHolder {

        ImageView imvOrder;

        TextView tvOrderName;

        TextView tvOrderId;

        TextView tvOrderDate;

        TextView tvOrderTotal;

        TextView btnDetailOrder;

        TextView btnRateOrder;


        public MyOrderViewHolder(View itemView) {
            super(itemView);


            tvOrderName = (TextView) itemView.findViewById(R.id.tvOrderName);

            tvOrderId = (TextView) itemView.findViewById(R.id.tvOrderId);

            tvOrderDate = (TextView) itemView.findViewById(R.id.tvOrderDate);

            tvOrderTotal = (TextView) itemView.findViewById(R.id.tvOrderTotal);

            btnDetailOrder = (TextView) itemView.findViewById(R.id.btnDetailOrder);

            btnRateOrder = (TextView) itemView.findViewById(R.id.btnRateOrder);

            btnRateOrder.setText(context.getString(R.string.reorder));

            imvOrder = (ImageView) itemView.findViewById(R.id.imvOrder);

            btnDetailOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onMyOrderAdapterClick.onDetailClick(getAdapterPosition(), orderList.get(getAdapterPosition()));

                }
            });

            btnRateOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onMyOrderAdapterClick.onReorderClick(getAdapterPosition(), orderList.get(getAdapterPosition()));
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onMyOrderAdapterClick.onItemClick(getAdapterPosition(), orderList.get(getAdapterPosition()));
                }
            });

        }


    }

    public interface OnMyOrderAdapterClick {
        void onDetailClick(int position, Order order);

        void onReorderClick(int position, Order order);

        void onItemClick(int position, Order order);

    }
}

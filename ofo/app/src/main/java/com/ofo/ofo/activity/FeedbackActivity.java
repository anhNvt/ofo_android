package com.ofo.ofo.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.ofo.ofo.R;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackActivity extends BaseActivity {

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.edtMail)
    EditText edtMail;
    @BindView(R.id.edtDecs)
    EditText edtDecs;

    @BindView(R.id.root)
    View root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.feedback));

    }

    @OnClick({R.id.btnBack, R.id.btnExit, R.id.btnSend})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnExit:

                finish();

                break;
            case R.id.btnSend:

                toastLoading();

                RestClient.get().sendFeedback(edtMail.getText().toString(), edtDecs.getText().toString()).enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {

                        if (!ErrorUtil.foundAndHandledResponseError(getBaseContext(), response, root)) {

                            toastOk();

                            Util.showSnackBarInView(root, getString(R.string.send_feedback_success));

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    finish();

                                }
                            }, 500);

                        } else {

                            toastErr();

                        }

                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {

                        toastErr();

                        Util.showSnackBarInView(root, getString(R.string.can_not_connect));

                    }
                });

                break;
        }
    }
}

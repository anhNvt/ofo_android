package com.ofo.ofo.network;

import android.content.Intent;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ofo.ofo.model.AccountInfo;
import com.ofo.ofo.model.Address;
import com.ofo.ofo.model.AddressType;
import com.ofo.ofo.model.Area;
import com.ofo.ofo.model.AreaAddress;
import com.ofo.ofo.model.Cuisine;
import com.ofo.ofo.model.Dish;
import com.ofo.ofo.model.DishMenu;
import com.ofo.ofo.model.Faq;
import com.ofo.ofo.model.Location;
import com.ofo.ofo.model.Notification;
import com.ofo.ofo.model.Order;
import com.ofo.ofo.model.PagedMetadata;
import com.ofo.ofo.model.ResMenu;
import com.ofo.ofo.model.ResPhoto;
import com.ofo.ofo.model.ResReview;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.model.Result;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmObject;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by cat on 4/25/16.
 */
public class RestClient {

//    private static String baseUrl = "http://ofo.myaawu.com/api/";

    private static String baseUrl = "http://ofo.vnblues.net/api/";

    private RestClient() {
    }

    public interface OfoApi {

        @GET("city/list")
        Call<Result<ArrayList<Area>>> getListCity();

        @GET("area/list")
        Call<Result<List<AreaAddress>>> getListArea();


        @POST("account/register")
        Call<Result> signUp(@Query("email") String email, @Query("name") String name, @Query("password") String pass, @Query("city_id") Integer id);

        @POST("auth/login")
        Call<Result> login(@Query("email") String email, @Query("password") String pass);

        @GET("account/info")
        Call<Result<AccountInfo>> getAccountInfo(@Query("account_id") Integer id);

        @POST("account/forgotPassword")
        Call<Result> resetPass(@Query("email") String email);

        @GET("restaurant/promotion")
        Call<Result<List<Restaurant>>> getListResPromo(@Query("account_id") Integer account_id);

        @GET("restaurant/listFavourites")
        Call<Result<List<Restaurant>>> getWishListRes(@Query("account_id") Integer account_id);

        @POST("restaurant/removeFavourite")
        Call<Result> unfavoriteRes(@Query("account_id") Integer account_id, @Query("restaurant_id") Integer res_id);

        @GET("restaurant/filter")
        Call<Result<List<Restaurant>>> filterRes(@Query("name") String name, @Query("location_id") Integer location_id, @Query("cuisine_id") Integer cuisine_id, @Query("order_amount") Integer order_amount, @Query("delivery_time") Integer delivery_time, @Query("rating") Integer rate, @Query("account_id") Integer account_id);

        @GET("restaurant/newlyOpen")
        Call<Result<List<Restaurant>>> getListNewlyOpen(@Query("account_id") Integer account_id);

        @GET("restaurant/nearMe")
        Call<Result<List<Restaurant>>> getListResNearMe(@Query("lat") Float lat, @Query("long") Float lon, @Query("account_id") Integer acc_id);

        @GET("cuisine/list")
        Call<Result<List<Cuisine>>> getListCuisine();

        @Multipart
        @POST("restaurant/create")
        Call<Result> addRes(@Query("location_id") Integer loc_id, @Query("cuisine_id") Integer id, @Query("name") String name, @Query("phone_no") String phone, @Query("email") String mail, @Query("account_id") Integer account_id, @Part("image\"; filename=\"img.jpg\"") RequestBody img);

        @POST("feedback/send")
        Call<Result> sendFeedback(@Query("email") String mail, @Query("message") String mess);

        @GET("faq/detail")
        Call<Result<Faq>> getFaq();

        @POST("account/changeCity")
        Call<Result> changeCity(@Query("account_id") int acc_id, @Query("city_id") int city_id);

        @GET("restaurant/trending")
        Call<Result<List<Restaurant>>> getListResTrending(@Query("account_id") Integer account_id, @Query("discovery")Integer discover, @Query("delivery")Integer deliver);

        @GET("restaurant/feature")
        Call<Result<List<Restaurant>>> getListResFeatured(@Query("account_id") Integer account_id, @Query("discovery")Integer discover, @Query("delivery")Integer deliver);

        @GET("dishes/list")
        Call<Result<List<Dish>>> getListDish();

        @GET("restaurant/discover")
        Call<Result<List<Restaurant>>> getListResDiscover(@Query("location_id") Integer loc_id, @Query("cuisine_id") Integer cui_id, @Query("account_id") Integer account_id);

        @GET("restaurant/delivery")
        Call<Result<List<Restaurant>>> getListResDelivery(@Query("location_id") Integer loc_id, @Query("account_id") Integer account_id);

        @GET("location/list")
        Call<Result<List<Location>>> getListLocation();

        @Multipart
        @POST("dishes/upload")
        Call<Result> postDish(@Query("restaurant_id") Integer res_id, @Query("account_id") Integer acc_id, @Query("comment") String cmt, @Part("image\"; filename=\"img.jpg\"") RequestBody img);

        @POST("restaurant/favourite")
        Call<Result> favoriteRes(@Query("account_id") Integer account_id, @Query("restaurant_id") Integer res_id);

        @GET("restaurant/listPhoto")
        Call<Result<List<ResPhoto>>> getListResPhoto(@Query("restaurant_id") Integer res_id);

        @Multipart
        @POST("restaurant/uploadPhoto")
        Call<Result> uploadResPhoto(@Query("restaurant_id") Integer res_id, @Query("account_id") Integer acc_id, @Query("comment") String cmt, @Part("image\"; filename=\"img.jpg\"") RequestBody img);

        @GET("restaurant/listReview")
        Call<Result<List<ResReview>>> getListResReview(@Query("restaurant_id") Integer res_id);

        @POST("restaurant/uploadReview")
        Call<Result> postReviewRes(@Query("restaurant_id") Integer res_id, @Query("account_id") Integer acc_id, @Query("rating") Float rate, @Query("comment") String cmt);

        @GET("menu/list")
        Call<Result<List<ResMenu>>> getListResMenu(@Query("restaurant_id") Integer res_id);

        @GET("menu/dish")
        Call<Result<List<DishMenu>>> getListDishMenu(@Query("menu_id") Integer menu_id);

        @POST("order/create")
        Call<Result> order(@Query("account_id") Integer acc_id, @Query("address_id") Integer address_id, @Query("total_amount") Double total, @Query("restaurant_id") Integer res_id, @Query("items") String items);

        @GET("accountAddress/list")
        Call<Result<List<Address>>> getListAddress(@Query("account_id") Integer acc_id);

        @Multipart
        @POST("account/updateInfo")
        Call<Result<AccountInfo>> updateAccountInfo(@Query("account_id") Integer acc_id, @Query("name") String name, @Query("email") String mail, @Query("phone_no") String phone, @Query("building") String building, @Query("street") String street, @Query("flat_villa_no") String flat_villa_no, @Query("directions") String direction, @Part("avatar\"; filename=\"avatar.jpg\"") RequestBody avatar);

        @POST("account/updateInfo")
        Call<Result<AccountInfo>> updateAccountInfo(@Query("account_id") Integer acc_id, @Query("name") String name, @Query("email") String mail, @Query("phone_no") String phone, @Query("building") String building, @Query("street") String street, @Query("flat_villa_no") String flat_villa_no, @Query("directions") String direction);


        @GET("addressType/list")
        Call<Result<List<AddressType>>> getListAddType(@Query("account_id") Integer acc_id);

        @POST("accountAddress/create")
        Call<Result> createAdd(@Query("account_id") Integer acc_id, @Query("address_name") String name, @Query("city_id") Integer city_id, @Query("area_id") Integer area_id, @Query("address_type_id") Integer add_type_id, @Query("street_name") String street, @Query("building_name") String building, @Query("floor") String floor, @Query("phone_no") String phone, @Query("direction") String direction);

        @POST("accountAddress/update")
        Call<Result> updateAdd(@Query("address_id") Integer add_id, @Query("account_id") Integer acc_id, @Query("address_name") String name, @Query("city_id") Integer city_id, @Query("area_id") Integer area_id, @Query("address_type_id") Integer add_type_id, @Query("street_name") String street, @Query("building_name") String building, @Query("floor") String floor, @Query("phone_no") String phone, @Query("direction") String direction);

        @POST("accountAddress/delete")
        Call<Result> deleteAdd(@Query("address_id") Integer add_id, @Query("account_id") Integer acc_id);

        @POST("account/changePassword")
        Call<Result> changePass(@Query("account_id") Integer acc_id, @Query("old_password") String old_pass, @Query("new_password") String new_pass);

        @GET("order/list")
        Call<Result<List<Order>>> getlistOrder(@Query("account_id") Integer acc_id, @Query("page") int page);

        @POST("order/rate")
        Call<Result> ratingOrder(@Query("order_id") int orderId, @Query("account_id") int account_id, @Query("quantity_rating") int quantity_rating, @Query("delivery_rating") int delivery_rating, @Query("restaurant_rating") int restaurant_rating, @Query("comment") String comment);

        @POST("order/reorder")
        Call<Result> reorder(@Query("order_id") int order_id);

        @GET("restaurant/detail")
        Call<Result<Restaurant>> getRes(@Query("id") Integer id);

        @POST("auth/login")
        Call<Result> loginFacebook(@Query("email") String email, @Query("name") String name, @Query("social_id") String social_id);

        @GET("notification/list")
        Call<Result<PagedMetadata<List<Notification>>>> getListNotification(@Query("registration_id") String reg_id, @Query("account_id") Integer acc_id);

        @POST("notification/clear-all")
        Call<Result> clearAllNotification(@Query("account_id") Integer acc_id);

        @POST("notification/registration")
        Call<Result> setDeviceRegId(@Query("registration_id")String id, @Query("os_type")String os, @Query("account_id") Integer acc_id);

        @GET("restaurant/ourchoice")
        Call<Result<List<Restaurant>>> getListOurChoice(@Query("account_id") Integer acc_id);

    }

    private static OfoApi ofo;

    private static void setupRetrofit() {

        Gson gson = new GsonBuilder().setExclusionStrategies(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return f.getDeclaringClass().equals(RealmObject.class);
            }

            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        }).create();


        Retrofit retrofit = new Retrofit.Builder().baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ofo = retrofit.create(OfoApi.class);

    }

    static {

        setupRetrofit();

    }

    public static OfoApi get() {

        return ofo;

    }

}

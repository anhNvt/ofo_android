package com.ofo.ofo.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.ofo.ofo.database.Data;
import com.ofo.ofo.network.RestClient;

/**
 * Created by cat on 5/11/16.
 */
public class FeaturedListResFragment extends RestaurantListFragment {

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (listData == null) {

            Integer discover = null;

            Integer deliver = null;

            if (DiscoverDeliveryFragment.type == DiscoverDeliveryFragment.TYPE_DELIVERY) {


                deliver = 1;

            } else {

                discover = 1;

            }

            getListData(RestClient.get().getListResFeatured(Data.getAccountInfo(getContext()) != null ? Data.getAccountInfo(getContext()).getAccountId() : null, discover, deliver));

        } else {

            setAdapter();

        }

    }
}

package com.ofo.ofo.adapter;

import android.animation.Animator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;

/**
 * Created by cat on 5/17/16.
 */
public class OrderAdapter extends FlexibleAdapter<AbstractFlexibleItem> {

    public static final int HEADER = 0;

    AbstractFlexibleItem header;

    public OrderAdapter(@NonNull List<AbstractFlexibleItem> items) {
        super(items);

        setNotifyChangeOfUnfilteredItems(true);

    }

    public OrderAdapter(@NonNull List<AbstractFlexibleItem> items, @Nullable Object listeners) {
        super(items, listeners);

        setNotifyChangeOfUnfilteredItems(true);

    }

    @Override
    public void updateDataSet(List<AbstractFlexibleItem> items) {
        super.updateDataSet(items);

//        addHeader();

    }

    @Override
    public synchronized void filterItems(@NonNull List<AbstractFlexibleItem> unfilteredItems) {
        super.filterItems(unfilteredItems);

//        addHeader();

    }

//    @Override
//    public List<Animator> getAnimators(View itemView, int position, boolean isSelected) {
//
//        List<Animator> animators = new ArrayList<>();
//
//        switch (getItemViewType(position)) {
//
//            case HEADER:
//                addScaleInAnimator(animators, itemView, 0.0f);
//                break;
//            default:
//                if (isSelected)
//                    addSlideInFromRightAnimator(animators, itemView, 0.5f);
//                else
//                    addSlideInFromLeftAnimator(animators, itemView, 0.5f);
//                break;
//        }
//
//        return animators;
//
//    }

    public void setHeader(AbstractFlexibleItem header) {

        this.header = header;

    }

    public AbstractFlexibleItem getHeader() {

        return header;

    }

    private void addHeader() {

        if (header != null) {

            addItemWithDelay(0, header, 700L, true);

        }

    }

}

package com.ofo.ofo.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ofo.ofo.R;
import com.ofo.ofo.customview.SizeAdjustingTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.Button;
import carbon.widget.TextView;

/**
 * Created by cat on 5/23/16.
 */
public class BasicDialog extends BaseDialog {

    boolean hasCancelBtn = false;

    private String msg = "";

    private String okBtnText = null;

    private String cancelBtnText = null;

    private View.OnClickListener okClickListener;

    private View.OnClickListener cancelClickListener;

    private String title = "";

    @BindView(R.id.tvMsg)
    TextView tvMsg;
    @BindView(R.id.btnCancel)
    Button btnCancel;
    @BindView(R.id.btnOkay)
    Button btnOk;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    public boolean isHasCancelBtn() {
        return hasCancelBtn;
    }

    public void setHasCancelBtn(boolean hasCancelBtn) {
        this.hasCancelBtn = hasCancelBtn;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getOkBtnText() {
        return okBtnText != null ? okBtnText : getString(R.string.ok);
    }

    public void setOkBtnText(String okBtnText) {
        this.okBtnText = okBtnText;
    }

    public String getCancelBtnText() {
        return cancelBtnText != null ? cancelBtnText : getString(R.string.cancel);
    }

    public void setCancelBtnText(String cancelBtnText) {
        this.cancelBtnText = cancelBtnText;
    }

    public void setTitle(String title) {

        this.title = title;

    }

    public String getTitle() {

        return title;

    }

    public View.OnClickListener getOkClickListener() {
        return okClickListener;
    }

    public void setOkClickListener(View.OnClickListener okClickListener) {
        this.okClickListener = okClickListener;
    }

    public View.OnClickListener getCancelClickListener() {
        return cancelClickListener;
    }

    public void setCancelClickListener(View.OnClickListener cancelClickListener) {
        this.cancelClickListener = cancelClickListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.dialog_ofo_icon_basic, container, false);

        ButterKnife.bind(this, root);

        return root;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvTitle.setText(getTitle());

        tvMsg.setText(getMsg());

        btnOk.setText(getOkBtnText());

        if (getOkClickListener() != null) {

            btnOk.setOnClickListener(getOkClickListener());

        }

        btnCancel.setVisibility(isHasCancelBtn() ? View.VISIBLE : View.GONE);

        btnCancel.setText(getCancelBtnText());

        if (getCancelClickListener() != null) {

            btnCancel.setOnClickListener(getCancelClickListener());

        }

    }

    @OnClick({R.id.btnCancel, R.id.btnOkay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCancel:

                if (getDialog() != null) {

                    dismiss();

                }

                break;
            case R.id.btnOk:

                if (getDialog() != null) {

                    dismiss();

                }

                break;
        }
    }

}

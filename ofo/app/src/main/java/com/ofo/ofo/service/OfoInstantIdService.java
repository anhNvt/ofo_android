package com.ofo.ofo.service;

import android.content.Context;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.Util;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by cat on 7/16/16.
 */
public class OfoInstantIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String token = FirebaseInstanceId.getInstance().getToken();

        if (token != null) {

            Util.getPrefManager(getApplicationContext()).set(Const.KEY_PREF_FCM_REG_ID).value(token).go();

            if (Util.getPrefManager(getApplicationContext()).get(Const.KEY_PREF_SIGNED_IN).defaultValue(false).go()) {

                regToken(getApplicationContext());

            }

        }

    }

    public static void sendTokenToServer(Context ctx, String token) {

        RestClient.get().setDeviceRegId(token, "android", Data.getAccountInfo(ctx).getAccountId()).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });

    }

    public static void regToken(Context ctx) {


        sendTokenToServer(ctx, Util.getPrefManager(ctx).get(Const.KEY_PREF_FCM_REG_ID).defaultValue("").go());

    }

    public static void deleteToken(Context ctx) {

        sendTokenToServer(ctx, "");

    }

}

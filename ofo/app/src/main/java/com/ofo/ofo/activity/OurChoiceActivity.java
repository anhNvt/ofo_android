package com.ofo.ofo.activity;

import android.app.Activity;
import android.os.Bundle;

import com.ofo.ofo.R;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.Util;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OurChoiceActivity extends PromoActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tvTitle.setText(R.string.our_choice);

    }

    @Override
    protected void getData() {

        RestClient.get().getListOurChoice(Data.getAccountInfo(getBaseContext()) != null ? Data.getAccountInfo(getBaseContext()).getAccountId() : null).enqueue(new Callback<Result<List<Restaurant>>>() {
            @Override
            public void onResponse(Call<Result<List<Restaurant>>> call, Response<Result<List<Restaurant>>> response) {

                if (!ErrorUtil.foundAndHandledResponseError(getBaseContext(), response, root)) {

                    toastOk();

                    fragListRes.setListData(response.body().getData());

                } else {

                    toastErr();

                }

            }

            @Override
            public void onFailure(Call<Result<List<Restaurant>>> call, Throwable t) {

                toastErr();

                Util.showSnackBarInView(root, getString(R.string.can_not_connect));

            }
        });

    }
}

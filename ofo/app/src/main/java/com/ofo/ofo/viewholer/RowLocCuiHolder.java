package com.ofo.ofo.viewholer;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.ofo.ofo.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by cat on 5/12/16.
 */
public class RowLocCuiHolder extends UltimateRecyclerviewViewHolder {

    @BindView(R.id.tvName)
    public TextView tvName;

    public RowLocCuiHolder(View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);

    }
}

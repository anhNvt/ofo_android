package com.ofo.ofo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.EditText;

import com.ofo.ofo.R;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends BaseActivity {

    private static SignUpActivity instance;

    public static final int CODE_LOGIN = 999;

    @BindView(R.id.sign_up_form)
    View signUpForm;

    @BindView(R.id.edtEmail)
    EditText edtMail;

    @BindView(R.id.edtName)
    EditText edtName;

    @BindView(R.id.edtPass)
    EditText edtPass;

    @OnClick(R.id.btnExit) void exit() {

        finish();

    }

    @OnClick(R.id.btnGoLogin) void goLogin() {

        startActivityForResult(LoginActivity.class, CODE_LOGIN);

    }

    @OnClick(R.id.btnSkipSignUp) void skipSignUp(){

        startActivity(HomeActivity.class);

        finish();

    }

    @OnClick(R.id.btnSignUp) void signUp() {

        String name = edtName.getText().toString().trim();

        final String mail = edtMail.getText().toString().trim();

        final String pass = edtPass.getText().toString();

        if (name.length() < 1 || mail.length() < 1 || pass.length() <1) {

            Util.showSnackBarInView(signUpForm, getString(R.string.res_ifo_missing));

            return;

        }

        toastLoading();

        RestClient.get().signUp(mail, name, pass, Util.getPrefManager(this).get(Const.KEY_PREF_CITY_ID).defaultValue(1).go()).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

                if (!ErrorUtil.foundAndHandledResponseError(response, signUpForm)) {

                    if (response.body().getSuccess()) {

                        toastOk();

                        Intent i = new Intent(getBaseContext(), LoginActivity.class);

                        i.putExtra("mail", mail);

                        i.putExtra("pass", pass);

                        startActivityForResult(i, CODE_LOGIN);

                    } else {

                        toastErr();

                        Util.showSnackBarInView(signUpForm, getString(R.string.res_err));

                    }

                } else {

                    toastErr();

                }

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

                toastErr();

                Util.showSnackBarInView(signUpForm, getString(R.string.can_not_connect));

            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        ButterKnife.bind(this);

        instance = this;

    }

    public static SignUpActivity getInstance() {

        return instance;

    }

    @Override
    protected void onDestroy() {

        instance = null;

        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CODE_LOGIN) {

            if (resultCode == RESULT_OK) {

                startActivity(new Intent(this, HomeActivity.class));

                finish();

            }

        }

    }
}

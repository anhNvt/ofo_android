package com.ofo.ofo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ofo.ofo.util.LocaleHelper;

import java.io.Serializable;

/**
 * Created by cat on 5/7/16.
 */
public class Item implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("name_ar")
    @Expose
    private String nameAr;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {

        if (LocaleHelper.getLanguage(LocaleHelper.LOCALE_ENG).equals(LocaleHelper.LOCALE_ENG)) {

            return name;

        }

        else return nameAr;

    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The nameAr
     */
    public String getNameAr() {
        return nameAr;
    }

    /**
     *
     * @param nameAr
     * The name_ar
     */
    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }


}

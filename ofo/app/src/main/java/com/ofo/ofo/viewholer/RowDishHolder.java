package com.ofo.ofo.viewholer;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.ofo.ofo.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by cat on 5/11/16.
 */
public class RowDishHolder extends UltimateRecyclerviewViewHolder {

    @BindView(R.id.imgViewPic)
    public ImageView imgViewPic;
    @BindView(R.id.tvCity)
    public TextView tvCity;
    @BindView(R.id.tvName)
    public TextView tvName;
    @BindView(R.id.imgViewAvatar)
    public ImageView imgViewAvatar;
    @BindView(R.id.tvCmt)
    public TextView tvCmt;

    public RowDishHolder(View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);

    }

}

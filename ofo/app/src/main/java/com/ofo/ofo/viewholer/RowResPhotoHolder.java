package com.ofo.ofo.viewholer;

import android.view.View;
import android.widget.ImageView;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.ofo.ofo.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by cat on 5/14/16.
 */
public class RowResPhotoHolder extends UltimateRecyclerviewViewHolder {


    @BindView(R.id.imgView1)
    public ImageView imgView1;
    @BindView(R.id.imgView2)
    public ImageView imgView2;
    @BindView(R.id.imgView3)
    public ImageView imgView3;
    @BindView(R.id.imgView4)
    public ImageView imgView4;

    public RowResPhotoHolder(View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);

    }
}

package com.ofo.ofo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.ofo.ofo.R;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.Util;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.Button;
import carbon.widget.LinearLayout;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by cat on 5/15/16.
 */
public class UploadResPhotoActivity extends BaseActivity{

    public static final int START_CODE = 6;

    public static Restaurant res;

    public static String photoPath;

    File imgFile;

    @BindView(R.id.btnPost)
    Button btnPost;
    @BindView(R.id.btnExit)
    ImageView btnExit;
    @BindView(R.id.imgViewPic)
    ImageView imgViewPic;
    @BindView(R.id.edtCmt)
    EditText edtCmt;
    @BindView(R.id.root)
    LinearLayout root;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_upload_res_photo);

        ButterKnife.bind(this);

        btnExit.setVisibility(View.GONE);

        imgFile = new File(photoPath);

        Glide.with(this).load(imgFile).into(imgViewPic);

        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RequestBody img = RequestBody.create(MediaType.parse("image/*"), imgFile);

                toastLoading();

                RestClient.get().uploadResPhoto(res.getId(), Data.getAccountInfo(getBaseContext()).getAccountId(), edtCmt.getText().toString().trim(), img).enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {

                        if (!ErrorUtil.foundAndHandledResponseError(response, root)) {

                            toastOk();

                            Util.showSnackBarInView(root, getString(R.string.upload_photo_success));

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    setResult(RESULT_OK);

                                    finish();

                                }
                            }, 500);

                        } else {

                            toastErr();

                        }

                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {

                        toastErr();

                        Util.showSnackBarInView(root, getString(R.string.can_not_connect));

                    }
                });

            }
        });


    }

    @OnClick({R.id.btnBack, R.id.btnEditPic, R.id.btnPost})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnEditPic:

                startActivityForResult(ChooseTakePicTypeActivity.class, ChooseTakePicTypeActivity.START_CODE);

                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ChooseTakePicTypeActivity.START_CODE) {

            if (resultCode == RESULT_OK) {

                photoPath = data.getStringExtra("photo_path");

                imgFile = new File(photoPath);

                Glide.with(this).load(imgFile).into(imgViewPic);

            }

        }

    }
}

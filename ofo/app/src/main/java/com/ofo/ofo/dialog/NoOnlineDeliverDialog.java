package com.ofo.ofo.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ofo.ofo.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by cat on 5/20/16.
 */
public class NoOnlineDeliverDialog extends BaseDialog {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_no_online_deliver, container, false);
        ButterKnife.bind(this, view);
        return view;

    }

    @OnClick(R.id.btnOk)
    public void onClick() {

        dismiss();

    }
}

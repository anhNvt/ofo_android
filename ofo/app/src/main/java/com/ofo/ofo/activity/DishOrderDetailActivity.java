package com.ofo.ofo.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ofo.ofo.R;
import com.ofo.ofo.model.DishMenu;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.model.SubDish;
import com.ofo.ofo.model.SubDishItem;
import com.ofo.ofo.model.SubDishOption;
import com.ofo.ofo.model.SubDishOptionItem;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.GlobalData;
import com.ofo.ofo.util.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.FloatingActionButton;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.common.SmoothScrollLinearLayoutManager;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;

public class DishOrderDetailActivity extends BaseActivity {

    int count = 1;

    public static Restaurant res;

    public static DishMenu dish;

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvName)
    carbon.widget.TextView tvName;
    @BindView(R.id.tvPrice)
    carbon.widget.TextView tvPrice;
    @BindView(R.id.tvDesc)
    carbon.widget.TextView tvDesc;
    @BindView(R.id.edtRequest)
    EditText edtRequest;
    @BindView(R.id.tvBadge)
    carbon.widget.TextView tvBadge;
    @BindView(R.id.root)
    RelativeLayout root;

    @BindView(R.id.tvCount)
    carbon.widget.TextView tvCount;

    @BindView(R.id.imgView)
    ImageView imgView;
    @BindView(R.id.btnAdd)
    FloatingActionButton btnAdd;
    @BindView(R.id.list)
    RecyclerView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dish_order_detail);
        ButterKnife.bind(this);

        tvBadge.setText(GlobalData.listDishOrder.size() + "");

        Glide.with(this).load(dish.getImage()).placeholder(R.drawable.ic_dish_detail).centerCrop().into(imgView);

        count = dish.getQuantity();

        tvTitle.setText(res.getName());

        tvName.setText(dish.getName());

        tvPrice.setText(dish.getPrice() + getString(R.string.aed));

        tvCount.setText(dish.getQuantity() + "");

        edtRequest.setText(dish.getSpecialRequest());

        List<AbstractFlexibleItem> subDishItems = new ArrayList<>();

        for (SubDish s : dish.getSubDishes()) {

            SubDishItem subDishItem = new SubDishItem("SD" + s.getId(), s, null);

            List<SubDishOptionItem> optionItems = new ArrayList<>();

            for (SubDishOption op : s.getOptions()) {

                optionItems.add(new SubDishOptionItem("SD" + s.getId() + "OP" + op.getId(), op));

            }

            subDishItem.setOptions(optionItems);

            subDishItems.add(subDishItem);

        }

        FlexibleAdapter adapter = new FlexibleAdapter(subDishItems, this);

        adapter.setAnimationOnScrolling(true);

        adapter.setRemoveOrphanHeaders(false);

        list.setHasFixedSize(false);

        list.setLayoutManager(new SmoothScrollLinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        list.setItemAnimator(new DefaultItemAnimator(){

            @Override
            public boolean canReuseUpdatedViewHolder(@NonNull RecyclerView.ViewHolder viewHolder) {

                return true;
            }
        });

        list.setAdapter(adapter);

        adapter.enableStickyHeaders();

    }

    @OnClick({R.id.btnBack, R.id.btnSearch, R.id.btnAdd, R.id.btnDecreaseQt, R.id.btnAddQt})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnSearch:

                startActivity(SearchActivity.class);

                break;
            case R.id.btnAddQt:

                count++;

                tvCount.setText(count + "");

                btnAdd.setEnabled(true);

                break;
            case R.id.btnDecreaseQt:

                count = count > 0 ? count - 1 : 0;

                tvCount.setText(count + "");

                if (!GlobalData.listDishOrder.contains(dish) && count == 0) {

                    btnAdd.setEnabled(false);

                }

                break;
            case R.id.btnAdd:

                if (!Util.getPrefManager(getBaseContext()).get(Const.KEY_PREF_SIGNED_IN).defaultValue(false).go()) {

                    startActivity(LoginSecondActivity.class);

                    return;

                }

                if (count > 0) {
                    dish.setQuantity(count);

                    dish.setSpecialRequest(edtRequest.getText().toString());

                    if (!GlobalData.listDishOrder.contains(dish)) {

                        GlobalData.listDishOrder.add(dish);

                    }
                } else {

                    GlobalData.listDishOrder.remove(dish);

                }


                finish();

                break;
        }
    }
}

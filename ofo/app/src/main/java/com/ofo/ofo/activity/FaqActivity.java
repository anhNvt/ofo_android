package com.ofo.ofo.activity;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.ofo.ofo.R;
import com.ofo.ofo.model.Faq;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FaqActivity extends BaseActivity {

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvContent)
    carbon.widget.TextView tvContent;

    @BindView(R.id.root)
    View root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.faq));

        toastLoading();

        RestClient.get().getFaq().enqueue(new Callback<Result<Faq>>() {
            @Override
            public void onResponse(Call<Result<Faq>> call, Response<Result<Faq>> response) {

                if (!ErrorUtil.foundAndHandledResponseError(getBaseContext(), response, root)) {

                    toastOk();

                    tvContent.setText(Html.fromHtml(response.body().getData().getContent()));

                } else {

                    toastErr();

                }

            }

            @Override
            public void onFailure(Call<Result<Faq>> call, Throwable t) {

                toastErr();

                ErrorUtil.showCannotConnectError(root);

            }
        });

    }

    @OnClick({R.id.btnBack, R.id.btnExit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnExit:

                finish();

                break;
        }
    }
}

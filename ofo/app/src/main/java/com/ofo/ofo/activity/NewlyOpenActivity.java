package com.ofo.ofo.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ofo.ofo.R;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.fragment.RestaurantListFragment;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.Util;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.LinearLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewlyOpenActivity extends BaseActivity {

    @BindView(R.id.btnBack)
    ImageView btnBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.btnSearch)
    ImageView btnSearch;
    @BindView(R.id.root)
    LinearLayout root;

    RestaurantListFragment frag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newly_open);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.newly_open));

        frag = (RestaurantListFragment) getSupportFragmentManager().findFragmentById(R.id.frag);

        toastLoading();

        RestClient.get().getListNewlyOpen(Data.getAccountInfo(getBaseContext()) != null ? Data.getAccountInfo(getBaseContext()).getAccountId() : null).enqueue(new Callback<Result<List<Restaurant>>>() {
            @Override
            public void onResponse(Call<Result<List<Restaurant>>> call, Response<Result<List<Restaurant>>> response) {

                if (!ErrorUtil.foundAndHandledResponseError(getBaseContext(), response, root)) {

                    toastOk();

                    frag.setListData(response.body().getData());

                } else {

                    toastErr();

                }

            }

            @Override
            public void onFailure(Call<Result<List<Restaurant>>> call, Throwable t) {

                toastErr();

                Util.showSnackBarInView(root, getString(R.string.can_not_connect));

            }
        });

    }

    @OnClick({R.id.btnBack, R.id.btnSearch})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnSearch:

                startActivity(SearchActivity.class);

                break;
        }
    }
}

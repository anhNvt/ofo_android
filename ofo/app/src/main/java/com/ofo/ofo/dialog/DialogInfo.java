package com.ofo.ofo.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.ofo.ofo.R;
import com.ofo.ofo.customview.SizeAdjustingTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by eo_cuong on 5/18/16.
 */
public class DialogInfo extends Dialog {

    @BindView(R.id.tvMessage)
    SizeAdjustingTextView tvMessage;

    @BindView(R.id.btnClose)
    carbon.widget.Button btnClose;

    String text;

    public DialogInfo(Context context, String text) {
        super(context);
        this.text = text;

    }

    OnDialogClose onDialogClose = new OnDialogClose() {
        @Override
        public void onCloseClick() {

        }
    };

    public OnDialogClose getOnDialogClose() {
        return onDialogClose;
    }

    public void setOnDialogClose(OnDialogClose onDialogClose) {
        this.onDialogClose = onDialogClose;
    }

    public DialogInfo(Context context, int themeResId) {
        super(context, themeResId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_info);
        ButterKnife.bind(this);

        setCancelable(false);
        setCanceledOnTouchOutside(false);

        tvMessage.setText(text);
    }

    @OnClick(R.id.btnClose)
    public void close(View v) {
        dismiss();
        onClose();
        onDialogClose.onCloseClick();

    }

    void onClose() {

    }

    public interface OnDialogClose {
        void onCloseClick();
    }
}

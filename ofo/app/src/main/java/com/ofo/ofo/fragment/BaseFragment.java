package com.ofo.ofo.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.ofo.ofo.activity.LoginSecondActivity;
import com.ofo.ofo.dialog.ProgressOfoDialog;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.Util;

/**
 * Created by cat on 5/6/16.
 */
public class BaseFragment extends Fragment {

    ProgressOfoDialog dialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void getData(){}

    public void displayData(){}


    public void startActivity(Class c) {

        startActivity(new Intent(getContext(), c));

    }

    public void startActivityForResult(Class c, int code) {

        startActivityForResult(new Intent(getContext(), c), code);
    }

    public void showToastLoading() {

//        toast.show();

        dialog = new ProgressOfoDialog();

        dialog.show(getChildFragmentManager(), "progress_dialog");

    }

    public void toastOk() {

        dialog.dismiss();

    }

    public void toastErr() {

        dialog.dismiss();

    }

    protected void startActivityForResultWithCheckLogin(Class c, int code) {

        if (Util.getPrefManager(getContext()).get(Const.KEY_PREF_SIGNED_IN).defaultValue(false).go()) {

            startActivityForResult(new Intent(getContext(), c), code);

        } else {

            startActivity(LoginSecondActivity.class);

        }

    }

    protected void startActivityForResultWithCheckLogin(Class c, int request_code, int caller_code) {

        if (Util.getPrefManager(getContext()).get(Const.KEY_PREF_SIGNED_IN).defaultValue(false).go()) {

            startActivityForResult(new Intent(getContext(), c), request_code);

        } else {

            Intent i = new Intent(getContext(), LoginSecondActivity.class);

            i.putExtra("caller", caller_code);

            startActivity(i);

        }

    }

}

package com.ofo.ofo.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.marshalchen.ultimaterecyclerview.gridSection.SectionedRecyclerViewAdapter;
import com.marshalchen.ultimaterecyclerview.layoutmanagers.ScrollSmoothLineaerLayoutManager;
import com.ofo.ofo.R;
import com.ofo.ofo.model.Area;
import com.ofo.ofo.model.City;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.Util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.RadioButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AreaCityActivity extends BaseActivity {

    public static final int START_CODE = 99;

    int group = 0;

    int item = 0;

    int id = 1;

    ArrayList<Area> data = new ArrayList<>();

    @BindView(R.id.btnExit)
    ImageView btnExit;

    @BindView(R.id.list_area)
    RecyclerView list;

    @OnClick(R.id.btnBack) void back() {

        finish();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_city);

        ButterKnife.bind(this);

        btnExit.setVisibility(View.INVISIBLE);

        id = Util.getPrefManager(this).get(Const.KEY_PREF_CITY_ID).defaultValue(1).go();

        getData();

    }

    @Override
    protected void getData() {
        super.getData();

        toastLoading();

        RestClient.get().getListCity().enqueue(new Callback<Result<ArrayList<Area>>>() {
            @Override
            public void onResponse(Call<Result<ArrayList<Area>>> call, Response<Result<ArrayList<Area>>> response) {

                if (response == null || response.body() == null) {

                    toastErr();

                    return;

                }

                toastOk();

                data = response.body().getData();

                displayData();

            }

            @Override
            public void onFailure(Call<Result<ArrayList<Area>>> call, Throwable t) {

                toastErr();

            }
        });

    }

    @Override
    protected void displayData() {
        super.displayData();

        list.setHasFixedSize(false);

        list.setLayoutManager(new ScrollSmoothLineaerLayoutManager(this, ScrollSmoothLineaerLayoutManager.VERTICAL, false, 200));

        list.setAdapter(new SectionedRecyclerViewAdapter<AreaHolder, CityHolder, UltimateRecyclerviewViewHolder>() {
            @Override
            protected int getSectionCount() {
                return data.size();
            }

            @Override
            protected int getItemCountForSection(int section) {
                return data.get(section).getCities().size();
            }

            @Override
            protected boolean hasFooterInSection(int section) {
                return false;
            }

            @Override
            protected AreaHolder onCreateSectionHeaderViewHolder(ViewGroup parent, int viewType) {
                return new AreaHolder(getLayoutInflater().inflate(R.layout.row_area, parent, false));
            }

            @Override
            protected UltimateRecyclerviewViewHolder onCreateSectionFooterViewHolder(ViewGroup parent, int viewType) {
                return null;
            }

            @Override
            protected CityHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
                return new CityHolder(getLayoutInflater().inflate(R.layout.row_city, parent, false));
            }

            @Override
            protected void onBindSectionHeaderViewHolder(AreaHolder holder, int section) {

                Area area = data.get(section);

                Glide.with(getBaseContext()).load(area.getIcon()).into(holder.imgIcon);

                holder.tvName.setText(area.getCountryName());

            }

            @Override
            protected void onBindSectionFooterViewHolder(UltimateRecyclerviewViewHolder holder, int section) {

            }

            @Override
            protected void onBindItemViewHolder(CityHolder holder, int section, int position) {

                final City city = data.get(section).getCities().get(position);

                if (id == city.getId()) {

                    holder.btnChoose.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.colorPrimary));

                    holder.btnChoose.setChecked(true);

                } else {

                    holder.btnChoose.setTextColor(Color.parseColor("#444444"));

                    holder.btnChoose.setChecked(false);

                }

                holder.btnChoose.setText(city.getName());

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Util.getPrefManager(getBaseContext()).set(Const.KEY_PREF_CITY_ID).value(city.getId()).go();

                        Util.getPrefManager(getBaseContext()).set(Const.KEY_PREF_CITY_NAME).value(city.getName()).go();

                        setResult(RESULT_OK);

                        finish();

                    }
                });

            }
        });

    }

    public class AreaHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvOverlayName)
        public TextView tvName;

        @BindView(R.id.imgViewIcon)
        public ImageView imgIcon;

        public AreaHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

        }
    }

    public class CityHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.btnCheck)
        public RadioButton btnChoose;

        public CityHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);


        }
    }

}

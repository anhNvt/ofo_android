package com.ofo.ofo.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.marshalchen.ultimaterecyclerview.quickAdapter.easyRegularAdapter;
import com.ofo.ofo.R;
import com.ofo.ofo.listener.OnItemClickListener;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.util.ErrorUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by cat on 5/7/16.
 */
public class ListFragment<T, VH extends  UltimateRecyclerviewViewHolder> extends BaseFragment{

    Boolean reloadData;

    VH holder;

    List listData;

    boolean fixedSize;

    OnItemClickListener<T> listener;

    @BindView(R.id.list)
    UltimateRecyclerView list;

    public ListFragment() {}

//    public static ListFragment newInstance(List listData, boolean fixedSize) {
//
//        ListFragment frag = new ListFragment();
//
//        Bundle arg = new Bundle();
//
//        arg.putSerializable("listData", (Serializable) listData);
//
//        arg.putBoolean("fixedSize", fixedSize);
//
//        frag.setArguments(arg);
//
//        return frag;
//
//    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        listData = (List) getArguments().get("listData");
//
//        fixedSize = getArguments().getBoolean("fixedSize");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        list.setHasFixedSize(fixedSize);

        list.setLayoutManager(new LinearLayoutManager(getContext()));

    }

    public Boolean willReloadData() {

        if (reloadData == null) {

            reloadData = false;

        }

        return reloadData;

    }

    public void setReloadData(Boolean reloadData) {

        this.reloadData = reloadData;

    }

    public List getSourceData() {

        return this.listData;

    }

    public void setListData(List listData) {

        this.listData = listData;

        setAdapter();

    }

    public void getListData(Call<Result<List<T>>> call) {

        call.enqueue(new Callback<Result<List<T>>>() {
            @Override
            public void onResponse(Call<Result<List<T>>> call, Response<Result<List<T>>> response) {

                if (!ErrorUtil.foundAndHandledResponseError(getContext(), response, getView()) && isAdded()) {

                    setListData(response.body().getData());

                }

            }

            @Override
            public void onFailure(Call<Result<List<T>>> call, Throwable t) {

                ErrorUtil.showCannotConnectError(getView());

            }
        });

    }

    public void setOnItemClickListener(OnItemClickListener<T> listener) {

        this.listener = listener;

    }

    protected void setAdapter() {

        list.setAdapter(new easyRegularAdapter<T, VH>(listData) {
            @Override
            protected int getNormalLayoutResId() {

                return getRowLayoutResId();

            }

            @Override
            protected VH newViewHolder(View view) {

              return inflateViewHolder(view);

            }

            @Override
            protected void withBindHolder(VH holder, T data, int position) {

               bindDataToHolder(holder, data, position);

            }

        });

    }

    protected int getRowLayoutResId() {
        return 0;
    }

    protected VH inflateViewHolder(View view) {
        return null;
    }

    protected void bindDataToHolder(VH holder, final T data, final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (listener != null) {

                    listener.onItemClick(v, position, data);

                }

            }
        });

    }
}

package com.ofo.ofo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ofo.ofo.util.LocaleHelper;

/**
 * Created by cat on 5/11/16.
 */
public class Dish {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("account_id")
    @Expose
    private Integer accountId;
    @SerializedName("account_name")
    @Expose
    private String accountName;
    @SerializedName("account_email")
    @Expose
    private String accountEmail;
    @SerializedName("account_phone")
    @Expose
    private String accountPhone;
    @SerializedName("restaurant_id")
    @Expose
    private Integer restaurantId;
    @SerializedName("restaurant_name")
    @Expose
    private String restaurantName;
    @SerializedName("restaurant_name_ar")
    @Expose
    private String restaurantNameAr;
    @SerializedName("restaurant_address")
    @Expose
    private String restaurantAddress;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("comment")
    @Expose
    private String comment;

    @SerializedName("account_avatar")
    @Expose
    private String AccountAvatarUrl;

    @SerializedName("location")
    @Expose
    private String location;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The accountId
     */
    public Integer getAccountId() {
        return accountId;
    }

    /**
     *
     * @param accountId
     * The account_id
     */
    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    /**
     *
     * @return
     * The accountName
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     *
     * @param accountName
     * The account_name
     */
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    /**
     *
     * @return
     * The accountEmail
     */
    public String getAccountEmail() {
        return accountEmail;
    }

    /**
     *
     * @param accountEmail
     * The account_email
     */
    public void setAccountEmail(String accountEmail) {
        this.accountEmail = accountEmail;
    }

    /**
     *
     * @return
     * The accountPhone
     */
    public String getAccountPhone() {
        return accountPhone;
    }

    /**
     *
     * @param accountPhone
     * The account_phone
     */
    public void setAccountPhone(String accountPhone) {
        this.accountPhone = accountPhone;
    }

    /**
     *
     * @return
     * The restaurantId
     */
    public Integer getRestaurantId() {
        return restaurantId;
    }

    /**
     *
     * @param restaurantId
     * The restaurant_id
     */
    public void setRestaurantId(Integer restaurantId) {
        this.restaurantId = restaurantId;
    }

    /**
     *
     * @return
     * The restaurantName
     */
    public String getRestaurantName() {

        if (LocaleHelper.getLanguage(LocaleHelper.LOCALE_ENG).equals(LocaleHelper.LOCALE_ENG)) {

            return restaurantName;

        }

        else return restaurantNameAr;

    }

    /**
     *
     * @param restaurantName
     * The restaurant_name
     */
    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    /**
     *
     * @return
     * The restaurantNameAr
     */
    public String getRestaurantNameAr() {
        return restaurantNameAr;
    }

    /**
     *
     * @param restaurantNameAr
     * The restaurant_name_ar
     */
    public void setRestaurantNameAr(String restaurantNameAr) {
        this.restaurantNameAr = restaurantNameAr;
    }

    /**
     *
     * @return
     * The restaurantAddress
     */
    public String getRestaurantAddress() {
        return restaurantAddress;
    }

    /**
     *
     * @param restaurantAddress
     * The restaurant_address
     */
    public void setRestaurantAddress(String restaurantAddress) {
        this.restaurantAddress = restaurantAddress;
    }

    /**
     *
     * @return
     * The imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     *
     * @param imageUrl
     * The image_url
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     *
     * @return
     * The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     * The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAccountAvatarUrl() {
        return AccountAvatarUrl;
    }

    public void setAccountAvatarUrl(String accountAvatarUrl) {
        this.AccountAvatarUrl = accountAvatarUrl;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}

package com.ofo.ofo.viewholer;

import android.view.View;

import com.ofo.ofo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import carbon.widget.RadioButton;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.viewholders.FlexibleViewHolder;

/**
 * Created by cat on 7/21/16.
 */
public class RowSubDishOptionHolder extends FlexibleViewHolder {

    @BindView(R.id.btnOption)
    public RadioButton btnOption;

    public RowSubDishOptionHolder(View view, FlexibleAdapter adapter) {
        super(view, adapter);

        ButterKnife.bind(this, view);

    }

    public RowSubDishOptionHolder(View view, FlexibleAdapter adapter, boolean stickyHeader) {
        super(view, adapter, stickyHeader);

        ButterKnife.bind(this, view);

    }
}

package com.ofo.ofo.viewholer;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ofo.ofo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.viewholders.FlexibleViewHolder;

/**
 * Created by cat on 5/17/16.
 */
public class OrderHeaderHolder extends FlexibleViewHolder {

    @BindView(R.id.imgViewCover)
    public ImageView imgViewCover;
    @BindView(R.id.tvAddress)
    public TextView tvAddress;
    @BindView(R.id.tvName)
    public TextView tvName;
    @BindView(R.id.tvPrice)
    public TextView tvPrice;
    @BindView(R.id.tvAvrTimeDeliver)
    public TextView tvAvrTimeDeliver;

    /**
     * Default constructor.
     *
     * @param view    The {@link View} being hosted in this ViewHolder
     * @param adapter Adapter instance of type {@link FlexibleAdapter}
     */
    public OrderHeaderHolder(View view, FlexibleAdapter adapter) {
        super(view, adapter);

        ButterKnife.bind(this, itemView);

    }
}

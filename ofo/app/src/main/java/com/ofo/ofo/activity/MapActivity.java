package com.ofo.ofo.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.ofo.ofo.R;
import com.ofo.ofo.model.Restaurant;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapActivity extends BaseActivity implements OnMapReadyCallback {

    public static Restaurant res;

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_near_me_map);
        ButterKnife.bind(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        IconGenerator factory = new IconGenerator(this);

        factory.setBackground(ContextCompat.getDrawable(this, R.drawable.ic_marker));

        Bitmap icon = factory.makeIcon();

        MarkerOptions marker = new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(icon)).position(new LatLng(Double.parseDouble(res.getLat()), Double.parseDouble(res.getLng()))).title(res.getName());

        mMap.addMarker(marker);

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(res.getLat()), Double.parseDouble(res.getLng())), 13f));


    }

    @OnClick(R.id.btnBack)
    public void onClick() {

        finish();

    }
}

package com.ofo.ofo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.ofo.ofo.R;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.fragment.ListResNameOnlyFragment;
import com.ofo.ofo.listener.OnItemClickListener;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.Util;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchResNameOnlyActivity extends BaseActivity implements OnItemClickListener<Restaurant> {

    public static final int START_CODE = 3;

    @BindView(R.id.btnBack)
    ImageView btnBack;
    @BindView(R.id.edtSearch)
    EditText edtSearch;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.root)
    LinearLayout root;

    ListResNameOnlyFragment frag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search_res_name_only);

        ButterKnife.bind(this);

        frag = (ListResNameOnlyFragment) getSupportFragmentManager().findFragmentById(R.id.frag);

        frag.setOnItemClickListener(this);

        edtSearch.setHintTextColor(ContextCompat.getColor(getBaseContext(), R.color.colorPrimary));

        edtSearch.setHint(getString(R.string.res));

        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                        Util.hideSoftKeyboard(getBaseContext(), root);

                        search();

                    return true;

                }

                return false;
            }
        });

        search();

    }

    @OnClick({R.id.btnBack, R.id.btnClear})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnClear:

                edtSearch.setText("");

                edtSearch.requestFocus();

                break;
        }
    }

    @Override
    public void onItemClick(View v, int pos, Restaurant item) {

        Intent i = new Intent();

        i.putExtra("res", item);

        setResult(RESULT_OK, i);

        finish();

    }

    void search() {

        toastLoading();

        RestClient.get().filterRes(edtSearch.getText().toString(), null, null, null, null, null, Data.getAccountInfo(getBaseContext()) != null ? Data.getAccountInfo(getBaseContext()).getAccountId() : null).enqueue(new Callback<Result<List<Restaurant>>>() {
            @Override
            public void onResponse(Call<Result<List<Restaurant>>> call, Response<Result<List<Restaurant>>> response) {

                if (!ErrorUtil.foundAndHandledResponseError(getBaseContext(), response, root)) {

                    toastOk();

                    frag.setListData(response.body().getData());

                } else {

                    toastErr();

                }

            }

            @Override
            public void onFailure(Call<Result<List<Restaurant>>> call, Throwable t) {

                toastErr();

                Util.showSnackBarInView(root, getString(R.string.can_not_connect));

            }
        });

    }

}

package com.ofo.ofo.viewholer;

import android.view.View;
import com.ofo.ofo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import carbon.widget.TextView;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.viewholders.ExpandableViewHolder;
import eu.davidea.viewholders.FlexibleViewHolder;

/**
 * Created by cat on 5/17/16.
 */
public class RowMenuOrderHolder extends ExpandableViewHolder {

    @BindView(R.id.tvName)
    public TextView tvName;

    /**
     * Default constructor.
     *
     * @param view    The {@link View} being hosted in this ViewHolder
     * @param adapter Adapter instance of type {@link FlexibleAdapter}
     */
    public RowMenuOrderHolder(View view, FlexibleAdapter adapter) {
        super(view, adapter);

        ButterKnife.bind(this, view);
    }

    @Override
    protected void expandView(int position) {
        super.expandView(position);

        if (mAdapter.isExpanded(position)) {

            mAdapter.notifyItemChanged(position, true);

        }

    }

    @Override
    protected void collapseView(int position) {
        super.collapseView(position);

        if (!mAdapter.isExpanded(position)) {

            mAdapter.notifyItemChanged(position, true);

        }

    }
}

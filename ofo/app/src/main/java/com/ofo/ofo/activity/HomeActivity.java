package com.ofo.ofo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.util.Log;
import android.view.View;

import com.facebook.FacebookSdk;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.ofo.ofo.R;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.fragment.DiscoverDeliveryFragment;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.service.OfoInstantIdService;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.GlobalData;
import com.ofo.ofo.util.LocaleHelper;
import com.ofo.ofo.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.DrawerLayout;
import carbon.widget.TextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends BaseActivity {

    public static final int CODE_CHECK_LOGIN_PROFILE = 101;

    public static final int CODE_CHECK_LOGIN_RE_ORDER = 103;

    public static final int CODE_CHECK_LOGIN_RATE_ORDER = 104;

    public static final int CODE_CHECK_LOGIN_ADD_RESTAURANT = 105;

    public static final int CODE_LOGIN = 102;

    public static final int CODE_CHECK_LOGIN_WISH_LIST = 106;

    public static final int CODE_CHECK_LOGIN_NOTIFICATION = 107;

    @BindView(R.id.content)
    View content;

    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;

    @BindView(R.id.tvTitleLogin)
    TextView tvLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        if (Data.getAccountInfoLogin(getApplicationContext()) != null) {
            GlobalData.CURRENT_USER = Data.getAccountInfo(getApplicationContext());
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Util.getPrefManager(getBaseContext()).get(Const.KEY_PREF_SIGNED_IN).defaultValue(false).go()) {

            tvLogin.setText(getText(R.string.logout));

        } else {

            tvLogin.setText(getText(R.string.login));

        }

    }

    @OnClick(R.id.btnMenu)
    void openMenu() {

        drawerLayout.openDrawer(GravityCompat.START);

    }

    @OnClick({R.id.btnNearMe, R.id.btnSearch, R.id.btnDelivery, R.id.btnDiscover, R.id.btnWish, R.id.btnNewly, R.id.spanMyProfile, R.id.spanMyFriend, R.id.spanPromo, R.id.spanRate, R.id.spanReOrder, R.id.spanChangeCity, R.id.spanLang, R.id.spanAddRes, R.id.spanFaq, R.id.spanFeedback, R.id.spanLogin, R.id.spanHome, R.id.btnOurChoice, R.id.spanInvite, R.id.spanNotification, R.id.spanInstagram, R.id.spanFacebook})
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btnNearMe:

                startActivity(NearMeActivity.class);

                break;
            case R.id.btnSearch:

                startActivity(SearchActivity.class);

                break;
            case R.id.btnDelivery:

                DiscoverDeliveryFragment.type = DiscoverDeliveryFragment.TYPE_DELIVERY;

                startActivity(DiscoverActivity.class);

                break;
            case R.id.btnDiscover:

                DiscoverDeliveryFragment.type = DiscoverDeliveryFragment.TYPE_DISCOVER;

                startActivity(DiscoverActivity.class);

                break;
            case R.id.btnWish:

                startActivityWithCheck(WishListActivity.class, CODE_CHECK_LOGIN_WISH_LIST, LoginSecondActivity.WISH_LIST);

                break;
            case R.id.btnNewly:

                startActivity(NewlyOpenActivity.class);

                break;
            case R.id.spanMyProfile:

                startActivityWithCheck(MyProfileActivity.class, CODE_CHECK_LOGIN_PROFILE, LoginSecondActivity.MY_PROFILE);

                break;
            case R.id.spanMyFriend:
                break;
            case R.id.spanPromo:

                startActivity(PromoActivity.class);

                break;
            case R.id.spanRate:

                startActivityWithCheck(RateOrderActivity.class, CODE_CHECK_LOGIN_RATE_ORDER, LoginSecondActivity.RATE_MY_ORDER);

                break;
            case R.id.spanReOrder:

                startActivityWithCheck(ReOrderActivity.class, CODE_CHECK_LOGIN_RE_ORDER, LoginSecondActivity.RE_ORDER);

                break;
            case R.id.spanChangeCity:

                startActivityForResult(AreaCityActivity.class, AreaCityActivity.START_CODE);

                break;
            case R.id.spanLang:

                if (LocaleHelper.getLanguage(getBaseContext(), LocaleHelper.LOCALE_ENG).equals(LocaleHelper.LOCALE_ENG)) {

                    LocaleHelper.setLocale(getBaseContext(), LocaleHelper.LOCALE_ARAB);

                } else {

                    LocaleHelper.setLocale(getBaseContext(), LocaleHelper.LOCALE_ENG);

                }

                recreate();

                break;
            case R.id.spanAddRes:

                startActivityWithCheck(AddResActivity.class,CODE_CHECK_LOGIN_ADD_RESTAURANT, LoginSecondActivity.ADD_RES);

                break;
            case R.id.spanFaq:

                startActivity(FaqActivity.class);

                break;
            case R.id.spanFeedback:

                startActivity(FeedbackActivity.class);

                break;
            case R.id.spanLogin:

                if (Util.getPrefManager(getBaseContext()).get(Const.KEY_PREF_SIGNED_IN).defaultValue(false).go()) {

                    OfoInstantIdService.deleteToken(getBaseContext());

                    Data.deleteAll(getBaseContext());

                    Util.getPrefManager(getBaseContext()).set(Const.KEY_PREF_SIGNED_IN).value(false).go();

                }

                startActivity(SignUpActivity.class);

                finish();

                break;
            case R.id.spanHome:

                break;

            case R.id.btnOurChoice:

                startActivity(OurChoiceActivity.class);

                break;

            case R.id.spanFacebook:

                Util.startViewIntent(this, "https://www.facebook.com/badassFood");

                break;

            case R.id.spanNotification:

                startActivityWithCheck(NotificationActivity.class, CODE_CHECK_LOGIN_NOTIFICATION, LoginSecondActivity.NOTIFICATION);

                break;

            case R.id.spanInvite:

                if (!FacebookSdk.isInitialized()){

                    FacebookSdk.sdkInitialize(getApplicationContext());

                }

                if (AppInviteDialog.canShow()) {
                    AppInviteContent content = new AppInviteContent.Builder()
                            .setApplinkUrl("https://fb.me/657492821071470")
                            .setPreviewImageUrl("https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/13626388_570869806448421_5624860424488684955_n.png?oh=bf29e7e2404184fb95f594e5cfe6e3e1&oe=5831C660")
                            .build();
                    AppInviteDialog.show(this, content);
                }

                break;

        }

        drawerLayout.closeDrawer(GravityCompat.START);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AreaCityActivity.START_CODE) {

            if (resultCode == RESULT_OK) {

                int id = Util.getPrefManager(this).get(Const.KEY_PREF_CITY_ID).defaultValue(1).go();

                if (Data.getAccountInfo(getBaseContext()) == null) {

                    return;

                }

                toastLoading();

                RestClient.get().changeCity(Data.getAccountInfo(getBaseContext()).getAccountId(), id).enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {

                        if (!ErrorUtil.foundAndHandledResponseError(getBaseContext(), response, content)) {

                            toastOk();

                            Util.showSnackBarInView(content, getString(R.string.change_city_succeeded));

                        } else {

                            toastErr();

                        }

                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {

                        toastErr();

                        ErrorUtil.showCannotConnectError(content);

                    }
                });

            }

        } else if (requestCode == CODE_CHECK_LOGIN_PROFILE) {

            if (resultCode == RESULT_OK) {

                startActivity(MyProfileActivity.class);

            }

        } else if (requestCode == CODE_CHECK_LOGIN_RE_ORDER) {

            if (resultCode == RESULT_OK) {

                startActivity(ReOrderActivity.class);

            }

        } else if (requestCode == CODE_CHECK_LOGIN_RATE_ORDER) {

            if (resultCode == RESULT_OK) {

                startActivity(RateOrderActivity.class);

            }
        } else if (requestCode == CODE_CHECK_LOGIN_ADD_RESTAURANT) {

            if (resultCode == RESULT_OK) {

                startActivity(AddResActivity.class);

            }

        } else if (requestCode == CODE_CHECK_LOGIN_WISH_LIST) {

            if (resultCode == RESULT_OK) {


                startActivity(WishListActivity.class);

            }

        } else if (requestCode == CODE_CHECK_LOGIN_NOTIFICATION) {

            if (resultCode == RESULT_OK) {

                startActivity(NotificationActivity.class);

            }

        }

    }

}

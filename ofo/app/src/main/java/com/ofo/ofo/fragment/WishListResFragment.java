package com.ofo.ofo.fragment;

import android.view.View;

import com.ofo.ofo.R;
import com.ofo.ofo.database.Data;
import com.ofo.ofo.model.AccountInfo;
import com.ofo.ofo.model.AccountInfoLogin;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.Util;
import com.ofo.ofo.viewholer.RowResHolder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by cat on 5/8/16.
 */
public class WishListResFragment extends RestaurantListFragment {

    @Override
    protected void bindDataToHolder(RowResHolder holder, final Restaurant data, int position) {
        super.bindDataToHolder(holder, data, position);

        holder.groupPromo.setVisibility(View.GONE);

        holder.groupOverlayPromo.setVisibility(View.GONE);

        holder.btnUfavorite.setVisibility(View.VISIBLE);

        holder.btnUfavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AccountInfoLogin info = (AccountInfoLogin) Data.findFirst(getContext(), AccountInfoLogin.class);

                RestClient.get().unfavoriteRes(info.getAccountId(), data.getId()).enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {

                        if (!ErrorUtil.foundAndHandledResponseError(getContext(), response, list)) {

                            getSourceData().remove(data);

                            list.getAdapter().notifyDataSetChanged();

                        }

                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {

                        Util.showSnackBarInView(list, getString(R.string.can_not_connect));

                    }
                });

            }
        });

    }
}

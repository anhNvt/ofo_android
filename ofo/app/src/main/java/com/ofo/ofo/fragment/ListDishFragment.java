package com.ofo.ofo.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.bumptech.glide.Glide;
import com.ofo.ofo.R;
import com.ofo.ofo.activity.ResDetailActivity;
import com.ofo.ofo.model.Dish;
import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.viewholer.RowDishHolder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by cat on 5/11/16.
 */
public class ListDishFragment extends ListFragment<Dish, RowDishHolder> {

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (listData == null || willReloadData()) {

            getListData(RestClient.get().getListDish());

            setReloadData(false);

        } else {

            setAdapter();

        }

    }

    @Override
    protected int getRowLayoutResId() {

        return R.layout.row_dish;

    }

    @Override
    protected RowDishHolder inflateViewHolder(View view) {

        return new RowDishHolder(view);

    }

    @Override
    protected void bindDataToHolder(RowDishHolder holder, final Dish data, int position) {

        super.bindDataToHolder(holder, data, position);

        Glide.with(this).load(data.getImageUrl()).into(holder.imgViewPic);

        Glide.with(this).load(data.getAccountAvatarUrl()).into(holder.imgViewAvatar);

        holder.tvName.setText(data.getAccountName());

        holder.tvCity.setText(data.getLocation());

        holder.tvCmt.setText(data.getComment());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showToastLoading();

                RestClient.get().getRes(data.getRestaurantId()).enqueue(new Callback<Result<Restaurant>>() {
                    @Override
                    public void onResponse(Call<Result<Restaurant>> call, Response<Result<Restaurant>> response) {

                        if (!ErrorUtil.foundAndHandledResponseError(response, getView())) {

                            toastOk();

                            ResDetailActivity.res = response.body().getData();

                            startActivity(ResDetailActivity.class);

                        } else {

                            toastErr();

                        }

                    }

                    @Override
                    public void onFailure(Call<Result<Restaurant>> call, Throwable t) {

                        ErrorUtil.showCannotConnectError(getView());

                    }
                });

            }
        });

    }

}

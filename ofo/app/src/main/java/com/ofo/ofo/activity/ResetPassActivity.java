package com.ofo.ofo.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ofo.ofo.R;
import com.ofo.ofo.model.Result;
import com.ofo.ofo.network.RestClient;
import com.ofo.ofo.util.ErrorUtil;
import com.ofo.ofo.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.Button;
import carbon.widget.EditText;
import carbon.widget.LinearLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPassActivity extends BaseActivity {

    @BindView(R.id.btnBack)
    ImageView btnBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.btnExit)
    ImageView btnExit;
    @BindView(R.id.edtEmail)
    EditText edtEmail;
    @BindView(R.id.btnConfirm)
    Button btnConfirm;
    @BindView(R.id.root)
    LinearLayout root;

    @OnClick({R.id.btnBack, R.id.btnExit, R.id.btnConfirm})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnExit:

                finish();

                break;
            case R.id.btnConfirm:

                toastLoading();

                RestClient.get().resetPass(edtEmail.getText().toString()).enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {

                        if (!ErrorUtil.foundAndHandledResponseError(getBaseContext(), response, root)) {

                            toastOk();

                            Util.showSnackBarInView(root, response.body().getMessage());

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    finish();

                                }
                            }, 1000);

                        } else {

                            toastErr();

                        }

                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {

                        toastErr();

                        Util.showSnackBarInView(root, getString(R.string.can_not_connect));

                    }
                });

                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pass);
        ButterKnife.bind(this);

        tvTitle.setText(R.string.reset_pass_title);

    }

}

package com.ofo.ofo.model;

import java.io.Serializable;

/**
 * Created by cat on 5/12/16.
 */
public class FilterField implements Serializable {

    int price;

    int offer;

    int order_amount;

    int delivery_time;

    int rating;

    public FilterField(int price, int offer, int order_amount, int delivery_time, int rating) {
        this.price = price;
        this.offer = offer;
        this.order_amount = order_amount;
        this.delivery_time = delivery_time;
        this.rating = rating;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(int delivery_time) {
        this.delivery_time = delivery_time;
    }

    public int getOrder_amount() {
        return order_amount;
    }

    public void setOrder_amount(int order_amount) {
        this.order_amount = order_amount;
    }

    public int getOffer() {
        return offer;
    }

    public void setOffer(int offer) {
        this.offer = offer;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}

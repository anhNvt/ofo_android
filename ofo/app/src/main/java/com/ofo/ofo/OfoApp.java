package com.ofo.ofo;

import android.app.Application;

import com.ofo.ofo.util.LocaleHelper;

/**
 * Created by cat on 5/11/16.
 */
public class OfoApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        LocaleHelper.init(getBaseContext());

        LocaleHelper.onCreate(getBaseContext(), LocaleHelper.LOCALE_ENG);


    }
}

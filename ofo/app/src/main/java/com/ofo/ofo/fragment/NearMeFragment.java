package com.ofo.ofo.fragment;

import android.view.View;

import com.ofo.ofo.model.Restaurant;
import com.ofo.ofo.util.Util;
import com.ofo.ofo.viewholer.RowResHolder;

/**
 * Created by cat on 5/9/16.
 */
public class NearMeFragment extends RestaurantListFragment {

    @Override
    protected void bindDataToHolder(RowResHolder holder, Restaurant data, int position) {
        super.bindDataToHolder(holder, data, position);

        holder.groupOverlayPromo.setVisibility(View.GONE);

        holder.groupPromo.setVisibility(View.GONE);

        holder.tvOverlayDistant.setVisibility(View.VISIBLE);

        holder.tvDistant.setVisibility(View.VISIBLE);

        String distant = Util.roundToString(Double.parseDouble(data.getDistance()), "#.##") + " km";

        holder.tvDistant.setText(distant);

        holder.tvOverlayDistant.setText(distant);

    }
}

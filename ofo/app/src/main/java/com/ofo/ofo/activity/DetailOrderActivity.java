package com.ofo.ofo.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.ofo.ofo.R;
import com.ofo.ofo.adapter.OrderItemAdapter;
import com.ofo.ofo.model.DishMenu;
import com.ofo.ofo.model.Order;
import com.ofo.ofo.model.OrderItem;
import com.ofo.ofo.util.Const;
import com.ofo.ofo.util.GlobalData;
import com.ofo.ofo.util.GsonUtils;
import com.ofo.ofo.util.Util;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.widget.TextView;

/**
 * Created by eo_cuong on 5/19/16.
 */
public class DetailOrderActivity extends BaseActivity {


    @BindView(R.id.tvTitle)
    android.widget.TextView tvTitle;

    @BindView(R.id.tvRestarantName)
    TextView tvRestarantName;

    @BindView(R.id.tvOrderDate)
    TextView tvOrderDate;

    @BindView(R.id.tvOrderId)
    TextView tvOrderId;

    @BindView(R.id.tvOrderName)
    TextView tvOrderName;

    @BindView(R.id.tvOrderCity)
    TextView tvOrderCity;

    @BindView(R.id.tvOrderMobile)
    TextView tvOrderMobile;

    @BindView(R.id.tvOrderStreetName)
    TextView tvOrderStreetName;

    @BindView(R.id.tvOrderBuilding)
    TextView tvOrderBuilding;

    @BindView(R.id.rcvOrderItem)
    RecyclerView rcvOrderItem;

    @BindView(R.id.tvTotalCost)
    TextView tvTotalCost;

    @BindView(R.id.tvDiscountPercent)
    TextView tvDiscountPercent;

    @BindView(R.id.tvDiscount)
    TextView tvDiscount;

    @BindView(R.id.tvDeliverCharge)
    TextView tvDeliverCharge;

    @BindView(R.id.tvGrandTotalCost)
    TextView tvGrandTotalCost;


    Order order;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.rate_order_btn_order_detail));

        String data = getIntent().getStringExtra(Const.KEY_SEND_ORDER);
        if (!TextUtils.isEmpty(data)) {
            order = GsonUtils.String2Object(data, Order.class);
        }


        if (order != null) {


            List<OrderItem> orderItems = order.getOrderItems();


            if (Util.isar_Arabic(getApplicationContext())) {
                tvRestarantName.setText(order.getRestaurantNameAr());
            } else {
                tvRestarantName.setText(order.getRestaurantName());
            }

            tvOrderDate.setText(order.getOrderTime());
            tvOrderId.setText(order.getOrderId() + "");
            tvOrderName.setText(order.getName());
            tvOrderMobile.setText(order.getPhoneNo());
            tvOrderCity.setText(order.getCity());
            tvOrderStreetName.setText(order.getStreetName());
            tvOrderBuilding.setText(order.getBuildingName());

            tvDiscountPercent.setText(getString(R.string.discount) + " (" + order.getRestaurantDiscount() + getString(R.string.percent) + ")");

            double subTotal = 0;

            for (OrderItem m : orderItems) {

                subTotal += Double.parseDouble(m.getPrice()) * Integer.parseInt(m.getQuantity());

            }

            double discount = 0;

            discount = subTotal * Double.parseDouble(order.getRestaurantDiscount()) / 100.0;

            double total = subTotal - discount;

            tvTotalCost.setText(getString(R.string.aed) + " " + subTotal + "");

            tvDiscount.setText("-" + discount + "");

            tvGrandTotalCost.setText(getString(R.string.aed) + " " + total + "");


            rcvOrderItem.setHasFixedSize(true);

            rcvOrderItem.setLayoutManager(new LinearLayoutManager(this));

            rcvOrderItem.setAdapter(new OrderItemAdapter(orderItems, getApplicationContext()));


        }

    }

    @OnClick({R.id.btnBack, R.id.btnExit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:

                finish();

                break;
            case R.id.btnExit:

                finish();

                break;

        }
    }
}

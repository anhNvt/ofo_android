package com.ofo.ofo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by cat on 7/20/16.
 */
public class SubDish extends Item {

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("dish_side_child")
    List<SubDishOption> options;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public List<SubDishOption> getOptions() {
        return options;
    }

    public void setOptions(List<SubDishOption> options) {
        this.options = options;
    }

}

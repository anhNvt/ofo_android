package com.ofo.ofo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cat on 5/4/16.
 */
public class Result<MetadataType> {

    @SerializedName("success")
    @Expose
    private Boolean success;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("metadata")
    private MetadataType data;

    @SerializedName("account_info")
    @Expose
    private AccountInfoLogin info;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MetadataType getData() {
        return data;
    }

    public void setData(MetadataType data) {
        this.data = data;
    }

    public AccountInfoLogin getInfo() {
        return info;
    }

    public void setInfo(AccountInfoLogin info) {
        this.info = info;
    }

}

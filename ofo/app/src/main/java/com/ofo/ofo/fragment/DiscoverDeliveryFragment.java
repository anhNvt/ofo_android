package com.ofo.ofo.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.ofo.ofo.R;
import com.ofo.ofo.listener.OnItemClickListener;
import com.ofo.ofo.model.Cuisine;
import com.ofo.ofo.model.Location;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by cat on 5/12/16.
 */
public class DiscoverDeliveryFragment extends BaseFragment implements OnItemClickListener {

    public static final int TYPE_DISCOVER = 0;

    public static final int TYPE_DELIVERY = 1;

    public static int type = 0;

    int step = 0;

    int loc_id = -1;

    int cui_id = -1;

    List data;

    @BindView(R.id.root)
    FrameLayout root;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_discover_delivery, container, false);
        ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (step == 0) {

            goFragLoc();

        }

    }

    @Override
    public void onItemClick(View v, int pos, Object item) {

        switch (type) {

            case TYPE_DISCOVER:

                switch (step){

                    case 0:

                        loc_id = ((Location)item).getId();

                        createFragCui();

                        step = 1;

                        break;
                    case 1:

                        cui_id = ((Cuisine)item).getId();

                        createFragListRes();

                        step = 2;

                        break;

                }

                break;
            case TYPE_DELIVERY:

                loc_id = ((Location)item).getId();

                createFragListRes();

                step = 1;

                break;

        }

    }

    void createFragLoc() {

        LocFragment locFrag = new LocFragment();

        locFrag.setOnItemClickListener(this);

        getChildFragmentManager().beginTransaction().replace(R.id.root, locFrag).commit();

    }

    void createFragCui() {

        CuiFragment cuiFrag = new CuiFragment();

        cuiFrag.setOnItemClickListener(this);

        getChildFragmentManager().beginTransaction().replace(R.id.root, cuiFrag).commit();

    }

    void createFragListRes() {

        ListDiscoverDeliveryResFragment listResFrag = new ListDiscoverDeliveryResFragment();

        listResFrag.setInitParam(type, loc_id, cui_id);

        getChildFragmentManager().beginTransaction().replace(R.id.root, listResFrag).commit();

    }

    void goFragLoc() {

        if (getChildFragmentManager().findFragmentById(R.id.root) == null) {

            createFragLoc();

        }

    }

}
